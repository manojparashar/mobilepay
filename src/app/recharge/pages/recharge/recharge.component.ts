import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { PlanService } from '../../../accounts/services/planService';
import { RechargeService } from '../../services/recharge.Service';
import { TransactionRequest, TransactionType } from '../../../payments/models/transaction-request.model';
import { TransactionService } from '../../../payments/services/transaction.service';
import { PaymentService } from '../../../accounts/services/payment.service';
import { CardinalPaymentService } from '../../../payments/services/cardinal-payment.service';
import { Plan } from '../../../accounts/models/plan';
import { RechargeOptionsModel } from '../../models/recharge-options.model';
import { RechargeCheckoutModel, ICheckoutModel } from '../../../checkout/models/checkout-model';
import { CheckoutService } from '../../../checkout/services/checkout.service';
import { RazaEnvironmentService } from '../../../core/services/razaEnvironment.service';
import { RazaLayoutService } from '../../../core/services/raza-layout.service';

import { LoginpopupComponent } from '../../../core/loginpopup/loginpopup.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Component({
  selector: 'app-recharge',
  templateUrl: './recharge.component.html',
  styleUrls: ['./recharge.component.scss']
})
export class RechargeComponent implements OnInit {
  id: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private rechargeService: RechargeService,
    private planService: PlanService,
    private checkoutService: CheckoutService,
    private razaLayoutService: RazaLayoutService,
    public dialog: MatDialog,
  ) {

  }

  planId: string
  rechargeAmounts: number[];
  plan: Plan;
  isAutoRefillEnable: boolean = false;
  ngOnInit() {
    this.titleService.setTitle('Select recharge amount');
    this.razaLayoutService.setFixedHeader(true);
    this.planId = this.route.snapshot.paramMap.get('planId');

    this.planService.getPlan(this.planId).subscribe(
      (res: Plan) => this.plan = res,
      err => console.log(err),
      () => this.getRechargeOption()
    );
  }

  getRechargeOption() {
    this.rechargeService.getRechargeAmounts(this.plan.CardId).subscribe(
      (res: number[]) => {
        this.rechargeAmounts = res;
      }
    )
  }

  onClickAmountOption(amount: number) {
    const model: RechargeCheckoutModel = new RechargeCheckoutModel();

    model.purchaseAmount = amount;
    model.couponCode = '';
    model.currencyCode = this.plan.CurrencyCode;
    model.cvv = '';
    model.planId = this.plan.PlanId
    model.transactiontype = TransactionType.Recharge;
    model.serviceChargePercentage = this.plan.ServiceChargePercent;
    model.planName = this.plan.CardName;
    model.countryFrom = this.plan.CountryFrom;
    model.countryTo = this.plan.CountryTo;
    model.cardId = this.plan.CardId;
    model.isAutoRefill = this.isAutoRefillEnable;
    this.checkoutService.setCurrentCart(model);
    this.router.navigate(['/checkout/payment-info']);
  }

  selectAmount(id: any) {
    this.id = id;
  }


}


