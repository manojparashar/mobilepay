import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { isNullOrUndefined } from 'util';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Country } from '../../../shared/model/country';
import { mobileTopupModel } from '../../model/mobileTopupModel';
import { OperatorDenominations } from '../../model/operatorDenominations';
import { CountriesService } from '../../../core/services/country.service';
import { MobiletopupService } from '../../mobiletopup.service';
import { ApiErrorResponse } from '../../../core/models/ApiErrorResponse';
import { SideBarService } from '../../../core/sidemenu/sidemenu.service';
import { TransactionType } from '../../../payments/models/transaction-request.model';
import { CheckoutService } from '../../../checkout/services/checkout.service';
import { MobileTopupCheckoutModel } from '../../../checkout/models/checkout-model';
import { AuthenticationService } from '../../../core/services/auth.service';
import { RazaEnvironmentService } from '../../../core/services/razaEnvironment.service';
import { CurrentSetting } from '../../../core/models/current-setting';
import { isNullOrUndefined } from "../../../shared/utilities";
import { Location } from '@angular/common';

@Component({
  selector: 'app-mobiletopup',
  templateUrl: './mobiletopup.component.html',
  styleUrls: ['./mobiletopup.component.scss']
})
export class MobiletopupComponent implements OnInit, OnDestroy {

  allCountry: Country[];
  mycountryName: string;
  mycountryId: number;
  filteredCountry: Observable<Country[]>;
  // autoControl = new FormControl();
  mobileTopupData: mobileTopupModel;
  operatorDenominations: OperatorDenominations[];
  mobileTopupForm: FormGroup;
  amountSent: number;
  countrySelect = new FormControl();
  currentSetting$: Subscription;
  currentSetting: CurrentSetting;
  isTopUpEnable: boolean = false;
  pinnumber:number;
  iso:string;

  constructor(private router: Router, private titleService: Title,
    private formBuilder: FormBuilder,
    private countryService: CountriesService,
    private sideBarService: SideBarService,
    private mobileTopupService: MobiletopupService,
    private checkoutService: CheckoutService,
    private authService: AuthenticationService,
    private razaEnvService: RazaEnvironmentService,
	private location:Location
  ) {

  }

  ngOnInit() {
    this.titleService.setTitle('Mobile Topup');
    this.sideBarService.toggle();
    this.getCountries();
	this.mycountryId = 0;
    this.mobileTopupForm = this.formBuilder.group({
      countryTo: ['', [Validators.required]],
      phoneNumber: ['', Validators.required],
      topUpAmount: [null]
    });

    this.mobileTopupForm.get('countryTo').valueChanges.subscribe(res => {
      this.changeNumber();
    });

    this.filteredCountry = this.mobileTopupForm.get('countryTo').valueChanges
      .pipe(
        startWith<string | Country>(''),
        map(value => typeof value === 'string' ? value : value.CountryName),
        map(CountryName => CountryName ? this._filter(CountryName) : this.allCountry)
      );

    this.currentSetting$ = this.razaEnvService.getCurrentSetting().subscribe(res => {
      this.currentSetting = res;
    });
	  
	  this.pinnumber = (history.state.pin)?history.state.pin:'';
    this.iso = (history.state.iso)?history.state.iso:'';
    if( this.pinnumber >0 )
    {
      this.mobileTopupForm.controls["phoneNumber"].setValue(this.pinnumber);
    }
     
  }

  ngOnDestroy(): void {
    this.currentSetting$.unsubscribe();
  }

  private getCountries() {
    this.countryService.getAllCountries().subscribe(
      (data: Country[]) => {
        this.allCountry = data;
        if(this.iso !='')
        {
          for (let i = 0; i < data.length; i++) {
           // if(data[i].iso == this.iso)
          // {
            //this.onSelectCountrFrom(data[i]);
          // }
          }
        }
      },
      (err: ApiErrorResponse) => console.log(err),
    );
  }

  radioChange(event) {
    this.amountSent = event.value;
  }

  displayFn(country?: Country): string | undefined {
    return country ? country.CountryName : undefined;
  }

  private _filter(value: any): Country[] {
    const filterValue = value.toLowerCase();
    return this.allCountry.filter(option => option.CountryName.toLowerCase().indexOf(filterValue) === 0);
  }

  changeNumber() {
    this.mobileTopupForm.get('topUpAmount').updateValueAndValidity();
    this.mobileTopupForm.get('phoneNumber').enable();

    this.mobileTopupForm.patchValue({
      phoneNumber: '',
      topUpAmount: null
    });

    this.mobileTopupData = null;
    this.isTopUpEnable = false;
  }

  getTopUpOperatorInfo() {
    if (!this.mobileTopupForm.valid) {
      return;
    }

    const formValue = this.mobileTopupForm.value;

    const country: Country = formValue.countryTo;
    let phoneNumberWithCode: string = country.CountryCode + formValue.phoneNumber;
    this.mobileTopupService.GetMobileTopUp(this.currentSetting.currentCountryId, phoneNumberWithCode).subscribe(
      (data: mobileTopupModel) => {
        if (isNullOrUndefined(data)) {
          this.mobileTopupForm.get('phoneNumber').setErrors({ Invalid_Operator: true })
          return;
        }
        this.mobileTopupData = data;
        this.isTopUpEnable = true;
        this.mobileTopupForm.get('topUpAmount').updateValueAndValidity();
        this.mobileTopupForm.get('phoneNumber').disable();
      },
      (err: ApiErrorResponse) => console.log(err),
    );
  }

  get operatorImage() {
    return `assets/images/operators/${this.mobileTopupData.Operator.toLowerCase()}.png`;
  }

  get buttonText() {
    if (!this.isTopUpEnable) {
      return 'Submit';
    } else {
      return 'Checkout';
    }
  }

  onMobileTopupFormSubmit() {
    // stop here if form is invalid
    if (!this.isTopUpEnable) {
      this.getTopUpOperatorInfo();
      return;
    }
    this.validateAmountSelection();
    if (!this.mobileTopupForm.valid) {
      return;
    }

    const checkoutModel: MobileTopupCheckoutModel = new MobileTopupCheckoutModel();

    checkoutModel.transactiontype = TransactionType.Topup;
    checkoutModel.country = this.mobileTopupForm.get('countryTo').value as Country; // this.autoControl.value.countryTo as Country;
    checkoutModel.topupOption = this.mobileTopupForm.value.topUpAmount as OperatorDenominations;
    checkoutModel.currencyCode = this.currentSetting.currency;
    checkoutModel.phoneNumber = this.mobileTopupForm.get('phoneNumber').value;
    checkoutModel.operatorCode = this.mobileTopupData.OperatorCode;
    checkoutModel.countryFrom = this.currentSetting.currentCountryId;
    checkoutModel.isHideCouponEdit = true;

    this.checkoutService.setCurrentCart(checkoutModel);

    if (this.authService.isAuthenticated()) {
      this.router.navigate(['checkout/payment-info']);
    } else {
      this.router.navigate(['checkout']);
    }
  }

  validateAmountSelection() {
    if (!this.isTopUpEnable) {
      return false;
    }
    const amount = this.mobileTopupForm.get('topUpAmount').value;
    if (isNullOrUndefined(amount)) {
      this.mobileTopupForm.get('topUpAmount').setErrors({ Amount_Req: true })
      return false
    }

    return true;
  }
  
  onSelectCountrFrom(country: Country) {
    
    
	this.mycountryId= country.CountryId; 
  } 
  
  unsetFlag() {
    
    
	this.mycountryId= -1; 
  }
  

}
