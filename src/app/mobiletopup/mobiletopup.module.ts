import { NgModule } from '@angular/core';
import { MaterialModule } from '../core/material.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MobiletopupService } from './mobiletopup.service';
import { RouterModule } from '@angular/router';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
/*
import { MobiletopupComponent } from './pages/mobiletopup/mobiletopup.component';
*/

@NgModule({
  imports: [
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    RouterModule.forChild([
     // { path: '', component: MobiletopupComponent },
     // { path: '/mobiletopup/:planid/:iso', component: MobiletopupComponent },
       
    ])
  ],
  exports: [

  ],
  declarations: [
   // MobiletopupComponent
  ],
  providers: [MobiletopupService]
})
export class MobiletopupModule { };