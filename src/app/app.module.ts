import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
//import { TextMaskModule } from 'angular2-text-mask';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientModule } from '@angular/common/http';
import { RecaptchaModule, RecaptchaFormsModule, RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from 'ng-recaptcha';

import { CoreModule } from './core/core.module';
import { HomeModule } from './home/home.module';
import { DealsModule } from './deals/deals.module';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
//import { MobiledealsModule } from './mobiledeals/mobiledeals.module';


import { RazaSplashScreenService } from './core/services/razaSplashScreen.Service';
import { SideBarService } from './core/sidemenu/sidemenu.service';
import { RazaEnvironmentService, LocationService } from './core/services/razaEnvironment.service';

import { AppComponent } from './app.component';
//import { CallUsComponent } from './shared/dialog/call-us/call-us.component';
import { ErrorDialogComponent } from './shared/dialog/error-dialog/error-dialog.component';
//import { Page404Component } from './page404/page404.component';
//import { LearnmoreComponent } from './globalrates/learnmore/learnmore.component';
//import { GlobalCallComponent } from './globalrates/global-call/global-call.component';
//import { GlobalCallIndiaComponent } from './globalrates/global-call-india/global-call-india.component';

import { ConfirmPopupDialog } from './shared/dialog/confirm-popup/confirm-popup-dialog';

import { environment } from '../environments/environment';
//import { MobiledealsComponent } from './mobiledeals/mobiledeals.component';
//import { LoginpopupComponent } from './core/loginpopup/loginpopup.component';


import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
//import { SignuppopupComponent } from './core/signuppopup/signuppopup.component';
//import { GlobalratesDialogComponent } from './globalrates/globalrates-dialog/globalrates-dialog.component';
//mport { FreetrialDialogComponent } from './globalrates/freetrial-dialog/freetrial-dialog.component';
//import { GlobalBuyComponent } from './globalrates/global-buy/global-buy.component';
//import { NodataFoundComponent } from './core/nodata-found/nodata-found.component';
//import { CreateInstanceComponent } from './create-instance/create-instance.component';
//import { Buy5popupComponent } from './globalrates/buy5popup/buy5popup.component';



import { NgxAutocomPlaceModule } from 'NgxAutocomPlace';
//import { GooglePlacesComponent } from './google-places/google-places.component';
//import { AutoLoginComponent } from './auto-login/auto-login.component';

import { RouterModule, Routes } from '@angular/router';
//import { MobilePayModule } from './mobile-pay/mobile-pay.module';

import { BottomAppScreenComponent } from './bottom-app-screen/bottom-app-screen.component';
import { PlanComponent } from './mobile-pay/plan/plan.component';
import { PaymentComponent } from './mobile-pay/payment/payment.component';
import { AddCardComponent } from './mobile-pay/add-card/add-card.component';
import { NewCreditCardComponent } from './mobile-pay/new-credit-card/new-credit-card.component';
import { DialogCofirmComponent } from './mobile-pay/dialog/dialog-cofirm/dialog-cofirm.component';
import { BottomUpComponent } from './mobile-pay/dialog/bottom-up/bottom-up.component';

import { MaterialModule } from './core/material.module';
import { PaymentModule } from './payments/payment.module';
import { CommonModule } from '@angular/common';



import { MatTooltipModule } from '@angular/material/tooltip';

 
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { TopupComponent } from './mobile-pay/topup/topup.component';
 
 
import { DialogOperatorComponent } from './mobile-pay/dialog/dialog-operator/dialog-operator.component';
import { TopupConfirmComponent } from './mobile-pay/dialog/topup-confirm/topup-confirm.component';
import { BottomGpayComponent } from './mobile-pay/dialog/bottom-gpay/bottom-gpay.component';
import { BottomApplePayComponent } from './mobile-pay/dialog/bottom-apple-pay/bottom-apple-pay.component';
import { ErrLoginComponent } from './mobile-pay/dialog/err-login/err-login.component';
import { CountryRatesComponent } from './mobile-pay/country-rates/country-rates.component';
import { TopupErrorComponent } from './mobile-pay/dialog/topup-error/topup-error.component';
import { TermsConditionComponent } from './mobile-pay/terms-condition/terms-condition.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { ContactUsComponent } from './mobile-pay/contact-us/contact-us.component';
import { Buy5get5Component } from './buy5get5/buy5get5.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { BuyOneGetOneComponent } from './mobile-pay/buy-one-get-one/buy-one-get-one.component'
//import { WebviewComponent } from './mobile-pay/webview/webview.component';
 

var google: any;

export function LocationProviderFactory(provider: RazaEnvironmentService) {


  return () => provider.getCurrentLocationByIp();

}

@NgModule({

  declarations: [

    AppComponent,
    //CallUsComponent,
    //GlobalCallComponent,
    //GlobalCallIndiaComponent,
    ErrorDialogComponent,
    ConfirmPopupDialog,
    // Page404Component,
    //LearnmoreComponent,
    //MobiledealsComponent,
    //LoginpopupComponent,
    // SignuppopupComponent,
    //GlobalratesDialogComponent,
    //FreetrialDialogComponent,
    //GlobalBuyComponent,
    //NodataFoundComponent,
    //CreateInstanceComponent,
    //Buy5popupComponent,
    //GooglePlacesComponent,
    //AutoLoginComponent,

    BottomAppScreenComponent,
    
    /*
    PlanComponent,  
    TopupComponent,
    PaymentComponent, 
    AddCardComponent, 
    NewCreditCardComponent, 
   
     DialogCofirmComponent, 
     BottomUpComponent,  
 */

    PlanComponent,
    TopupComponent, 
    PaymentComponent,
    AddCardComponent,
    NewCreditCardComponent,

    DialogCofirmComponent,
    BottomUpComponent,
   
    DialogOperatorComponent,
    TopupConfirmComponent,
    BottomGpayComponent,
    BottomApplePayComponent,
    ErrLoginComponent,
    CountryRatesComponent,
    TopupErrorComponent,
    TermsConditionComponent,
    ContactUsComponent,
    Buy5get5Component,
    ThankyouComponent,
    BuyOneGetOneComponent,
    //WebviewComponent,
 

  ],

  imports: [
     
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    CoreModule,
    HomeModule,
    DealsModule,
    SharedModule,
    MatButtonModule,
    MatDialogModule,
    //MobiledealsModule,
    //RecaptchaModule,
    //RecaptchaFormsModule,
    //RecaptchaV3Module,
    NgxAutocomPlaceModule,
    CommonModule,
    MaterialModule,
    PaymentModule,

    MatTooltipModule,
    MatBottomSheetModule,
    //MobilePayModule
    // TextMaskModule,
    MatExpansionModule
     


  ],

  entryComponents: [
    //CallUsComponent,
    //GlobalCallComponent,
    //GlobalCallIndiaComponent,
    ErrorDialogComponent,
    ConfirmPopupDialog,

  ],

  providers: [
    RazaSplashScreenService,
    SideBarService,
    LocationService,
    RazaEnvironmentService,
    { provide: RECAPTCHA_V3_SITE_KEY, useValue: environment.captchaKeyV3 },
    { provide: APP_INITIALIZER, useFactory: LocationProviderFactory, deps: [RazaEnvironmentService], multi: true },
  ],
  exports: [NewCreditCardComponent, AddCardComponent],
  bootstrap: [AppComponent]
})
export class AppModule {

}
