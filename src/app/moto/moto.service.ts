import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
 
import { RazaEnvironmentService } from "../core/services/razaEnvironment.service";
import { Api } from "../core/services/api.constants";
import { HttpClient } from "@angular/common/http";
import { CustomErrorHandlerService } from "../core/services/custom-error-handler.service";
import { State, PostalCode } from "../accounts/models/billingInfo";
import { ApiErrorResponse } from "../core/models/ApiErrorResponse";
import { catchError } from "rxjs/operators";

@Injectable({providedIn: 'root'})

@Injectable({
  providedIn: 'root'
})
export class MotoService {

  constructor(private httpClient: HttpClient,
    private razaEnvService: RazaEnvironmentService,
    private errorHandleService: CustomErrorHandlerService
) { }
 

public generateMotoOrder(token: string):Observable<[] | ApiErrorResponse >
{
    return this.httpClient.get<[]>(`${Api.moto.generateMotoOrder}/${token}`)
    .pipe(
        catchError(err=> this.errorHandleService.handleHttpError(err))
    );  
}


}