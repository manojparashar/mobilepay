import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MotoComponent } from './moto/moto.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { CartRequireGuard } from '../checkout/guards/cart-require.guard';
import { CartResolverService } from '../checkout/services/cart-resolver.service';

const routes: Routes = [{ path: '', component: MotoComponent, pathMatch: 'full' },
{ path: '/:motocode', component: MotoComponent },
 
{
  path: 'checkout', component: CheckoutComponent,
  canActivate: [AuthGuard, CartRequireGuard],
  resolve: { cart: CartResolverService },
   
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MotoRoutingModule { }
