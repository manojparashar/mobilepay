import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MotoRoutingModule } from './moto-routing.module';
import { MotoComponent } from './moto/moto.component';
import { CheckoutComponent } from './checkout/checkout.component';


@NgModule({
  declarations: [MotoComponent, CheckoutComponent],
  imports: [
    CommonModule,
    MotoRoutingModule
  ]
})
export class MotoModule { }
