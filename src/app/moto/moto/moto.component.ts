import { Component, OnInit } from '@angular/core';
import { MotoService } from '../moto.service'
import { Router } from '@angular/router';
import { CountriesService } from '../../core/services/country.service';
import { AuthenticationService } from '../../core/services/auth.service';
import { SearchRatesService } from '../../rates/searchrates.service';
import { RazaEnvironmentService } from '../../core/services/razaEnvironment.service';
import { NewPlanCheckoutModel } from '../../checkout/models/checkout-model';
import { CheckoutService } from '../../checkout/services/checkout.service';
import { PromotionPlan } from '../../deals/model/promotion-plan';
import { CurrentSetting } from '../../core/models/current-setting';
import { TransactionType } from '../../payments/models/transaction-request.model';
@Component({
  selector: 'app-moto',
  templateUrl: './moto.component.html',
  styleUrls: ['./moto.component.css']
})
export class MotoComponent implements OnInit {

  constructor(
    private router: Router,
    private motoService: MotoService,
    private authService: AuthenticationService,
    private checkoutService: CheckoutService,
    ) { }
    
  currentSetting: CurrentSetting;
  userData: any[]; 
  userToken:string;
  ngOnInit(): void {
    var moto = 'MOTOB8C7BC6562';
    this.userToken = moto;
    localStorage.removeItem('currentUser');  
    this.loginWithToken();
  }


  loginWithToken() 
  {
        let body = {
          username: this.userToken,
          password: '0000000000',
          captcha: ''
        };
     
        this.authService.loginwToken(body, false, "Y").subscribe((response) => {
           if (response != null) {
           this.getMotoInfo();  
          }  
        },
        (error) => {
         });
 
  }
  
  getMotoInfo()
  {
    this.motoService.generateMotoOrder(this.userToken).subscribe(res => {
      console.log(res);
      this.buyPlan(res);
     //  if(res){this.userData = res};
      //this.getActivePromotion(this.currentSetting.currentCountryId);
    })
  }

  buyPlan(plan) {
    const model: NewPlanCheckoutModel = new NewPlanCheckoutModel();

    model.CardId = plan.CardId;
    model.CardName = plan.CardName;
    model.CurrencyCode = plan.CurrencyCode;
    model.details = {
      Price: plan.PurchaseAmount,
      ServiceCharge: plan.ServiceChargePercent ? plan.ServiceChargePercent : 0,
      SubCardId: plan.SubCardId
    }
    model.country = null;
    model.phoneNumber = null;
    model.countryFrom = plan.CountryFrom;
    model.countryTo = plan.CountryTo;
    model.couponCode = '';
    model.currencyCode = plan.CurrencyCode;
    model.transactiontype = TransactionType.Activation;
    model.couponCode = plan.CouponCode;
    model.isCalculatedServiceFee = false;
    model.isAutoRefill = true;
    model.isMandatoryAutorefill = true;
    model.isHideCouponEdit = !plan.IsEditCoupon;

    this.checkoutService.setCurrentCart(model);
    this.router.navigate(['./moto/checkout']);
    
     
  }

}
