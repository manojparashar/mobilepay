import { Component, OnInit, Injector, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialog} from '@angular/material/dialog';
import { ViewChild, AfterViewInit, NgZone} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
 

import { AuthenticationService } from '../../core/services/auth.service';
import { CountriesService } from '../../core/services/country.service';
 
import { RazaSnackBarService } from '../../shared/razaSnackbar.service';
import { isValidaEmail, isValidPhoneNumber, autoCorrectIfPhoneNumber, isValidPhoneOrEmail } from '../../shared/utilities';
import { environment } from '../../../environments/environment';
import { AppBaseComponent } from '../../shared/components/app-base-component';

 
import { CreditCardValidators } from 'angular-cc-library';
import { Observable } from 'rxjs/internal/Observable';
 
 
import { RazaEnvironmentService } from '../../core/services/razaEnvironment.service';
import { CustomerService } from '../../accounts/services/customerService';
import { RazaSharedService } from '../../shared/razaShared.service';
import { State, BillingInfo } from '../../accounts/models/billingInfo';
import { CodeValuePair } from '../../core/models/codeValuePair.model';
import { Country } from '../../shared/model/country';
import { ICheckoutModel, NewPlanCheckoutModel } from '../../checkout/models/checkout-model';
import { TransactionType } from '../../payments/models/transaction-request.model';
import { CreditCard } from '../../accounts/models/creditCard'; //BraintreeCard
import { ConfirmPopupDialog } from '../../accounts/dialog/confirm-popup/confirm-popup-dialog';
import { ConfirmMsgDialogComponent } from '../../accounts/dialog/confirm-msg-dialog/confirm-msg-dialog.component';
import { ApiErrorResponse } from '../../core/models/ApiErrorResponse';

import { AddCreditcardDialog } from '../../accounts/dialog/add-creditcard-dialog/add-creditcard-dialog';
import { AddCreditcardPayDialog } from '../../accounts/dialog/add-creditcard-pay-dialog/add-creditcard-pay-dialog';

import { isNullOrUndefined } from "../../shared/utilities";
import { TransactionService } from '../../payments/services/transaction.service';
import { BraintreeService } from '../../payments/services/braintree.service';
import { BraintreeCard } from '../../accounts/models/braintreeCard';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as braintree from 'braintree-web';
import { IPaypalCheckoutOrderInfo, ActivationOrderInfo, RechargeOrderInfo, ICheckoutOrderInfo, MobileTopupOrderInfo } from '../../payments/models/planOrderInfo.model';

import { ApiProcessResponse } from '../../core/models/ApiProcessResponse';  
 import { TransactionMobProcessBraintreeService } from "../../payments/services/transaction-mob-process-braintree.service";
 import { ValidateCouponCodeResponseModel, ValidateCouponCodeRequestModel } from '../../payments/models/validate-couponcode-request.model';
 import { RechargeCheckoutModel } from '../../checkout/models/checkout-model';
 import { Plan } from '../../accounts/models/plan';
 import { PlanService } from '../../accounts/services/planService';
 import { CheckoutService } from '../../checkout/services/checkout.service';
 import { ErrorDialogModel } from '../../shared/model/error-dialog.model';
 import { ErrorDialogComponent } from '../../shared/dialog/error-dialog/error-dialog.component';

 import { DialogCofirmComponent } from '../dialog/dialog-cofirm/dialog-cofirm.component';
 import { BottomUpComponent } from '../dialog/bottom-up/bottom-up.component';
 import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';

 import { IOnApproveCallbackData, IPayPalConfig } from '../../payments/paypal/model/paypal.model';
 import { TransactionProcessFacadeService } from '../../payments/services/transactionProcessFacade';
 import { throwError as observableThrowError,  of, Subscription } from 'rxjs';
 declare var jQuery: any;
interface Window {
  webkit?: any;
  androidCallBackInterface:any;
}
declare var window: Window;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent extends AppBaseComponent implements OnInit {
  hide_header_footer:boolean=true;
  first_page:boolean=true;
  second_page:boolean=false;
  third_page:boolean=false;
  fourth_page:boolean=false;
  fifth_page:boolean=false;
  six_page:boolean=false;
  seventh_page:boolean=false;
  eight_page:boolean=false;
  userPhone : any;
  userPass : any;
  enc_key:any;
  key_arr:any;
  creditCard:CreditCard;
  paymentDetailForm: FormGroup;
  billingInfoForm: FormGroup;
  paymentInfoForm: FormGroup;
  existingCreditCardForm: FormGroup;

  countries: Country[]
  fromCountry: Country[] = [];
  months: CodeValuePair[];
  years: CodeValuePair[];
  states: State[] = [];
  customerSavedCards: CreditCard[];
  havingExistingCard: boolean = false;
  selectedCard: CreditCard;
  selectedCardPay: CreditCard;
  braintreeCard:BraintreeCard;
  cvvStored: string;
  billingInfo: BillingInfo;
  countryFromId: number = 1;
  autoRefillTipText: string;
  braintreeToken:any;
  paymentProcessor:any;
  countryId:number;
  IsEnableFreeTrial:boolean=true;
  autocompleteInput: string;
queryWait: boolean;
checked:boolean=true;

address: Object;
establishmentAddress: Object;
formattedAddress: string;
formattedEstablishmentAddress: string;
phone: string;
search_country:any='US';
search_country_id:any=1;
plan: Plan;
isEnableOtherPlan: boolean = false;
isAutoRefillEnable: boolean = false;
paymentSubmitted: boolean;
amount:any;
balance:number=0;
processType:any=''; 
addNewCard:boolean=false;
editCard:boolean=false; 
showCartInfo:boolean=false;
currentCart: ICheckoutModel;
process_info:any='';
platform:any;
cardCvvError:boolean=false;

DeviceId:string;
DeviceName:string;
DeviceModel:string;
DeviceType:string;
AppVersion:string;
  hide: boolean;
  promotionCode :string='';
  topup_no:String='';
  headingText: String='';
  btnText:String;
  customer_id:any;
pin_no:any;
alphabet:any;
processWithPaypal:boolean=false;
public payPalConfig?: IPayPalConfig;
/**********EOF Google place search **********/
 
constructor( 
  private _bottomSheet: MatBottomSheet,
  private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private _formBuilder: FormBuilder,
    
    private countriesService: CountriesService,
    private dialog: MatDialog,
    private snackBarService: RazaSnackBarService,

    public zone: NgZone,
    private transactionService: TransactionService, 
    private braintreeService: BraintreeService,
    private formBuilder: FormBuilder,
    private countryService: CountriesService,
    private razaEnvService: RazaEnvironmentService,
    private customerService: CustomerService,
    
    private razaSnackbarService: RazaSnackBarService,
    private authService: AuthenticationService,
    private httpClient: HttpClient,
    private razaSharedService: RazaSharedService,
    
    private transactionProcessFacade: TransactionProcessFacadeService,
    private transactionProcessBraintree: TransactionMobProcessBraintreeService,
    private planService: PlanService,
    private checkoutService: CheckoutService,
    _injector: Injector
  ) {
    
    super(_injector);
    
  }

  
  ngOnInit(): void { 
    
    this.enc_key = localStorage.getItem('enc_key');
    let data_list =  JSON.parse(atob(this.enc_key));
    this.key_arr = data_list;
    console.log(data_list);
    this.userPhone = data_list.phone;
    this.userPass = data_list.password;
     
    this.search_country_id = data_list.country_id;
    this.countryFromId = this.search_country_id;
    this.promotionCode = (data_list.coupon_code)?data_list.coupon_code:'';

    this.customer_id =(data_list.customer_id)?data_list.customer_id:'' ;
    this.pin_no = (data_list.pin_no)?data_list.pin_no:'' ;
    this.alphabet = (data_list.alphabet)?data_list.alphabet:'A';

    let device_info         = data_list.de;
    if(device_info.includes("||"))
    {
      var dres = device_info.split("||")
      this.platform           = dres[4];
      this.DeviceType         = dres[4];
    }
    else
    {
      this.platform           = device_info;
      this.DeviceType         = device_info;
    }

     
    let top_no              = ( data_list.top_no && data_list.top_no !='' ) ?data_list.top_no:'';
    this.topup_no           = top_no;
 

    this.currentCart = this.route.snapshot.data['cart'];
    if(this.currentCart.transactiontype == 4)
    {
      this.headingText = 'Mobile Top-up';
      this.btnText = 'Top-up Now';
    }
    else{
      this.headingText = 'Recharge';
      this.btnText = 'Recharge Now';
    }
    console.log("this.currentCart");
    console.log(this.currentCart);

    this.getCards();
    this.getUserPlans();
    this.paymentDetailForm = this._formBuilder.group({
      CardNumber: ['', [Validators.required, CreditCardValidators.validateCCNumber]],
      Cvv2: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(4)]],
      ExpMonth: ['', Validators.required],
      ExpYear: ['', Validators.required]
    });
    this.billingInfoForm = this._formBuilder.group({
      FullName: ['', Validators.required],
      Country: ['', Validators.required],
      State: ['', Validators.required],
      BillingAddress: ['', Validators.required],
      City: ['', Validators.required],
      PostalCode: ['', [Validators.required, Validators.maxLength(10)]]
    });
    
   // const cart = JSON.parse(localStorage.getItem('currentCart'))
     // console.log(cart);
    
   // this.getCart();
   this.initPaypalConfig(); 
  }
  getCart()
  {
    this.checkoutService.getCurrentCart().subscribe((model: ICheckoutModel) => {
      if (model === null) {
        
      }
      
     // this.currentCart = model;
      //console.log("this.currentCart");
      //console.log(this.currentCart);
    }, err => {
    })
  }
  

  onIconClick(event){
    event.stopPropagation();
    this.hide = !this.hide;
}
 
  onClickAmountOption(cart) {

    this.amount= cart.purchaseAmount;
    const model: RechargeCheckoutModel = new RechargeCheckoutModel();
    model.purchaseAmount = cart.purchaseAmount;
    model.couponCode = cart.couponCode;
    model.currencyCode = cart.currencyCode;
    model.cvv = cart.cvv;
    model.planId = cart.planId;
    model.transactiontype = cart.transactiontype;
    model.serviceChargePercentage = cart.serviceChargePercentage;
    model.planName = cart.planName;
    model.countryFrom = cart.countryFrom;
    model.countryTo = cart.countryTo;
    model.cardId = cart.cardId;
    
    model.isAutoRefill = cart.isAutoRefill;
    this.checkoutService.setCurrentCart(model);
    
    this.currentCart = model;
    console.log(this.currentCart);
  } 

  buyPlan(cart) {

    this.amount = cart.Price;

   
    const model: NewPlanCheckoutModel = new NewPlanCheckoutModel();
    model.CardId = cart.CardId;
    model.CardName = cart.CardName;
    model.CurrencyCode = cart.CurrencyCode;
    model.details = {
      Price: cart.details.Price,
      ServiceCharge: cart.details.ServiceCharge,
      SubCardId: cart.details.SubCardId
    }
    model.isPromotion = true;
    model.country = null;
    model.phoneNumber = null;
    model.countryFrom = cart.countryFrom;
    model.countryTo = cart.countryTo;
     
    model.currencyCode = cart.currencyCode;
    model.transactiontype = cart.transactiontype;
    model.couponCode = cart.couponCode;
    model.isCalculatedServiceFee = cart.isCalculatedServiceFee;
    model.isAutoRefill = cart.isAutoRefill;
    model.isMandatoryAutorefill = cart.isMandatoryAutorefill;
    model.isHideCouponEdit = cart.isHideCouponEdit;

    this.checkoutService.setCurrentCart(model);
    //this.currentCart = model;
    this.getCart();
  }
  operatorImage(obj) {
    return `assets/images/operators/${obj.toLowerCase()}.png`;
  }
  getUserPlans(){
    var phone =   this.userPhone;
    if(this.search_country_id == 2 || this.search_country_id == 1) 
  {
    
   

      if(this.search_country_id == 3 || this.search_country_id == 26)
      {
        phone =   phone.substring(2);
      }
      else
      {
        phone =   phone.substring(1);
      }
  }

    this.planService.getPlanInfo(phone).subscribe(
      (res:any)=>{
        console.log(res);
        this.plan = res;
         
        this.balance = this.plan.Balance;
         
        //PlanId
        //this.currentPlanName = res.CardName;
       // this.currentBalance = res.Balance;
      }
    );

/*
    this.planService.getAllPlans().subscribe(
      (data: Plan[]) => {
        this.plan = data[0];
        this.balance = this.plan.Balance;
       // this.onClickAmountOption();
        this.isEnableOtherPlan = data.length > 1
      },
      (err: ApiErrorResponse) => console.log(err)
    );*/
  }
  loadBillingInfo(): void {
    this.customerService.GetBillingInfo().subscribe(
      (res: any) => { this.billingInfo = res;
     
      },
      (err: ApiErrorResponse) => console.log(err),
    )
  }
  getCreditCardValidityOptions() {
    this.years = this.razaEnvService.getYears();
    this.months = this.razaEnvService.getMonths();
  }
  getBillingInfo() {
    this.customerService.GetBillingInfo().toPromise().then(
      (res: BillingInfo) => {
        if (res.Address) {
          //this.onCountryChange(res.Address.Country.CountryId)

          this.search_country_id = res.Address.Country.CountryId;
        }

        if(res.Email && res.Email !='')
        {
          var user_email = res.Email;
          if(user_email.includes("@raza.temp"))
          {
            res.Email = '';
          }
        }

        const billInfo = {
          firstName: res.FirstName,
          lastName: res.LastName,
          email: res.Email,
          address: res.Address.StreetAddress,
          zipcode: res.Address.ZipCode,
          city: res.Address.City,
          country: res.Address.Country.CountryId,
          state: res.Address.State,
          phoneNumber: res.Address.HomePhone,
        };
        this.billingInfo = res;
        this.billingInfoForm.patchValue(billInfo);
      })
  }
  setItem(obj:any)
  {
    this.processType = 'pay';
    this.selectedCard =obj;
    
  }
  deleteCardDetails()
  {
    const dialogRef = this.dialog.open(ConfirmMsgDialogComponent, {
      data: {
        success: 'success'
      }
    });
    let card = this.selectedCard;
    dialogRef.afterClosed().subscribe(result => {

      if (result == "success") {
        this.customerService.DeleteCreditCard(card.CardId).subscribe(
          (res: boolean) => {
            if (res) {
              this.razaSnackbarService.openSuccess("Credit card deleted successfully.");
             /* this.customerService.getSavedCreditCards().subscribe(
                (data: CreditCard[]) => { this.creditCards = data;  },
                (err: ApiErrorResponse) => console.log(err),
              )*/

              this.getCards();
            }
            else
              this.razaSnackbarService.openError("Unable to delete information, Please try again.");
          },
          err => this.razaSnackbarService.openError("An error occurred!! Please try again.")
        )
      }
    });

  }
  addCardClick(obj)
  {
    this.selectedCard = null;
    this.processType = obj;
    
  }
  modelChangeFn(event: any)
  {
     
    this.selectedCard.Cvv = event.target.value;
  }
  getChecked(item:any)
  {

    if(item !='' && item.CardId == this.selectedCardPay.CardId)
    {
      return true;
    }
    else
    return false;
  }
  getCards()
  {
    this.customerService.getSavedCreditCards().toPromise().then(
      (res: CreditCard[]) => {
        if (res.length > 0) 
        {
          this.customerSavedCards = res.splice(0, 2);
		 
          this.selectedCardPay=this.customerSavedCards[0];
          console.log(this.selectedCardPay);
          this.selectedCard = this.selectedCardPay;
          //localStorage.setItem('card':{card:this.selectedCardPay.CardId, selectedCard:this.selectedCardPay.Cvv});
          //firstFourChars = input.substring(0, 4);
          //res = str.Substring(str.Length - 4);
          this.processType = 'pay';
          this.loadBillingInfo();
          this.havingExistingCard = true;
          this.addNewCard = false;
          //this.closeApp('Back from Credit card page');
        } 
        else
         {
          this.getCreditCardValidityOptions();
          this.getBillingInfo();
          this.havingExistingCard = false;
          this.addNewCard = true;
         // this.closeApp('Back to Credit card page')
		 
        }
       });
  }

  getCardIcon(CreditCard: CreditCard) {
    switch (CreditCard.CardType.toLowerCase()) {
      case 'visa':
        return 'assets/images/visa_card.png';
        break;
      case 'mastercard':
        return 'assets/images/master-card.png';
        break;
      case 'discover':
        return 'assets/images/discover-card.png';
        break;
      case 'amex':
        return 'assets/images/american-card.png';
        break;
      default:
        break;
    }
  }
    isDisplayCvvRequired(item: CreditCard): boolean {
    return this.selectedCard.CardId === item.CardId && item.Cvv.length === 0 && this.paymentSubmitted;
  }
  getFormatedCard(obj:any)
  {
    var firstFourChars = obj.substr(0, 4);
    var last = obj.substr(-4);
    return firstFourChars+' ....'+last;
  }

  editCardClick(obj:any)
  {
    
    localStorage.removeItem('errorMsg');
    localStorage.removeItem('errorCode');
    localStorage.removeItem('selectedCvv');
    
    if(this.selectedCard.CardId)
    {
      this.processType = obj;
      
      this.customerService.EditCreditCard(this.selectedCard).subscribe(data => {
        this.cardBillingAddress(data);
      });
    }
  }
  cardBillingAddress(data) {
    this.selectedCard = data;
    this.addNewOrPay();
  }

  androidFuntionRevert(toast) {
    console.log(toast);
    try {
         window.androidCallBackInterface.callBackFormData(toast);
        } catch(err) {
       }
    }

    iosFuntionRevert(resultdata)
    {
      console.log(resultdata);
      try {
           var message = resultdata;
            window.webkit.messageHandlers.callbackHandler.postMessage(message);
            } 
            catch(err) {
            }
 
    }
    closeApp2()
    {
      this.closeApp('Closed, routed to keypad')
    }
  closeApp(obj:any)
  {
    console.log(obj);
     
    var msg = '';
    if(obj !='')
    {
      msg = obj;
    }
    else
    {
      msg = ''
    }
    msg = 'Mobile web view closed by client'
    if(this.platform == 'ios')
    {
      this.iosFuntionRevert(msg);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(msg);
    }
  }
gotopreviouPage()
{
  
  //console.log(this.topup_no);
   if(this.topup_no != '')
   {
     //this.closeApp('Back to Topup page')
    this.router.navigate(['/topup/'+this.enc_key]);
   }
   
   else if(this.customer_id !='')
   {
    //this.closeApp('Back to Rates page')
    this.router.navigate(['/country_rates/']);
   }
   else
   {
    //this.closeApp('Back to Recharge page')
    this.router.navigate(['/plans/'+this.enc_key]);
   }
   
}
  goBack() 
  {

    
  

    console.log( "Process "+ this.processType);
       
      if(this.processType == 'pay')
      {
        this.gotopreviouPage()
      }
      else if( this.havingExistingCard == false && this.processType == '')
      {
        this.gotopreviouPage();
      }
      else
      {
        
        this.addNewCard = false;
        this.showCartInfo = false;
        this.editCard = false;
        this.havingExistingCard = true;
        this.processType = 'pay';
        this.selectedCard= null;
        this.getCards();
      }

     /* if(this.processType == 'newCrd')
      {
        this.addNewCard = false;
        this.showCartInfo = false;
        this.editCard = false;
        this.havingExistingCard = true;
        this.processType = 'pay';
      }
      if(this.processType == 'editCrd')
      {
        this.editCard = false;
        this.havingExistingCard = true;
        this.showCartInfo = false;
        this.processType = 'pay';
      }

      if(this.processType == 'showCartInfo')
      {
        this.showCartInfo = false;
        this.editCard = false;
        this.addNewCard = false;
        this.havingExistingCard = true;
        this.processType = 'pay'; 
        alert(this.processType);
      }*/
  }
  addNewOrPay()
  {
    this.processWithPaypal = false;
    if(this.processType == 'pay')
    {
      
       
      if (!this.selectedCard.Cvv || this.selectedCard.Cvv =='' || this.selectedCard.Cvv.length < 3)
      {
        this.cardCvvError = true;
        
        return false;
      }
      else{
        this.cardCvvError = false;
        this.processType = 'showCartInfo';
        this.addNewCard = false;
        this.editCard = false;
        this.showCartInfo = true;
      }
      

     // console.log( this.selectedCard);
    }

    if(this.processType == 'newCrd')
    {
      this.showCartInfo = false;
      this.addNewCard = true;
      
    }
    if(this.processType == 'editCrd') 
    {
      this.showCartInfo = false;
      this.editCard = true;
    }
    
  }
  getlast4digit()
  {
   if(this.selectedCard)
   {
    var last = this.selectedCard.CardNumber.substr(-4);
    return ' ....'+last;
   }
    
  }
  addCreditCardClick(obj:any)
  {
     
  }
  onPaymentInfoFormSubmit(creditCard: CreditCard) {
   // this.onCreditCardPayment(creditCard);
   this.selectedCard = creditCard;
   this.addNewCard = false;
   this.editCard = false;
   this.showCartInfo = true;
   this.processType = 'showCartInfo'
   

  }
  processPayment()
  {
   this.rechargeNow(this.selectedCard);
  }
  rechargeNow(creditCard: CreditCard)
  {
      this.paymentSubmitted = true;
        let trans_type = '';
        if (this.selectedCard === null)
        return;
      if (this.selectedCard.Cvv.length < 3)
        return;
        
      this.selectedCard.CardHolderName = `${this.billingInfo.FirstName} ${this.billingInfo.LastName}`;
      this.selectedCard.FullName = this.selectedCard.CardHolderName;
      this.selectedCard.PhoneNumber = this.billingInfo.Address.HomePhone;


      let planOrderInfo: ICheckoutOrderInfo;
      if (this.currentCart.transactiontype === TransactionType.Recharge) {
        planOrderInfo = new RechargeOrderInfo();
        trans_type = 'Recharge';
      } else if (this.currentCart.transactiontype === TransactionType.Activation || this.currentCart.transactiontype === TransactionType.Sale) {
       // planOrderInfo = new ActivationOrderInfo();
        planOrderInfo = new RechargeOrderInfo();
        trans_type = 'Sale';
      } else if (this.currentCart.transactiontype === TransactionType.Topup) {
        planOrderInfo = new MobileTopupOrderInfo();
        trans_type = 'Topup';
      }
      else if(this.currentCart.transactiontype === TransactionType.MR )
      {
        planOrderInfo = new RechargeOrderInfo();
        trans_type = 'Recharge';
      }

      if (this.currentCart.transactiontype === TransactionType.Activation) {
        const cart = this.currentCart as NewPlanCheckoutModel;
        cart.pinlessNumbers = [creditCard.PhoneNumber];
      }

      

      planOrderInfo.creditCard = creditCard;
      planOrderInfo.checkoutCart = this.currentCart;


      localStorage.setItem('selectedCard',  creditCard.CardId.toString());
       
      planOrderInfo.creditCard = creditCard;
      planOrderInfo.checkoutCart = this.currentCart;
        
        var first_fivenum = creditCard.CardNumber.substring(0, 5);
        this.braintreeService.testProcess(first_fivenum, trans_type).subscribe( (data: ApiProcessResponse)=>{ 
          this.paymentProcessor = data.ThreeDSecureGateway; 
            if(data.Use3DSecure)
              {
                
                if (!isNullOrUndefined(planOrderInfo.checkoutCart.couponCode) && planOrderInfo.checkoutCart.couponCode.length > 0) {
                 /* this.validateCoupon(planOrderInfo.checkoutCart.getValidateCouponCodeReqModel(planOrderInfo.checkoutCart.couponCode))
                    .then((res: ValidateCouponCodeResponseModel) => {
                      if (res.Status) 
                      {} else {
                        this.handleInvalidCouponCodeError();
                      }
                    }).catch(err => {
                      this.handleInvalidCouponCodeError();
                    });*/
                        if(planOrderInfo.checkoutCart.couponCode == 'FREETRIAL')
                        {
                          /********** Use3DSecure :false  then process transaction directly **********/
                          let service: TransactionMobProcessBraintreeService = this.transactionProcessBraintree;
                          let checkoutInfo = this.transactionService.processPaymentNormal(planOrderInfo);
                        }
                        else
                        {
                          if(this.paymentProcessor=='BrainTree')
                          {
                            this.process_info = this.transactionService.processMobPaymentToBraintree(planOrderInfo );
                           // let return_msg = btoa(this.process_info);
                            //this.showAndroidToast(return_msg); 
                          }
                          else
                          {
                            this.process_info = this.transactionService.processPaymentToCentinel(planOrderInfo);
                            //let return_msg = btoa(this.process_info);
                            //this.showAndroidToast(return_msg);
                          }
                        }
                      
                } 
                else 
                {
              
                
                    if(this.paymentProcessor== 'BrainTree')
                    {
                    this.process_info = this.transactionService.processMobPaymentToBraintree(planOrderInfo);
                     //let return_msg = btoa(this.process_info);
                     //this.showAndroidToast(return_msg); 
                    }
                    else
                    {
                      this.process_info = this.transactionService.processPaymentToCentinel(planOrderInfo);
                     // let return_msg = btoa(this.process_info);
                     //this.showAndroidToast(return_msg); 
                    }
                } 
            }
            else
            {
              /********** Use3DSecure :false  then process transaction directly **********/
              let service: TransactionMobProcessBraintreeService = this.transactionProcessBraintree;
              this.process_info = this.transactionService.processPaymentNormal(planOrderInfo);
              //let return_msg = btoa(this.process_info);
               //this.showAndroidToast(return_msg); 
              
            }
          });
     
     
  }

  returnPaymentResp()
  { 
   
    let return_msg = localStorage.getItem('payment_response');
     let payment_type = localStorage.getItem('payment_type'); 
    // let payment_type = 'Top-up';
    localStorage.removeItem('payment_response');
     console.log("Payment response is as given bellow ");
     console.log(return_msg);
    /* this.showAndroidToast(return_msg); */
      

     const dialogRef = this.dialog.open(DialogCofirmComponent, {
      data: {
        success: 'success',
        payment_type:payment_type
      }
    });
    let card = this.selectedCard;
    dialogRef.afterClosed().subscribe(result => {
      this.showAndroidToast('Mobile web view closed by client. '+ return_msg);
    });

  }

  whatiscvv()
   {
    this._bottomSheet.open(BottomUpComponent);
    /*const dialogRef = this.dialog.open(BottomUpComponent, {
      data: {
        success: 'success'
      }
    });*/
     
   }

  showAndroidToast(obj)
  {
  if(this.platform == 'ios')
    {
      this.iosFuntionRevert(obj);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(obj);
    }
  }
   // Validate coupon code.
   validateCoupon(req: ValidateCouponCodeRequestModel): Promise<ValidateCouponCodeResponseModel | ApiErrorResponse> {
    return this.transactionService.validateCouponCode(req).toPromise();
  }

  handleInvalidCouponCodeError() {
    let error = new ErrorDialogModel();
    error.header = 'Invalid Coupon Code';
    error.message = 'Please check your information and try again.';

    this.currentCart.isHideCouponEdit = false;
    this.currentCart.couponCode = null;

    this.openErrorDialog(error);
  }

  openErrorDialog(error: ErrorDialogModel): void {
    this.dialog.open(ErrorDialogComponent, {
      data: { error }
    });
  }

  showCardEdit()
  {
    if(this.selectedCard.CardId)
    {
       this.processType = 'editCrd';
       this.showCartInfo = false;
       this.editCard = true;
       this.customerService.EditCreditCard(this.selectedCard).subscribe(data => {
        this.cardBillingAddress(data);
      });
    }
  }

/*********************/
showCartInfoClick()
{
      this.cardCvvError = false;
      this.processType = 'showCartInfo';
      this.addNewCard = false;
      this.editCard = false;
      this.showCartInfo = true;
     this.processWithPaypal = true;
}
  // Intitiate paypal config
  private initPaypalConfig(): void {
    if (isNullOrUndefined(this.currentCart)) {
      return;
    }

    this.payPalConfig = {
      currency: this.currentCart.currencyCode.toString(),
      clientId: environment.paypalClientId,
      createOrderOnServer: () => {
        return this.validateAndGeneratePaypalOrder();
      },
      
      advanced: {
        updateOrderDetails: {
          commit: true
        },
        extraQueryParams: [
          { name: 'intent', value: 'authorize' },
          { name: 'disable-funding', value: 'credit,card' }
        ]
      },
      onApprove: (data, actions) => {
        // console.log('onApprove - transaction was approved, but not authorized', data, actions);
        actions.order.get().then(details => {
          //  console.log('onApprove - you can get full order details inside onApprove: ', details);
        });
      },
      authorizeOnServer: (data, actions) => {
        console.log('authorizeOnServer - you should probably inform your server to autorize and capture transaction at this point', data);
        return this.onPaypalPaymentApprove(data);
      },
      // onClientAuthorization: (data) => {
      //     console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
      // },
      onCancel: (data, actions) => {
        // console.log('OnCancel', data, actions);

      },
      onError: err => {
        // console.log('OnError', err);
      },
      onClick: () => {
        // console.log('onClick');
      },
    };
  }
 
  validateAndGeneratePaypalOrder(): Promise<string> {
    if (!isNullOrUndefined(this.currentCart.couponCode) && this.currentCart.couponCode.length > 0) {
      return this.validateCoupon(this.currentCart.getValidateCouponCodeReqModel(this.currentCart.couponCode)).then((res: ValidateCouponCodeResponseModel) => {
        if (res.Status) {
          return this.transactionService.generatePaypalOrder(this.currentCart.getTransactionReqModel()).toPromise();
        }
        else {
          this.handleInvalidCouponCodeError();
           new Error("Invalid coupon code");
        }
      })
    }
    else {
      return this.transactionService.generatePaypalOrder(this.currentCart.getTransactionReqModel()).toPromise();
    }

  }

  /* On paypal payment approve. */
  onPaypalPaymentApprove(data: IOnApproveCallbackData): Promise<any> {

    const checkOutOrderInfo: IPaypalCheckoutOrderInfo = {
      checkoutCart: this.currentCart,
      orderId: data.orderID,
      paypalPayerId: data.payerID,
      paymentTransactionId: data.orderID
    };

    console.log('paypal checkout order info', checkOutOrderInfo);
    //this.transactionProcessFacade.processPaypalTransaction(this.currentCart.transactiontype, checkOutOrderInfo);

    this.transactionProcessBraintree.processPaypalTransaction(this.currentCart.transactiontype, checkOutOrderInfo);
    return of(true).toPromise();
  }
/***************/

}
