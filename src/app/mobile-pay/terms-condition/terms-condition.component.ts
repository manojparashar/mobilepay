import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare var jQuery: any;
interface Window {
  webkit?: any;
  androidCallBackInterface:any;
}
declare var window: Window;
@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.scss']
})
export class TermsConditionComponent implements OnInit {
  platform:string='';
  enc_key:any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    //this.loadScript('assets/js/livechat.js');

    this.enc_key    = this.route.snapshot.paramMap.get('post_data');
    localStorage.setItem('web_enc_key', this.enc_key);
 
    let data_list   =  JSON.parse(atob(this.enc_key));

    let device_info         = data_list.de; 
    this.platform = device_info;

  }
  public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  goBack(obj:any)
  {
      var msg = '';
      if(obj !='')
      {
        msg = obj;
      }
      else{
        msg = 'Mobile web view closed by client'
      }
      msg = 'Mobile web view closed by client'
      this.closeApp(msg);
  }

  closeApp(obj:any)
  {
    var msg = '';
    
    if(obj !='')
    {
      msg = obj;
    }
    else{
      msg = 'Mobile web view closed by client'
    }
    msg = 'Mobile web view closed by client'
    if(this.platform == 'ios')
    {
      this.iosFuntionRevert(msg);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(msg);
    }
 }
  showAndroidToast(obj)
  {
  if(this.platform == 'ios')
    {
      this.iosFuntionRevert(obj);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(obj);
    }
  }
   
  iosFuntionRevert(resultdata)
  {
    console.log(resultdata);
    try {
         var message = resultdata;
          window.webkit.messageHandlers.callbackHandler.postMessage(message);
          } 
          catch(err) {
          }

  }

  androidFuntionRevert(toast) {
    console.log(toast);
    try {
         window.androidCallBackInterface.callBackFormData(toast);
        } catch(err) {
       }
    }

}
