import { Component, OnInit, Injector, Input } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import { ViewChild, AfterViewInit, NgZone} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
 
import { ElementRef } from '@angular/core';
import { AuthenticationService } from '../../core/services/auth.service';
import { CountriesService } from '../../core/services/country.service';
 
import { RazaSnackBarService } from '../../shared/razaSnackbar.service';
//import { isValidaEmail, isValidPhoneNumber, autoCorrectIfPhoneNumber, isValidPhoneOrEmail } from '../../shared/utilities';
//import { environment } from '../../../environments/environment';
import { AppBaseComponent } from '../../shared/components/app-base-component';

 
//import { CreditCardValidators } from 'angular-cc-library';
//import { Observable } from 'rxjs/internal/Observable';
 
 
import { RazaEnvironmentService } from '../../core/services/razaEnvironment.service';
import { CustomerService } from '../../accounts/services/customerService';
import { RazaSharedService } from '../../shared/razaShared.service';

import { State, BillingInfo } from '../../accounts/models/billingInfo';
import { CodeValuePair } from '../../core/models/codeValuePair.model';
import { Country } from '../../shared/model/country';
import { ICheckoutModel, NewPlanCheckoutModel } from '../../checkout/models/checkout-model';
import { TransactionType } from '../../payments/models/transaction-request.model';
import { CreditCard } from '../../accounts/models/creditCard'; //BraintreeCard
 
import { TransactionService } from '../../payments/services/transaction.service';
import { BraintreeService } from '../../payments/services/braintree.service';
import { BraintreeCard } from '../../accounts/models/braintreeCard';

import { HttpClient, HttpHeaders } from '@angular/common/http';
 
 import { TransactionMobProcessBraintreeService } from "../../payments/services/transaction-mob-process-braintree.service";
 //import { ValidateCouponCodeResponseModel, ValidateCouponCodeRequestModel } from '../../payments/models/validate-couponcode-request.model';
 import { RechargeCheckoutModel } from '../../checkout/models/checkout-model';
 import { Plan } from '../../accounts/models/plan';
 import { PlanService } from '../../accounts/services/planService';
 import { CheckoutService } from '../../checkout/services/checkout.service';

 import { RechargeService } from '../../recharge/services/recharge.Service';

 //import { ErrorDialogModel } from '../../shared/model/error-dialog.model';
 //import { ErrorDialogComponent } from '../../shared/dialog/error-dialog/error-dialog.component';
 import { CurrentSetting } from "../../core/models/current-setting";
 import { PromotionResolverService } from '../../deals/services/promotion-resolver.service';

 import { PromotionsService } from '../../deals/services/promotions.service';
 import { PromotionPlanDenomination } from '../../deals/model/promotion-plan-denomination';
import { data, trim } from 'jquery';
import { OperatorDenominations } from '../../mobiletopup/model/operatorDenominations';
 
import { MobiletopupService } from '../../mobiletopup/mobiletopup.service';
import { MobileTopupCheckoutModel } from '../../checkout/models/checkout-model';
import { mobileTopupModel } from '../../mobiletopup/model/mobileTopupModel';
import { isNullOrUndefined } from "../../shared/utilities";
import { ApiErrorResponse } from '../../core/models/ApiErrorResponse';
import { Location } from '@angular/common';
import { startWith, map, catchError } from 'rxjs/operators';
import { ErrLoginComponent } from '../dialog/err-login/err-login.component'; 
import { TopupErrorComponent } from '../dialog/topup-error/topup-error.component';

 declare var jQuery: any;
 declare var $: any;
interface Window {
  webkit?: any;
  androidCallBackInterface:any;
}
declare var window: Window;
@Component({
  selector: 'app-topup',
  templateUrl: './topup.component.html',
  styleUrls: ['./topup.component.scss']
})
export class TopupComponent extends AppBaseComponent implements OnInit {
  hide_header_footer:boolean=true;
  userPhone : any;
  userPass : any;
  userToken : any;
  enc_key:any;
  key_arr:any;
  planId: string;
  creditCard:CreditCard;
  paymentDetailForm: FormGroup;
  billingInfoForm: FormGroup;
  paymentInfoForm: FormGroup;
  existingCreditCardForm: FormGroup;

  countries: Country[]
  fromCountry: Country[] = [];
  months: CodeValuePair[];
  years: CodeValuePair[];
  states: State[] = [];
  customerSavedCards: CreditCard[];
  havingExistingCard: boolean = false;
  selectedCard: CreditCard;
  selectedCardPay: CreditCard;
  braintreeCard:BraintreeCard;
  cvvStored: string;
  billingInfo: BillingInfo;
  countryFromId: number = 1;
  autoRefillTipText: string;
  braintreeToken:any;
  paymentProcessor:any;
  countryId:number;
  IsEnableFreeTrial:boolean=true;
  autocompleteInput: string;
queryWait: boolean;
checked:boolean=true;
balance:number=0;
address: Object;
establishmentAddress: Object;
formattedAddress: string;
formattedEstablishmentAddress: string;
phone: string;
search_country:any='US';
search_country_id:any=1;
plan: Plan;
isEnableOtherPlan: boolean = false;
isAutoRefillEnable: boolean = false;
paymentSubmitted: boolean;
amount:any=10;
processType:any=''; 
addNewCard:boolean=false;
editCard:boolean=false; 
showCartInfo:boolean=false;
currentCart: ICheckoutModel;
process_info:any='';
platform:any;
cardCvvError:boolean=false;
rechargeAmounts: number[];

DeviceId:string;
DeviceName:string;
DeviceModel:string;
DeviceType:string;
AppVersion:string;

promotionCode:string='';
denominationList: any[];
promotionPlan  : any[];
pCardId : number;
pCardName : string;
pServiceCharge : number;
pCouponCode: string;
pIsEditCoupon : any;
showContinue:boolean=false;
logintrial : number=0;
topup_no :string;

allCountry: Country[];
  mycountryName: string;
  mycountryId: number;
  filteredCountry: Observable<Country[]>;
  // autoControl = new FormControl();
  mobileTopupData: mobileTopupModel;
  operatorDenominations: OperatorDenominations[];
  mobileTopupForm: FormGroup;
  amountSent: number;
  countrySelect = new FormControl();
  currentSetting$: Subscription;
  currentSetting: CurrentSetting;
  isTopUpEnable: boolean = false;
  pinnumber:number;
  iso:string;
  countryName:String;
  showError:boolean=false; 
  plans_list:any=[];
  ResponseMessage:string='';
  topup_country:any=0;
  
  showOperators:boolean=false;
  operatorsList:any=[];
  countryTo:number=0;
  currentOperator:string='';
/**********EOF Google place search **********/
private static _setting: CurrentSetting;
constructor( 
  
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private _formBuilder: FormBuilder,
    
    private countriesService: CountriesService,
    private dialog: MatDialog,
    private snackBarService: RazaSnackBarService,

    public zone: NgZone,
    private transactionService: TransactionService, 
    private braintreeService: BraintreeService,
    private formBuilder: FormBuilder,
    private countryService: CountriesService,
    private razaEnvService: RazaEnvironmentService,
    private customerService: CustomerService,
    
    private razaSnackbarService: RazaSnackBarService,
    private authService: AuthenticationService,
    private httpClient: HttpClient,
    private razaSharedService: RazaSharedService,
    private rechargeService: RechargeService,
    private transactionProcessBraintree: TransactionMobProcessBraintreeService,
    private planService: PlanService,
    private checkoutService: CheckoutService,
    private promotionResolverService: PromotionResolverService,
    private promotionService: PromotionsService,
    private mobileTopupService: MobiletopupService,
    
	private location:Location,
    _injector: Injector,
     
  ) {
    
    super(_injector);
    
  }
  @ViewChild('privatUserCheckbox') privatUserCheckbox: ElementRef;
  
  ngOnInit(): void {
 
    if(localStorage.getItem('web_enc_key'))
    {
      this.enc_key    = localStorage.getItem('web_enc_key');
     }
    else
    {
      this.enc_key    = this.route.snapshot.paramMap.get('post_data');
      }
    localStorage.setItem('enc_key', this.enc_key);
    
    //this.enc_key  = 'ewoicGhvbmUiOiIzMTI5NzU4NTQyIiwKInBhc3N3b3JkIjoiMTIzNDU2IiwKImNvdW50cnlfaWQiOjEsCiJwbGF0Zm9ybSI6ICJpb3MiLAoiYW1vdW50IjogIjUiCn0=';
    let data_list   =  JSON.parse(atob(this.enc_key));
     
    this.key_arr    = data_list;
     
    this.userPhone  = (data_list.phone)?data_list.phone:'';;
    this.userPass   = (data_list.password)?data_list.password:'';
    this.userToken = (data_list.token)?data_list.token:'';
    this.promotionCode = (data_list.coupon_code)?data_list.coupon_code:'';
     
    
    if(!this.authService.isAuthenticated ())
    {
      
      this.processLoginSteps();
  }
  else{
    var d = new Date();
    var stime = d.getTime();
       
    var seconds = (stime - parseFloat(localStorage.getItem('last_login_time')) ) / 1000;
     
      if(this.userToken == localStorage.getItem('prev_token'))
      {
          this.getUserPlans();
      }
      else
      {
       
          this.processLoginSteps();
      }
      
     
  }

  
    this.search_country_id  = data_list.country_id;
    this.setCurrentCountry();
    this.countryFromId      = this.search_country_id;
    let device_info         = data_list.de;
    
    this.currentSetting$ = this.razaEnvService.getCurrentSetting().subscribe(res => {
      this.currentSetting = res;
    });

    if(device_info.includes("||"))
    {
      var dres = device_info.split("||")
      this.platform           = dres[4];
      this.DeviceType         = dres[4];
    }
    else
    {
      this.platform           = device_info;
      this.DeviceType         = device_info;
    }


    this.getCountries();
    //this.platform = device_info;
     //this.loginWithToken();
     this.search_country_id  = data_list.country_id;
    this.setCurrentCountry();
    this.countryFromId      = this.search_country_id;
    let top_no              = ( data_list.top_no && data_list.top_no !='' ) ?data_list.top_no:0;
    this.topup_no           = top_no;

    this.topup_country  = (data_list.t_c)?data_list.t_c:'';

    this.mycountryId = 0;
    this.mobileTopupForm = this.formBuilder.group({
      countryTo: ['', [Validators.required]],
      phoneNumber: ['', Validators.required],
      topUpAmount: [null]
    });
    this.mobileTopupForm.get('countryTo').valueChanges.subscribe(res => {
      this.changeNumber();
    });
    
    this.filteredCountry = this.mobileTopupForm.get('countryTo').valueChanges
    .pipe(
      startWith<string | Country>(''),
      map(value => typeof value === 'string' ? value : value.CountryName),
      map(CountryName => CountryName ? this._filter(CountryName) : this.allCountry)
    );
  

     /*******/
    /* $(document).ready(function(){
      var settings = {
        "url": "https://services.razacomm.com/razawebservices/raza_mobileapp_services.asmx",
        "method": "POST",
        "timeout": 0,
         
        "headers": {
          "Content-Type": "text/xml",
          "Access-Control-Request-Headers": "*"
        },
        "data": "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n    <soap:Header>\r\n    <Raza_AuthHeader xmlns=\"http://tempuri.org/\">\r\n    <AuthUsername>AjinTestUser</AuthUsername>\r\n    <AuthPassword>AJTeStPWD!FSTr</AuthPassword>\r\n    </Raza_AuthHeader>\r\n    </soap:Header>\r\n    <soap:Body>\r\n    <GetOnlinePlanRates_Json1 xmlns=\"http://tempuri.org/\">\r\n      <customerID>2008999</customerID>\r\n      <Pin>1112267924</Pin>\r\n      <Alphabet>A</Alphabet>\r\n    </GetOnlinePlanRates_Json1>\r\n    </soap:Body>\r\n    </soap:Envelope>",
      };
    
      var settings2 = {
        "url": "https://services.razacomm.com/razawebservices/raza_mobileapp_services.asmx",
        "method": "POST",
        "timeout": 0,
        //"crossDomain": true,
        "headers": {
          "Content-Type": "text/xml",
          "Access-Control-Request-Headers": "*"
        },
        "data": "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n    <soap:Header>\r\n    <Raza_AuthHeader xmlns=\"http://tempuri.org/\">\r\n    <AuthUsername>AjinTestUser</AuthUsername>\r\n    <AuthPassword>AJTeStPWD!FSTr</AuthPassword>\r\n    </Raza_AuthHeader>\r\n    </soap:Header>\r\n    <soap:Body>\r\n    <GetOnlinePlanRates xmlns=\"http://tempuri.org/\">\r\n      <customerID>2008999</customerID>\r\n      <Pin>1112267924</Pin>\r\n      <Alphabet>A</Alphabet>\r\n    </GetOnlinePlanRates>\r\n    </soap:Body>\r\n    </soap:Envelope>",
      };
       
      
     
      var json_arr = [];
      $.ajax(settings2).done(function (xmlString) {
       
        if(xmlString)
        {
          
          $(xmlString).find('GetOnlinePlanRatesResult').each(function(){
            var PlanId = $(this).find('PlanId').text();
            var CountryFromId = $(this).find('CountryFromId').text();
            var CurrencyCode = $(this).find('CurrencyCode').text();
            var ExchangeRate = $(this).find('ExchangeRate').text();
    
          })
          
          $(xmlString).find('OnlinePlanRate').each(function(){
            var sTitle = $(this).text();
            
            var CountryToId = $(this).find('CountryToId').text();
            var CountryToName = $(this).find('CountryToName').text();
            var CallingRate = $(this).find('CallingRate').text();
    
            var rate= {CountryToId:CountryToId, CountryToName:CountryToName,CallingRate:CallingRate }
            // console.log(rate);
             json_arr.push(rate);
             
             //console.log(this.plans_list);
            // this.printJson(rate);
          });
     
        }
        
      });
     
        
    // this.plans_list = this.countryService.getSoap();
    }.bind(this)); */  
     /****** */
     
  }
  printJson(obj:any[])
  {
    this.plans_list.push(obj) ;
  }
 
  private _filter(value: any): Country[] {
    const filterValue = value.toLowerCase();
     
    var data =  this.allCountry.filter(option => option.CountryName.toLowerCase().indexOf(filterValue) === 0);
    //console.log(data);
    return data;
  }
  processLoginSteps()
  {
    localStorage.setItem('prev_token', this.userToken);
    localStorage.removeItem('currentUser');
    var d = new Date();
      var gtime = d.getTime()+" ";
       localStorage.setItem('last_login_time', gtime);
 
    if(this.userPhone == '13129758542')
    //if(this.userPhone == '13129758542' )
    {
      this.autoLoginUser();
    }
    else
    {
      this.loginWithToken();
    }
   
  }

  setCurrentCountry()
  {
    //localStorage.removeItem('session_key');
      let countries_list = [{CountryId:1,CountryName:"U.S.A",CountryCode:'1'},
    {CountryId:2,CountryName:"CANADA",CountryCode:'1'},
    {CountryId:3,CountryName:"U.K",CountryCode:'44'}];

    for(var j = 0; j <  countries_list.length;j++){
      if(countries_list[j].CountryId == this.search_country_id)
      { 
        const setting = new CurrentSetting();
        setting.country = countries_list[j];
         this.razaEnvService.setCurrentSetting(setting);
      }
  } 
}
  autoLoginUser() 
  {
    const phoneOrEmail = this.userPhone;
    /*this.executeCaptcha('login').subscribe((token) =>{},(error) => {
        console.log(error.error_description);
          //console.log("error "+error); 
         // this.closeApp();
    }
    )*/
    let body = {
        username: phoneOrEmail,
        password: this.userPass,
        captcha: ''
      };
      
      this.authService.login(body, false, "Y").subscribe((response) => {
        if (response != null) {
           
            this.getUserPlans(); 
           
           
        
        }  
      },
        (error) => {
          console.log(error.error_description);
         // this.closeApp();
        });
     
  }

   
continue()
{
   //this.router.navigateByUrl('/mobile_pay');
 
   this.router.navigate(['/mobile_pay']);
   
}
getSelectedClass(amt:any)
{
  if(this.amount == amt)
  {
    return 'selected-dom';
  }
  else{
    return '';
  }
}
 
  selectAmount(id: any) {
    //this.id = id;
  }
  private getCountries() {
    
    this.countryService.getAllCountries().subscribe(
      (data: Country[]) => {
        this.allCountry = data;
       //
      
      },
      (err: ApiErrorResponse) => console.log(err),
    );
  }

  changeNumber() {
    this.mobileTopupForm.get('topUpAmount').updateValueAndValidity();
    this.mobileTopupForm.get('phoneNumber').enable();
/*
    this.mobileTopupForm.patchValue({
      phoneNumber: '',
      topUpAmount: null
    });
*/
    this.mobileTopupForm.patchValue({ topUpAmount: null });

    this.mobileTopupData = null;
    this.isTopUpEnable = false;
  }

  changeCountry() {
    this.mobileTopupForm.get('topUpAmount').updateValueAndValidity();
    this.mobileTopupForm.get('phoneNumber').enable();

   // this.mobileTopupForm.patchValue({  phoneNumber: '', countryTo:'', topUpAmount: null });
    this.mobileTopupForm.patchValue({ topUpAmount: null });
    this.mobileTopupData = null;
    this.isTopUpEnable = false;
  }
  displayFn(country?: Country): string | undefined {
    return country ? country.CountryName : undefined;
  }
 onSelectCountrFrom(country: Country) {
   // console.log(country);
  this.countryName = country.CountryName;
  this.mycountryId = country.CountryId; 
  }
    unsetFlag() {
    
    
      this.mycountryId= -1; 
      }
 

  validateAmountSelection() {
    if (!this.isTopUpEnable) {
      return false;
    }
    const amount = this.mobileTopupForm.get('topUpAmount').value;
     
    if (isNullOrUndefined(amount)) {
      this.mobileTopupForm.get('topUpAmount').setErrors({ Amount_Req: true })
      return false
    }

    return true;
  }
  get operatorImage() {
    return `assets/images/operators/${this.mobileTopupData.Operator.toLowerCase()}.png`;
  }
  onMobileTopupFormSubmit() {
    // stop here if form is invalid
    if (!this.isTopUpEnable) {
      this.getTopUpOperatorInfo();
      return;
    }
    this.validateAmountSelection();
    console.log(this.mobileTopupForm);
    if (!this.mobileTopupForm.valid) {
     
      return;
    }

    const checkoutModel: MobileTopupCheckoutModel = new MobileTopupCheckoutModel();

    checkoutModel.transactiontype = TransactionType.Topup;
    checkoutModel.country = this.mobileTopupForm.get('countryTo').value as Country; // this.autoControl.value.countryTo as Country;
    checkoutModel.topupOption = this.mobileTopupForm.value.topUpAmount as OperatorDenominations;
    checkoutModel.currencyCode = this.currentSetting.currency;
    checkoutModel.phoneNumber = this.mobileTopupForm.get('phoneNumber').value;
    checkoutModel.operatorCode = this.mobileTopupData.OperatorCode;
    checkoutModel.countryFrom = this.currentSetting.currentCountryId;
    checkoutModel.isHideCouponEdit = true;

    this.checkoutService.setCurrentCart(checkoutModel);
    localStorage.setItem('currentCart', JSON.stringify(checkoutModel))
    localStorage.setItem('topupPhone', checkoutModel.country.CountryCode+checkoutModel.phoneNumber);
    localStorage.setItem('topupAmount', checkoutModel.currencyCode+" "+checkoutModel.topupOption.UsDenomination);
    

 console.log(checkoutModel);
 this.router.navigate(['/mobile_pay']);
  }

  getInitialTopUpOperatorInfo() {
     
    this.showError = false;
    let phoneNumberWithCode: string = this.topup_no;
 
    this.mobileTopupService.GetMobileTopUp(this.countryFromId, phoneNumberWithCode).subscribe(
      (data: mobileTopupModel) => {
        //console.log("step 1");
        
        if(data && data.OperatorDenominations && data.OperatorDenominations[1])
        {
          
          //console.log("step 2");

          this.operatorsList = data.AvaliableOperators;
          this.countryTo = data.CountryId;
            for (let i = 0; i < this.allCountry.length; i++) 
            {
              if(this.allCountry[i].CountryId ==  data.CountryId)
              {
                //console.log("step 3");
                 
                  this.onSelectCountrFrom(this.allCountry[i]);
                  console.log(this.allCountry[i].CountryCode);
                  var ccode = this.allCountry[i].CountryCode.replace('-', '');
                  
                  var length = ccode.length;
                  var phone =   this.topup_no.substring(length);
                  
                  this.mobileTopupForm.patchValue({  phoneNumber:phone, countryTo:this.allCountry[i] });
                   
                  this.onClickAmountOption(data.OperatorDenominations[1]);
                
                  this.mobileTopupData = data;
                   
              }
          }
          
          this.isTopUpEnable = true;
        }
        else
        {
           
         this.noDenominationMsg();
         this.showErrMsg(data);
          var prefix = '';
         for (let i = 0; i < this.allCountry.length; i++) 
         {
         
          if(this.allCountry[i].CountryCode ==  this.topup_country)
            {
              prefix = this.allCountry[i].CountryCode;
              this.mobileTopupForm.patchValue({  countryTo:this.allCountry[i]});
              this.onSelectCountrFrom(this.allCountry[i]);
            }
          }
          if(prefix.length > 0)
          {
            //var new_phone = parseFloat( phoneNumberWithCode.toString().substring(prefix.length));
            var first_nos = phoneNumberWithCode.slice(0, prefix.length)
            if(first_nos == prefix)
            {
              phoneNumberWithCode = phoneNumberWithCode.slice(prefix.length);
            }
            
            
          }
          this.mobileTopupForm.patchValue({  phoneNumber:phoneNumberWithCode});
          
        }
      },
      (err: ApiErrorResponse) => {
        console.log(err)
        this.noDenominationMsg();
      
      },
    );
  }

  showErrMsg(data:any)
  {
    if(data && data.ResponseCode && data.ResponseCode != 1)
    {
      this.ResponseMessage= data.ResponseMessage;

      const dialogRef = this.dialog.open(TopupErrorComponent, {
        data: {
          success: 'success',
          error_msg:this.ResponseMessage
        }
      });
       
      dialogRef.afterClosed().subscribe(result => {
        this.showAndroidToast('Mobile web view closed by client.');
      });

    }
    else{
      this.ResponseMessage = '';
    }
    //ResponseCode: "0"
    //ResponseMessage: "Mobile Top-Up is unavailable for this country."
  }

  
 

  noDenominationMsg()
  {
   // this.mobileTopupForm.patchValue({  phoneNumber:'', countryTo:'' });
    this.mobileTopupData = null;
    this.showError = true;

    

  }
  getTopUpOperatorInfo() {
   
    //let phoneNumberWithCode: string = this.topup_no;
    this.showError = false;
    const formValue = this.mobileTopupForm.value;
    const country: Country = formValue.countryTo;
    
    let phoneNumberWithCode: string = country.CountryCode + formValue.phoneNumber;
    this.topup_no = phoneNumberWithCode;
    this.mobileTopupService.GetMobileTopUp(this.countryFromId, phoneNumberWithCode).subscribe(
      (data: mobileTopupModel) => {
        if(data && data.OperatorDenominations && data.OperatorDenominations[1])
        {
          this.mobileTopupData = data;
		      this.operatorsList = data.AvaliableOperators;
          this.countryTo = data.CountryId;
          this.onClickAmountOption(data.OperatorDenominations[1]);
          this.isTopUpEnable = true;
        }
        else{
          this.noDenominationMsg();
          this.showErrMsg(data)
        }
        // this.setSelectedCountry();
        
        
      },
      (err: ApiErrorResponse) => {
        this.noDenominationMsg();
        console.log(err)},
    );
  }

  getChecked(amt:number)
  {
    if(this.mobileTopupForm.get('topUpAmount').value)
    {
      if( amt == this.mobileTopupForm.get('topUpAmount').value.UsDenomination)
      {
        return 'selected-plan';
      }
      else{
        return '';
      }
    }
  else
    return false;
  }
  onClickAmountOption(item: any) {
    
    this.mobileTopupForm.get('topUpAmount').setValue(item);
    this.showContinue = true;
  }


  getUserPlans()
  {

    var phone =   this.userPhone;
    if(this.search_country_id == 2 || this.search_country_id == 1) 
  {
    
   

      if(this.search_country_id == 3 || this.search_country_id == 26)
      {
        phone =   phone.substring(2);
      }
      else
      {
        phone =   phone.substring(1);
      }
  }
    localStorage.setItem('pnone_no', phone);   
    this.planService.getPlanInfo(phone).subscribe(
      (res:any)=>{
        console.log(res);
       
        this.plan = res;
        
          this.getInitialTopUpOperatorInfo()
        
        
        this.planId = this.plan.PlanId;
        this.balance = this.plan.Balance;
        /*this.planService.getPlan(this.planId).subscribe(
          (res: Plan) => this.plan = res,
          err => console.log(err),
          () => ''
        );*/
        //PlanId
        //this.currentPlanName = res.CardName;
       // this.currentBalance = res.Balance;
      }
    );

    
  }

  getPromotionPlans(promotionCode: string, countryId: number)
  {
   
       this.promotionService.getPromotion(countryId, promotionCode).subscribe(
        res  => {
          //this.promotionPlan = res.Plans[0];
          this.denominationList = res.Plans[0].Denominations;
          this.pCardId = res.Plans[0].CardId;
          this.pCardName = res.Plans[0].CardName;
          this.pServiceCharge  = res.Plans[0].ServiceCharge;
          this.pCouponCode = res.Plans[0].CouponCode;
          this.pIsEditCoupon = res.Plans[0].IsEditCoupon
          console.log(this.denominationList);
          this.buyPlan(this.denominationList[0]);
          this.showContinue = true;
        }
      )
  }

  buyPlan(denomination: PromotionPlanDenomination) {

    this.amount = denomination.Price;

    
    const model: NewPlanCheckoutModel = new NewPlanCheckoutModel();
    model.CardId = this.plan.CardId;
    //model.CardId = this.pCardId;
    model.CardName = this.pCardName;
    model.CurrencyCode = denomination.CurrencyCode;
    model.details = {
      Price: denomination.Price,
      ServiceCharge: this.pServiceCharge ? this.pServiceCharge : 0,
      SubCardId: denomination.SubCardId
    }
    model.isPromotion = true;
    model.country = null;
    model.phoneNumber = null;
    model.countryFrom = this.countryFromId;
    model.countryTo = denomination.CountryToId;
    model.couponCode = '';
    model.currencyCode = denomination.CurrencyCode;
    model.transactiontype = TransactionType.Activation;
    model.couponCode = this.pCouponCode;
    model.isCalculatedServiceFee = false;
    model.isAutoRefill = false;
    model.isMandatoryAutorefill = true;
    model.isHideCouponEdit = !this.pIsEditCoupon;

    this.checkoutService.setCurrentCart(model);
    localStorage.setItem('currentCart', JSON.stringify(model))
    
  }

     getRechargeOption(card_id) {
    this.rechargeService.getMobileRechargeAmounts(card_id).subscribe(
      (res: number[]) => {
        this.rechargeAmounts = res;
        this.showContinue = true;
        this.onClickAmountOption(res[1]);
      }
    )
  }
   
   
  androidFuntionRevert(toast) {
    console.log(toast);
    try {
         window.androidCallBackInterface.callBackFormData(toast);
        } catch(err) {
       }
    }

    iosFuntionRevert(resultdata)
    {
      console.log(resultdata);
      try {
           var message = resultdata;
            window.webkit.messageHandlers.callbackHandler.postMessage(message);
            } 
            catch(err) {
            }
 
    }
    closeApp()
    {
      console.log(this.platform);
      if(this.platform == 'ios')
      {
        this.iosFuntionRevert('Mobile web view closed by client');
      }
      if(this.platform == 'android')
      {
        this.androidFuntionRevert('Mobile web view closed by client');
      }
       
     
    }
  
   
  goBack()
  {
      this.closeApp();
  
  }
   
   
  loginWithToken() 
  {
    
      
       let body = {
          username: this.userToken,
          password: '0000000000',
          //captcha: token
        };
     
        this.authService.loginwToken(body, false, "Y").subscribe((response) => {
          if (response != null) {
            
            this.razaSharedService.getTopThreeCountry().subscribe((res: any) => {
              this.fromCountry = res.slice(0, 3);
              this.getUserPlans();
              this.logintrial = 0;
            });
        
            
          }  
        },
        (error) => {
          /*if(this.logintrial <1)
          {
            this.loginWithToken();
            this.logintrial = this.logintrial+1;
          }
          else
          {
            this.logintrial = 0;
            //this.loginWithToken();
            this.razaSnackbarService.openError("The user name or password is incorrect. please try again.");
          }*/
          //this.razaSnackbarService.openError("Oops something went wrong! Try again.");
          const dialogRef = this.dialog.open(ErrLoginComponent, {
            data: {
              success: 'success',
               
            }
          });
          let card = this.selectedCard;
          dialogRef.afterClosed().subscribe(result => {
            this.showAndroidToast('Mobile web view closed by client.');
          });
        });
       /* this.executeCaptcha('login').subscribe((token) =>{},(error) => {
          console.log("error "+error); 
    } ) */

  }
  showAndroidToast(obj)
  {
  if(this.platform == 'ios')
    {
      this.iosFuntionRevert(obj);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(obj);
    }
  }
  get_promotion_class(){
    if(this.promotionCode!= ''){
      return 'top-space'
    }
    else{
      return '';
    }
  }
  
  
  
  setOperator(obj)
  {
    this.mobileTopupData.Operator = obj;
    this.showOperators = false;
    this.currentOperator = obj;
    this.getOperatorDenominations();
  }
  get_optr_image(obj)
  {
    return `assets/images/operators/${obj.toLowerCase()}.png`;
  }

  changeOperator()
  {
    this.showOperators = true;
  }
  getOperatorDenominations()
  {
    this.mobileTopupService.getOperatorsDenomination(this.currentSetting.currentCountryId, this.countryTo, this.currentOperator).subscribe(  (data: any) =>{
      if(data)
      {
        this.mobileTopupData.OperatorDenominations = data;
        this.onClickAmountOption(this.mobileTopupData.OperatorDenominations[1]) ;
      }
      
    })
  }

}

