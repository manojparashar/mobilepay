import { Component, OnInit, Injector, Input } from '@angular/core';
//import { FormControl } from '@angular/forms';
//import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialog} from '@angular/material/dialog';
import { ViewChild, AfterViewInit, NgZone} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
 
import { ElementRef } from '@angular/core';
import { AuthenticationService } from '../../core/services/auth.service';
import { CountriesService } from '../../core/services/country.service';
 
import { RazaSnackBarService } from '../../shared/razaSnackbar.service';
//import { isValidaEmail, isValidPhoneNumber, autoCorrectIfPhoneNumber, isValidPhoneOrEmail } from '../../shared/utilities';
//import { environment } from '../../../environments/environment';
import { AppBaseComponent } from '../../shared/components/app-base-component';

 
//import { CreditCardValidators } from 'angular-cc-library';
//import { Observable } from 'rxjs/internal/Observable';
 
 
import { RazaEnvironmentService } from '../../core/services/razaEnvironment.service';
import { CustomerService } from '../../accounts/services/customerService';
import { RazaSharedService } from '../../shared/razaShared.service';

import { State, BillingInfo } from '../../accounts/models/billingInfo';
import { CodeValuePair } from '../../core/models/codeValuePair.model';
import { Country } from '../../shared/model/country';
import { ICheckoutModel, NewPlanCheckoutModel } from '../../checkout/models/checkout-model';
import { TransactionType } from '../../payments/models/transaction-request.model';
import { CreditCard } from '../../accounts/models/creditCard'; //BraintreeCard
//import { ConfirmPopupDialog } from '../../accounts/dialog/confirm-popup/confirm-popup-dialog';
 import { ApiErrorResponse } from '../../core/models/ApiErrorResponse';

//import { AddCreditcardDialog } from '../../accounts/dialog/add-creditcard-dialog/add-creditcard-dialog';
//import { AddCreditcardPayDialog } from '../../accounts/dialog/add-creditcard-pay-dialog/add-creditcard-pay-dialog';

//import { isNullOrUndefined } from "../../shared/utilities";
import { TransactionService } from '../../payments/services/transaction.service';
import { BraintreeService } from '../../payments/services/braintree.service';
import { BraintreeCard } from '../../accounts/models/braintreeCard';

import { HttpClient, HttpHeaders } from '@angular/common/http';
//import * as braintree from 'braintree-web';
//import { IPaypalCheckoutOrderInfo, ActivationOrderInfo, RechargeOrderInfo, ICheckoutOrderInfo, MobileTopupOrderInfo } from '../../payments/models/planOrderInfo.model';

//import { ApiProcessResponse } from '../../core/models/ApiProcessResponse';  
 import { TransactionMobProcessBraintreeService } from "../../payments/services/transaction-mob-process-braintree.service";
 //import { ValidateCouponCodeResponseModel, ValidateCouponCodeRequestModel } from '../../payments/models/validate-couponcode-request.model';
 import { RechargeCheckoutModel } from '../../checkout/models/checkout-model';
 import { Plan } from '../../accounts/models/plan';
 import { PlanService } from '../../accounts/services/planService';
 import { CheckoutService } from '../../checkout/services/checkout.service';

 import { RechargeService } from '../../recharge/services/recharge.Service';

 //import { ErrorDialogModel } from '../../shared/model/error-dialog.model';
 //import { ErrorDialogComponent } from '../../shared/dialog/error-dialog/error-dialog.component';
 import { CurrentSetting } from "../../core/models/current-setting";
 import { PromotionResolverService } from '../../deals/services/promotion-resolver.service';

 import { PromotionsService } from '../../deals/services/promotions.service';
 import { PromotionPlanDenomination } from '../../deals/model/promotion-plan-denomination';

 
import { SearchRatesService } from '../../rates/searchrates.service';

import { trim } from 'jquery';
import { ErrLoginComponent } from '../dialog/err-login/err-login.component';
import { GlobalPlansData } from '../../globalrates/model/globalPlansData';
import { Ratedenominations } from '../../globalrates/model/ratedenominations';
import { Subscription } from 'rxjs';
import { ErrorDialogModel } from 'app/shared/model/error-dialog.model';
import { ErrorDialogComponent } from 'app/shared/dialog/error-dialog/error-dialog.component';
import { ValidateCouponCodeRequestModel, ValidateCouponCodeResponseModel } from 'app/payments/models/validate-couponcode-request.model';

 declare var jQuery: any;
interface Window {
  webkit?: any;
  androidCallBackInterface:any;
}
declare var window: Window;
@Component({
  selector: 'app-buy-one-get-one',
  templateUrl: './buy-one-get-one.component.html',
  styleUrls: ['./buy-one-get-one.component.scss']
})
export class BuyOneGetOneComponent extends AppBaseComponent implements OnInit {
  hide_header_footer:boolean=true;
  first_page:boolean=true;
  second_page:boolean=false;
  third_page:boolean=false;
  fourth_page:boolean=false;
  fifth_page:boolean=false;
  six_page:boolean=false;
  seventh_page:boolean=false;
  eight_page:boolean=false;
  userPhone : any;
  userPass : any;
  userToken : any;
  enc_key:any;
  key_arr:any;
  planId: string;
  creditCard:CreditCard;
  paymentDetailForm: FormGroup;
  billingInfoForm: FormGroup;
  paymentInfoForm: FormGroup;
  existingCreditCardForm: FormGroup;

  countries: Country[]
  fromCountry: Country[] = [];
  months: CodeValuePair[];
  years: CodeValuePair[];
  states: State[] = [];
  customerSavedCards: CreditCard[];
  havingExistingCard: boolean = false;
  selectedCard: CreditCard;
  selectedCardPay: CreditCard;
  braintreeCard:BraintreeCard;
  cvvStored: string;
  billingInfo: BillingInfo;
  countryFromId: number = 1;
  autoRefillTipText: string;
  braintreeToken:any;
  paymentProcessor:any;
  countryId:number;
  IsEnableFreeTrial:boolean=true;
  autocompleteInput: string;
queryWait: boolean;
checked:boolean=true;
balance:number=0;
address: Object;
establishmentAddress: Object;
formattedAddress: string;
formattedEstablishmentAddress: string;
phone: string;
search_country:any='US';
search_country_id:any=1;
plan: Plan;
isEnableOtherPlan: boolean = false;
isAutoRefillEnable: boolean = false;
paymentSubmitted: boolean;
amount:any=10;
processType:any=''; 
addNewCard:boolean=false;
editCard:boolean=false; 
showCartInfo:boolean=false;
currentCart: ICheckoutModel;
process_info:any='';
platform:any;
cardCvvError:boolean=false;
rechargeAmounts: number[];

DeviceId:string;
DeviceName:string;
DeviceModel:string;
DeviceType:string;
AppVersion:string;

promotionCode:string='';
denominationList: any[];
promotionPlan  : any[];
pCardId : number;
pCardName : string;
pServiceCharge : number;
pCouponCode: string;
pIsEditCoupon : any;
showContinue:boolean=false;
logintrial : number=0;
topup_no : number=0;
dcode:any=0;
callingFrom :number;
callingTo:number;
RatePerMin:number;
globalPlanData: GlobalPlansData;
countryCode :any;
        ratesLoaded=false;
        countryName :any;
        WithoutAutorefillPlans: Ratedenominations[];
        rechargeAmountsList: number[];      
        RatePerMinPromo :any;
     ////////////////////////////

    
  RatePerMinWithOutPromo: number;
 
  isAutorefill = true;
  viewAllrate = false;
  viewAllPlan = false;
 

  
  customdata : any[];
  customCountry:any;
  customCountryId:any;
  customCountryPrice:any;
  currentSetting: CurrentSetting;
  currenctSetting$: Subscription;

  CountryToId:any;
      CountryToName:any;
      CurrencyCode:any;
      FreeMin:any;
      IsPercentageAmountOffer:any;
      OfferPercentage:any;
      Price:any=3;
       
      RegularMin:any;
      ServiceCharge:any;
      SubCardId:any=12;
      firstBox:any[];
      secondBox:any[];
      thirdBox:any[];
      oneFreemin:number;
      
      
/**********EOF Google place search **********/
private static _setting: CurrentSetting;
constructor( 
  
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private _formBuilder: FormBuilder,
    private searchRatesService: SearchRatesService,
    private countriesService: CountriesService,
    private dialog: MatDialog,
    private snackBarService: RazaSnackBarService,

    public zone: NgZone,
    private transactionService: TransactionService, 
    private braintreeService: BraintreeService,
    private formBuilder: FormBuilder,
    private countryService: CountriesService,
    private razaEnvService: RazaEnvironmentService,
    private customerService: CustomerService,
    
    private razaSnackbarService: RazaSnackBarService,
    private authService: AuthenticationService,
    private httpClient: HttpClient,
    private razaSharedService: RazaSharedService,
    private rechargeService: RechargeService,
    private transactionProcessBraintree: TransactionMobProcessBraintreeService,
    private planService: PlanService,
    private checkoutService: CheckoutService,
    private promotionResolverService: PromotionResolverService,
    private promotionService: PromotionsService,
   
    _injector: Injector
  ) {
    
    super(_injector);
    
  }
  @ViewChild('privatUserCheckbox') privatUserCheckbox: ElementRef;
  
  ngOnInit(): void {

    /***
     * 
     *
     *  {
        "password" : "123456",
        "phone" : "7733961313",
        "country_id" : 1,
        "de" : "9fc3772dcb9c1364d6cba57ba5694c8636109ba2f1334041316c142dbdd061da||iPhone||iphone||5.7||ios"
        DeviceId:0
        DeviceName:1
        DeviceModel:2
        DeviceType:3
        AppVersion:4
        platform:5
      }
      
     */
      localStorage.removeItem('topupPhone');
      localStorage.removeItem('currentCart');
      localStorage.removeItem('topupAmount');
      
    if(localStorage.getItem('web_enc_key'))
    {
      this.enc_key    = localStorage.getItem('web_enc_key');
    }
    else
    {
      this.enc_key    = this.route.snapshot.paramMap.get('post_data');
    }
    localStorage.setItem('enc_key', this.enc_key);
    //this.enc_key    = this.route.snapshot.paramMap.get('post_data');
   // localStorage.setItem('enc_key', this.enc_key);
    
    //this.enc_key  = 'ewoicGhvbmUiOiIzMTI5NzU4NTQyIiwKInBhc3N3b3JkIjoiMTIzNDU2IiwKImNvdW50cnlfaWQiOjEsCiJwbGF0Zm9ybSI6ICJpb3MiLAoiYW1vdW50IjogIjUiCn0=';
    let data_list           =  JSON.parse(atob(this.enc_key));
    this.key_arr            = data_list;
    this.userPhone          = (data_list.phone)?data_list.phone:'';;
    this.userPass           = (data_list.password)?data_list.password:'';
    this.userToken          = (data_list.token)?data_list.token:'';
    this.promotionCode      = (data_list.coupon_code)?data_list.coupon_code:'';
    this.dcode              = (data_list.dcode)?data_list.dcode:''; 
    this.search_country_id  = data_list.country_id;
    this.countryFromId      = this.search_country_id;
    let device_info         = data_list.de;

    if(device_info.includes("||"))
    {
      var dres = device_info.split("||")
      this.platform           = dres[4];
      this.DeviceType         = dres[4];
    }
    else
    {
      this.platform           = device_info;
      this.DeviceType         = device_info;
    }

    let top_no              = ( data_list.top_no && data_list.top_no !='' ) ?data_list.top_no:0;
    this.topup_no           = top_no;
   
     
    if(!this.authService.isAuthenticated ())
    {
      
      this.processLoginSteps();
     }
   else{
    this.getUserPlans();
   }
  
    
    this.setCurrentCountry();
     
     
     
  }

  processLoginSteps()
  {
    localStorage.setItem('prev_token', this.userToken);
    localStorage.removeItem('currentUser');
    var d = new Date();
      var gtime = d.getTime()+" ";
       localStorage.setItem('last_login_time', gtime);
 
    if(this.userPhone == '13129758542')
    //if(this.userPhone == '13129758542' )
    {
      this.autoLoginUser();
    }
    else
    {
      this.loginWithToken();
    }
   
  }

  setCurrentCountry()
  {
    //localStorage.removeItem('session_key');
      let countries_list = [{CountryId:1,CountryName:"U.S.A",CountryCode:'1'},
    {CountryId:2,CountryName:"CANADA",CountryCode:'1'},
    {CountryId:3,CountryName:"U.K",CountryCode:'44'}];

    for(var j = 0; j <  countries_list.length;j++){
      if(countries_list[j].CountryId == this.search_country_id)
      { 
        const setting = new CurrentSetting();
        setting.country = countries_list[j];
         this.razaEnvService.setCurrentSetting(setting);
      }
  } 
}

setCurrentPlan =(obj, obj1)=>{
  this.Price = obj;
  this.SubCardId = obj1;
}

continue()
{
  //this.goBack('passed denomination page');
  this.buyNow(this.Price, this.SubCardId);
}
getClass = obj =>{
  if(this.Price == obj)
  {
    return 'selected-dom';
  }
  else
  return '';
}
 
 
  autoLoginUser() 
  {
    const phoneOrEmail = this.userPhone;
   /* this.executeCaptcha('login').subscribe((token) =>{},(error) => {
      console.log(error.error_description);
        //console.log("error "+error); 
       // this.closeApp();
  }
  ) */
    let body = {
        username: phoneOrEmail,
        password: this.userPass,
        captcha: ''
      };
      
      this.authService.login(body, false, "Y").subscribe((response) => {
        if (response != null) {
           
            this.getUserPlans(); 
           
           
        
        }  
      },
        (error) => {
          console.log(error.error_description);
         // this.closeApp();
        });
    
  }

  onClickAmountOption_old(amount: number) {
    this.amount = amount;

    const model: RechargeCheckoutModel = new RechargeCheckoutModel();
    var cupon_code = '';
    
    model.purchaseAmount = amount;
    model.couponCode = cupon_code;
    model.currencyCode = this.plan.CurrencyCode;
    model.cvv = '';
    model.planId = this.plan.PlanId
    model.transactiontype = TransactionType.MR;
    model.serviceChargePercentage = this.plan.ServiceChargePercent;
    model.planName = this.plan.CardName;
    model.countryFrom = this.plan.CountryFrom;
    model.countryTo = this.plan.CountryTo;
    model.cardId = this.plan.CardId;
    model.isAutoRefill = this.isAutoRefillEnable;
    this.checkoutService.setCurrentCart(model);
    localStorage.setItem('currentCart', JSON.stringify(model))
    //this.router.navigate(['/mobile_pay']);
    //this.currentCart = model;
    
  }
  
  onClickAmountOption(item: Ratedenominations) {
    
     
    const model: RechargeCheckoutModel = new RechargeCheckoutModel();
    this.amount = item.Price;

    if(item.DiscountApplied > 0)
    localStorage.setItem('promo', 'Free '+item.DiscountApplied.toString()+'%');
    else{
       
      localStorage.setItem('promo', 'N/A' );
    }

     model.purchaseAmount = item.Price;
     model.couponCode = item.PromoCode;
     model.currencyCode = this.plan.CurrencyCode;
     model.cvv = '';
     model.planId = this.plan.PlanId
     model.transactiontype = TransactionType.MR;
     model.serviceChargePercentage = this.plan.ServiceChargePercent;
     model.planName = this.plan.CardName;
     model.countryFrom = this.plan.CountryFrom;
     model.countryTo = this.plan.CountryTo;
     model.cardId = this.plan.CardId;
     model.isAutoRefill = this.isAutoRefillEnable;
     model.offerPercentage = '';
     this.checkoutService.setCurrentCart(model);
      
      
     localStorage.setItem('currentCart', JSON.stringify(model))
   }


getSelectedClass(amt:any)
{
  if(this.amount == amt)
  {
    return 'selected-dom';
  }
  else{
    return '';
  }
}
getChecked(amt:number)
{
  
  if(this.Price == amt)
  {
     
    return true;
    
  }
  else{
    
    return false;
  }
}
  selectAmount(id: any) {
    //this.id = id;
  }

  

  
  getUserPlans()
  {

  
    //this.fromCountry
 /*   if(parseFloat(lentgth)>0)
    {
      phone =   phone.substring(lentgth);
      console.log("New phone number is "+phone)
    }else
    { 
     if(this.search_country_id == 3 || this.search_country_id == 26)
    {
      phone =   phone.substring(2);
    }
    else
    {
      phone =   phone.substring(1);
    }
  }
   */  
  var phone =   this.userPhone;
    
  if(this.search_country_id == 2 || this.search_country_id == 1) 
  {
    
    

      if(this.search_country_id == 3 || this.search_country_id == 26)
      {
        phone =   phone.substring(2);
      }
      else
      {
        phone =   phone.substring(1);
      }
  }
  localStorage.setItem('search_country_id', this.countryFromId.toString() );
    //localStorage.setItem('pnone_no', phone);   
    this.planService.getPlanInfo(phone).subscribe(
      (res:any)=>{
        
       
        this.plan = res;

        if(this.promotionCode == 'buy1get1')
        {
          if(res.CardId && res.CardId !== 'undefined')
          {
              var obj =  {
                CouponCode: 'Buy1Get1',
                CardId: res.CardId,
                CountryFrom: res.CountryFrom,
                CountryTo: res.CountryTo,
                Price: 3,
                TransType: TransactionType.Recharge
            }
    
            this.validateCoupon(obj).then((res: ValidateCouponCodeResponseModel) => {
              if (res.Status) 
              {
    
              }
              else{
                let error = new ErrorDialogModel();
                error.header = 'Invalid Coupon Code';
                error.message = 'This offer is available for New customers only. Kindly recharge your account or call customer service for assistance. Thank you!';
                this.openErrorDialog(error);
                localStorage.setItem('removeCupon', 'yes');

              //console.log(this.enc_key);
                let data_list           =  JSON.parse(atob(this.enc_key));
               // console.log(data_list);
                data_list.coupon_code = '';
                localStorage.setItem('enc_key',  btoa(data_list) );
                console.log(data_list);
                this.router.navigate(['/plans']);
                
              }
            
            })
          }

        }
       else if( this.promotionCode == '')
        {
          this.callingFrom = this.plan.CountryFrom
          this.callingTo = this.plan.CountryTo
          //this.getRates();
          this.getRechargeOption1()
          this.getRechargeOption(this.plan.CardId)
        }
        else
        {
          this.getPromotionPlans(this.promotionCode, this.countryFromId)
        }
        
        this.planId = this.plan.PlanId;
        this.balance = this.plan.Balance;
         
      }
    );

    
  }


  openErrorDialog(error: ErrorDialogModel): void {
    this.dialog.open(ErrorDialogComponent, {
      data: { error }
    });
  }
  validateCoupon(req: ValidateCouponCodeRequestModel): Promise<ValidateCouponCodeResponseModel | ApiErrorResponse> {
    return this.transactionService.validateCouponCode(req).toPromise();
  }
  
  getPromotionPlans(promotionCode: string, countryId: number)
  {
   
       this.promotionService.getPromotion(countryId, promotionCode).subscribe(
        res  => {
          //this.promotionPlan = res.Plans[0];
          this.denominationList = res.Plans[0].Denominations;
          this.pCardId = res.Plans[0].CardId;
          this.pCardName = res.Plans[0].CardName;
          this.pServiceCharge  = res.Plans[0].ServiceCharge;
          this.pCouponCode = res.Plans[0].CouponCode;
          this.pIsEditCoupon = res.Plans[0].IsEditCoupon
          
          this.buyPlan(this.denominationList[0]);
          this.showContinue = true;
        }
      )
  }
  getRechargeOption1() {
    this.rechargeService.getRechargeAmounts(this.plan.CardId).subscribe(
      (res: number[]) => {
        this.rechargeAmounts = res;
        this.rechargeAmountsList = res;
        if(res[0] && res.length >=4)
        {
          /************ To show new reate page enable this page uncomment this ***********/
          
          this.getRates()

        }

		   // this.getCustomRechargeOption();
       

      }
    )
  }
 
  getRates()
  {
    
    this.searchRatesService.getSearchGlobalRates(this.callingFrom, this.callingTo).subscribe(
      (data: any) => {
       var RatePerMinWithOutPromo = data.WithoutAutorefillPlans.RatePerMin;
        
        this.countryCode = data.CountryCode;
        this.countryId = data.CountryId;
        this.countryName = data.CountryName;
         
        this.RatePerMin = this.RatePerMinPromo;
       
        this.globalPlanData = data;
        var data_arr = {};
        var multi_data_arr = [];
        var j=0;
        var wdata_arr = {};
        var wmulti_data_arr = [];
      
       


        if(data.DiscountedPlans.Denominations)
        {
          var j=0;
          var wdata_arr = {};
          var wmulti_data_arr = [];
          var withoutAfilPlan = data.DiscountedPlans.Denominations;
          for(var r = 0; r <this.rechargeAmountsList.length; r++)
          {
            wdata_arr = this.getplanbyprice(withoutAfilPlan, this.rechargeAmountsList[r], "AUTOREFILL");
               wmulti_data_arr[r] = wdata_arr;
             
               wdata_arr = {};
               if( r+1 == this.rechargeAmountsList.length)
                {
                 
                  this.WithoutAutorefillPlans = wmulti_data_arr;
                  
                } 
          }
        
          
        }  
       var i = 0;
       },
      (err: ApiErrorResponse) => console.log(err),
    );
  }
  
  getplanbyprice(arr, val, obj) {
    var plan =  
    {
      'id' : 'slide_'+val,
      "SubCardId": "",
      "Price": val,
      "ServiceCharge": 0,
      "Priority": 1,
      "TotalTime": 0,
      "DiscountApplied": 0,
      "DiscountType": "0",
      "PromoMinutes": 0,
      "PromoCode": obj
  }
    
      for(var i=0; i<arr.length; i++)
      {
       // console.log(arr[i]);
        if(arr[i].Price == val)
        {
          
          plan =  
          {
            'id' : 'slide_'+val,
            "SubCardId": arr[i].SubCardId,
            "Price": val,
            "ServiceCharge": arr[i].ServiceCharge,
            "Priority":arr[i].Priority,
            "TotalTime": arr[i].TotalTime,
            "DiscountApplied": arr[i].DiscountApplied,
            "DiscountType": arr[i].DiscountType,
            "PromoMinutes": arr[i].PromoMinutes,
            "PromoCode": arr[i].PromoCode
        }
        }
      }
     
    return plan;
    
  }
  getRatePerMin(item)
  {
    return this.toFixed((item.Price/(item.TotalTime+item.PromoMinutes))*100)
  }

  buyPlanNow(item: Ratedenominations) {
    
     
    const model: RechargeCheckoutModel = new RechargeCheckoutModel();
 
     model.purchaseAmount = item.Price;
     model.couponCode = item.PromoCode;
     model.currencyCode = this.plan.CurrencyCode;
     model.cvv = '';
     model.planId = this.plan.PlanId
     model.transactiontype = TransactionType.MR;
     model.serviceChargePercentage = this.plan.ServiceChargePercent;
     model.planName = this.plan.CardName;
     model.countryFrom = this.callingFrom;
     model.countryTo = this.plan.CountryTo;
     model.cardId = this.plan.CardId;
     model.isAutoRefill = false;
     model.offerPercentage = '';
     this.checkoutService.setCurrentCart(model);
      
     this.checkoutService.setCurrentCart(model);
     localStorage.setItem('currentCart', JSON.stringify(model))
   }


  buyPlan(denomination: PromotionPlanDenomination) {
    this.amount = denomination.Price;
    const model: NewPlanCheckoutModel = new NewPlanCheckoutModel();
    model.CardId = this.plan.CardId;
    //model.CardId = this.pCardId;
    model.CardName = this.pCardName;
    model.CurrencyCode = denomination.CurrencyCode;
    model.details = {
      Price: denomination.Price,
      ServiceCharge: this.pServiceCharge ? this.pServiceCharge : 0,
      SubCardId: denomination.SubCardId
    }
    model.isPromotion = true;
    model.country = null;
    model.phoneNumber = null;
    model.countryFrom = this.callingFrom;
    model.countryTo = denomination.CountryToId;
    model.couponCode = '';
    model.currencyCode = denomination.CurrencyCode;
    model.transactiontype = TransactionType.Activation;
    model.couponCode = this.pCouponCode;
    model.isCalculatedServiceFee = false;
    model.isAutoRefill = false;
    model.isMandatoryAutorefill = true;
    model.isHideCouponEdit = !this.pIsEditCoupon;
    console.log(model);
    this.checkoutService.setCurrentCart(model);
    localStorage.setItem('currentCart', JSON.stringify(model))
    
  }


  toFixed(num) {
    /*fixed = fixed || 0;
    fixed = Math.pow(10, fixed);
    return Math.floor(num * fixed) / fixed;
  */
 var amount = ( num * 10  / 10 ).toFixed(1)
   return parseFloat(amount);
  }

   getRechargeOption(card_id) {
    this.rechargeService.getExchangeRate(this.plan.CurrencyCode).subscribe(
      (rate: any) => {
       
		if(this.plan.CurrencyCode == 'CAD' || this.plan.CurrencyCode == 'USD')	   
		 rate = 1;
      
     this.balance = (this.balance*rate)* 10  / 10;
        this.rechargeService.getMobileRechargeAmounts(card_id).subscribe(
          (res: number[]) => {
            if(res[0])
            {
              for(var i=0; i<res.length; i++)
              {
                res[i] = this.toFixed(res[i]*rate);
              }
            }
            this.rechargeAmounts = res;
            this.showContinue = true;
            console.log(res[1]);
            this.onClickAmountOption_old(res[1]);
          }
        )
    }
    )
  }
   
   
  androidFuntionRevert(toast) {
    console.log(toast);
    try {
         window.androidCallBackInterface.callBackFormData(toast);
        } catch(err) {
       }
    }

    iosFuntionRevert(resultdata)
    {
      console.log(resultdata);
      try {
           var message = resultdata;
            window.webkit.messageHandlers.callbackHandler.postMessage(message);
            } 
            catch(err) {
            }
 
    }
  
  
   
  goBack(obj:any)
  {
      var msg = '';
      if(obj !='')
      {
        msg = obj;
      }
      else{
        msg = 'Mobile web view closed by client'
      }
      msg = 'Mobile web view closed by client'
      this.closeApp(msg);
  }

  closeApp(obj:any)
  {
    var msg = '';
    
    if(obj !='')
    {
      msg = obj;
    }
    else{
      msg = 'Mobile web view closed by client'
    }
    msg = 'Mobile web view closed by client'
    if(this.platform == 'ios')
    {
      this.iosFuntionRevert(msg);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(msg);
    }
 }
   
  loginWithToken() 
  {
    
     
       let body = {
          username: this.userToken,
          password: '0000000000',
          captcha: ''
        };
     
        this.authService.loginwToken(body, false, "Y").subscribe((response) => {
          if (response != null) {
            
            this.razaSharedService.getTopThreeCountry().subscribe((res: any) => {
              this.fromCountry = res.slice(0, 3);
              
              this.logintrial = 0;
            });
            this.getUserPlans();
            
          }  
        },
        (error) => {
           
          //this.razaSnackbarService.openError("Oops something went wrong! Try again.");
          
          const dialogRef = this.dialog.open(ErrLoginComponent, {
            data: {
              success: 'success',
               
            }
          });
          let card = this.selectedCard;
          dialogRef.afterClosed().subscribe(result => {
            this.showAndroidToast('Mobile web view closed by client.');
          });

        });
       /* this.executeCaptcha('login').subscribe((token) =>{},(error) => {
          console.log("error "+error); 
        }  ) */

  }

  showAndroidToast(obj)
  {
  if(this.platform == 'ios')
    {
      this.iosFuntionRevert(obj);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(obj);
    }
  }
  get_promotion_class(){
    if(this.promotionCode!= ''){
      return 'top-space'
    }
    else{
      return '';
    }
  }

  onClickAmountOption1(item: any) {
    const model: RechargeCheckoutModel = new RechargeCheckoutModel();

    model.purchaseAmount = item;
    model.couponCode = 'Buy1Get1';
    model.currencyCode = this.plan.CurrencyCode;
    model.cvv = '';
    model.planId = this.plan.PlanId
    model.transactiontype = TransactionType.Recharge;
    model.serviceChargePercentage = this.plan.ServiceChargePercent;
    model.planName = this.plan.CardName;
    model.countryFrom = this.plan.CountryFrom;
    model.countryTo = this.plan.CountryTo;
    model.cardId = this.plan.CardId;
    model.isAutoRefill = false;
    model.offerPercentage = '';
    this.checkoutService.setCurrentCart(model);
    this.router.navigate(['/mobile_pay']);
  }
  buyNow(obj:any, obj2:any) {
    if(this.plan && this.plan.CardId)
    {
      this.onClickAmountOption1(obj);
    }
    else
    {
          var subcardid = '';
          var cuponcode = 'BUY1GET1';
          var service_fee = 0;
          if(this.plan.CountryFrom == 1)
          {
          subcardid = '161-'+obj2;
          service_fee = 0;
          }
          if(this.plan.CountryFrom== 2)
          {
          subcardid = '162-'+obj2;
          service_fee = 10;
          }
            const model: NewPlanCheckoutModel = new NewPlanCheckoutModel();

            //model.CardId = this.cardId;
          // model.CardName = this.cardName;
            //model.CurrencyCode = this.currencyCode;
            model.CardId = this.globalPlanData.CardId;
            model.CardName = this.globalPlanData.CardName;
            model.CurrencyCode = this.globalPlanData.CurrencyCode;

            model.details = {
              Price: obj,
              ServiceCharge: service_fee,
            SubCardId:subcardid
            }
            model.country = null;
            model.phoneNumber = null;
            model.countryFrom = this.plan.CountryFrom;
            model.countryTo = this.plan.CountryTo;
            
            model.currencyCode = this.plan.CurrencyCode;
            model.transactiontype = TransactionType.Activation;
            model.isAutoRefill = false;
            model.couponCode = cuponcode;
            model.currencyCode = this.globalPlanData.CurrencyCode;
            model.isHideCouponEdit = true;
          
          console.log(model);
          
            this.checkoutService.setCurrentCart(model);
 
              this.router.navigate(['/mobile_pay']);
           
          
          
        }
  }

}
