import { Component, OnInit, Injector, Input } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import { ViewChild, AfterViewInit, NgZone, HostListener} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
 
import { ElementRef } from '@angular/core';
import { AuthenticationService } from '../../core/services/auth.service';
import { CountriesService } from '../../core/services/country.service';
 
import { RazaSnackBarService } from '../../shared/razaSnackbar.service';
 
import { AppBaseComponent } from '../../shared/components/app-base-component';
 
import { RazaEnvironmentService } from '../../core/services/razaEnvironment.service';
import { CustomerService } from '../../accounts/services/customerService';
import { RazaSharedService } from '../../shared/razaShared.service';

import { State, BillingInfo } from '../../accounts/models/billingInfo';
import { CodeValuePair } from '../../core/models/codeValuePair.model';
import { Country } from '../../shared/model/country';
import { PlanRte } from '../../accounts/models/planRate';
import { CurrencyRate } from '../../accounts/models/currencyRate';
import { ICheckoutModel, NewPlanCheckoutModel } from '../../checkout/models/checkout-model';
import { TransactionType } from '../../payments/models/transaction-request.model';
import { CreditCard } from '../../accounts/models/creditCard'; 
 
import { TransactionService } from '../../payments/services/transaction.service';
import { BraintreeService } from '../../payments/services/braintree.service';
import { BraintreeCard } from '../../accounts/models/braintreeCard';

import { HttpClient, HttpHeaders } from '@angular/common/http';
 
 import { RechargeCheckoutModel } from '../../checkout/models/checkout-model';
 import { Plan } from '../../accounts/models/plan';
 import { PlanService } from '../../accounts/services/planService';
 import { CheckoutService } from '../../checkout/services/checkout.service';

import { RechargeService } from '../../recharge/services/recharge.Service';

import { CurrentSetting } from "../../core/models/current-setting";
 import { PromotionResolverService } from '../../deals/services/promotion-resolver.service';

 import { PromotionsService } from '../../deals/services/promotions.service';
 import { PromotionPlanDenomination } from '../../deals/model/promotion-plan-denomination';
import { data, trim } from 'jquery';
import { OperatorDenominations } from '../../mobiletopup/model/operatorDenominations';
 
import { MobiletopupService } from '../../mobiletopup/mobiletopup.service';
import { MobileTopupCheckoutModel } from '../../checkout/models/checkout-model';
import { mobileTopupModel } from '../../mobiletopup/model/mobileTopupModel';
import { isNullOrUndefined } from "../../shared/utilities";
import { ApiErrorResponse } from '../../core/models/ApiErrorResponse';
import { Location } from '@angular/common';
import { startWith, map, catchError } from 'rxjs/operators';
import { ErrLoginComponent } from '../dialog/err-login/err-login.component'; 
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { GlobalRatesService } from '../../home/globalrates.service';
 declare var jQuery: any;
 declare var $: any;
interface Window {
  addEventListener(arg0: string, scrolling: any, arg2: boolean);
  webkit?: any;
  androidCallBackInterface:any;
  height:any;
}
declare var window: Window;

@Component({
  selector: 'app-country-rates',
  templateUrl: './country-rates.component.html',
  styleUrls: ['./country-rates.component.scss']
})
export class CountryRatesComponent extends AppBaseComponent implements OnInit {
  hide_header_footer:boolean=true;
  userPhone : any;
  userPass : any;
  userToken : any;
  enc_key:any;
  key_arr:any;
  planId: string;
  creditCard:CreditCard;
  paymentDetailForm: FormGroup;
  billingInfoForm: FormGroup;
  paymentInfoForm: FormGroup;
  existingCreditCardForm: FormGroup;

  countries: Country[]
  fromCountry: Country[] = [];
  months: CodeValuePair[];
  years: CodeValuePair[];
  states: State[] = [];
  customerSavedCards: CreditCard[];
  havingExistingCard: boolean = false;
  selectedCard: CreditCard;
  selectedCardPay: CreditCard;
  braintreeCard:BraintreeCard;
  cvvStored: string;
  billingInfo: BillingInfo;
  countryFromId: number = 1;
  autoRefillTipText: string;
  braintreeToken:any;
  paymentProcessor:any;
  countryId:number;
  IsEnableFreeTrial:boolean=true;
  autocompleteInput: string;
queryWait: boolean;
checked:boolean=true;
balance:number=0;
address: Object;
establishmentAddress: Object;
formattedAddress: string;
formattedEstablishmentAddress: string;
phone: string;
search_country:any='US';
search_country_id:any=1;
plan: Plan;
isEnableOtherPlan: boolean = false;
isAutoRefillEnable: boolean = false;
paymentSubmitted: boolean;
amount:any=10;
processType:any=''; 
addNewCard:boolean=false;
editCard:boolean=false; 
showCartInfo:boolean=false;
currentCart: ICheckoutModel;
process_info:any='';
platform:any;
cardCvvError:boolean=false;
rechargeAmounts: number[];

DeviceId:string;
DeviceName:string;
DeviceModel:string;
DeviceType:string;
AppVersion:string;

promotionCode:string='';
denominationList: any[];
promotionPlan  : any[];
pCardId : number;
pCardName : string;
pServiceCharge : number;
pCouponCode: string;
pIsEditCoupon : any;
showContinue:boolean=false;
logintrial : number=0;
topup_no :string;

allCountry: Country[];
  mycountryName: string;
  mycountryId: number;
  filteredCountry: Observable<Country[]>;
  // autoControl = new FormControl();
  mobileTopupData: mobileTopupModel;
  operatorDenominations: OperatorDenominations[];
  mobileTopupForm: FormGroup;
  amountSent: number;
  countrySelect = new FormControl();
  currentSetting$: Subscription;
  currentSetting: CurrentSetting;
  isTopUpEnable: boolean = false;
  pinnumber:number;
  iso:string;
  countryName:String;
  showError:boolean=false; 
  plans_list:any;
  currency_rate:any=[];
  show_currency_rate:boolean=false;
  customer_id:any ;
  pin_no:any ;
  alphabet:string='A';
  alphabets:any[];
  step_one:boolean=false;
  selected_country:any;
  //planInfo:any=[{ 'amount':0, 'minutes':0, 'rate_pm':0}];
  planInfo:any=[];
  minutes:any;

  plan_exchange_rate:number;
  plan_currency_code:any;
  country_calling_rate:any;
  plan_charg_in_currency:any;
  SubCurrencySymbol : any;
  hundred_price : any;
  selectedAmount:any;
  isAutorefill:boolean=true;
  rateDenomination : any[];
  autoControl = new FormControl(); 
  searchicon: string = 'assets/images/search8.svg';
  showPlaceholder: boolean = true;
  showDropdown: boolean = false;
  selectedCountry : string ='';
  viewHeight: number;
  deviceHeight:number;
  allowAutoload: boolean = true;
    
/**********EOF Google place search **********/
private static _setting: CurrentSetting;
//
  scroll: boolean;
constructor( 
  
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private _formBuilder: FormBuilder,
    
    private countriesService: CountriesService,
    private dialog: MatDialog,
    private snackBarService: RazaSnackBarService,

    public zone: NgZone,
    private transactionService: TransactionService, 
    private braintreeService: BraintreeService,
    private formBuilder: FormBuilder,
    private countryService: CountriesService,
    private razaEnvService: RazaEnvironmentService,
    private customerService: CustomerService,
    
    private razaSnackbarService: RazaSnackBarService,
    private authService: AuthenticationService,
    private httpClient: HttpClient,
    private razaSharedService: RazaSharedService,
    private rechargeService: RechargeService,
 
    private planService: PlanService,
    private checkoutService: CheckoutService,
    private promotionResolverService: PromotionResolverService,
    private promotionService: PromotionsService,
    private mobileTopupService: MobiletopupService,
    private location:Location,
    private globalRatesService: GlobalRatesService,
    _injector: Injector,
     
  ) {
    
    super(_injector);
    
  }
  
  

  @ViewChild('privatUserCheckbox') privatUserCheckbox: ElementRef;
  @ViewChild('mainScreen') elementView: ElementRef;
  ngOnInit(): void {
   
    this.minutes = {0:5,1:10,2:20,3:50};
    if(localStorage.getItem('web_enc_key'))
    {
      this.enc_key    = localStorage.getItem('web_enc_key');
     }
    else
    {
      this.enc_key    = this.route.snapshot.paramMap.get('post_data');
      }
    localStorage.setItem('enc_key', this.enc_key);
    
    //this.enc_key  = 'ewoicGhvbmUiOiIzMTI5NzU4NTQyIiwKInBhc3N3b3JkIjoiMTIzNDU2IiwKImNvdW50cnlfaWQiOjEsCiJwbGF0Zm9ybSI6ICJpb3MiLAoiYW1vdW50IjogIjUiCn0=';
    let data_list   =  JSON.parse(atob(this.enc_key));
     
    this.key_arr    = data_list;
     
    this.userPhone  = (data_list.phone)?data_list.phone:'';;
    this.userPass   = (data_list.password)?data_list.password:'';
    this.userToken = (data_list.token)?data_list.token:'';
    this.promotionCode = (data_list.coupon_code)?data_list.coupon_code:'';
 

    this.currency_rate = [];
    this.search_country_id  = data_list.country_id;
    this.generateAlphabets();
    this.setCurrentCountry();
    this.countryFromId      = this.search_country_id;
    let device_info         = data_list.de;
    
    this.customer_id =(data_list.customer_id)?data_list.customer_id:'' ;
    this.pin_no = (data_list.pin_no)?data_list.pin_no:'' ;
    this.alphabet = (data_list.alphabet)?data_list.alphabet:'A';

    this.currentSetting$ = this.razaEnvService.getCurrentSetting().subscribe(res => {
      this.currentSetting = res;
    });

    if(device_info.includes("||"))
    {
      var dres = device_info.split("||")
      this.platform           = dres[4];
      this.DeviceType         = dres[4];
    }
    else
    {
      this.platform           = device_info;
      this.DeviceType         = device_info;
    }


    this.getCountries();
    
     this.search_country_id  = data_list.country_id;
  
    this.countryFromId      = this.search_country_id;
    let top_no              = ( data_list.top_no && data_list.top_no !='' ) ?data_list.top_no:0;
    this.topup_no           = top_no;

    this.mycountryId = 0;
    
    this.getRatesList(); 

   this.getUserPlans();

     /*******/
      $(document).ready(function(){
       
      var settings2 = {
        "url": "https://services.razacomm.com/razawebservices/raza_mobileapp_services.asmx",
        "method": "POST",
        "timeout": 0,
        //"crossDomain": true,
        "headers": {
          "Content-Type": "text/xml",
          "Access-Control-Request-Headers": "*"
        },
        "data": "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n    <soap:Header>\r\n    <Raza_AuthHeader xmlns=\"http://tempuri.org/\">\r\n    <AuthUsername>AjinTestUser</AuthUsername>\r\n    <AuthPassword>AJTeStPWD!FSTr</AuthPassword>\r\n    </Raza_AuthHeader>\r\n    </soap:Header>\r\n    <soap:Body>\r\n    <GetOnlinePlanRates xmlns=\"http://tempuri.org/\">\r\n      <customerID>2008999</customerID>\r\n      <Pin>1112267924</Pin>\r\n      <Alphabet>A</Alphabet>\r\n    </GetOnlinePlanRates>\r\n    </soap:Body>\r\n    </soap:Envelope>"
        ,
      };
       
      

      var json_arr = [];
     /*
      $.ajax(settings2).done(function (xmlString) {
       
        if(xmlString)
        {
          
          $(xmlString).find('GetOnlinePlanRatesResult').each(function(){
            var PlanId = $(this).find('PlanId').text();
            var CountryFromId = $(this).find('CountryFromId').text();
            var CurrencyCode = $(this).find('CurrencyCode').text();
            var ExchangeRate = $(this).find('ExchangeRate').text();
    
          })
          
          $(xmlString).find('OnlinePlanRate').each(function(){
            var sTitle = $(this).text();
            
            var CountryToId = $(this).find('CountryToId').text();
            var CountryToName = $(this).find('CountryToName').text();
            var CallingRate = $(this).find('CallingRate').text();
    
            var rate= {CountryToId:CountryToId, CountryToName:CountryToName,CallingRate:CallingRate }
            // console.log(rate);
             json_arr.push(rate);
             
             //console.log(this.plans_list);
            // this.printJson(rate);
          });
     
        }
        
      });
    
        */
    // this.plans_list = this.countryService.getSoap();
    }.bind(this));  
     /****** */
     

     this.searchRates();
     this.filteredCountry = this.autoControl.valueChanges
      .pipe(
        startWith<string | any>(''),
        map(value => typeof value === 'string' ? value : value.CountryName),
        map(CountryName => CountryName ? this._filter(CountryName) : this.allCountry)
      );
    
      
      window.addEventListener('scroll', this.scrolling, true)
  }
  

   private _filter(value: any): any[] {
    const filterValue = value.toLowerCase();
    return this.allCountry.filter(option => option.CountryName.toLowerCase().indexOf(filterValue) === 0);
  }
  private searchRates() {
    this.globalRatesService.getAllCountriesRates(this.currentSetting.currentCountryId).subscribe(
      (data: any) => {
        this.allCountry = data;
      },
      (err: ApiErrorResponse) => console.log(err),
    )
  }
  displayFn(country?: any): string | undefined {
   
    return country ? country.CountryName : undefined;
  }
  onClickInput() {
    this.searchicon = 'assets/images/search8.svg';
  }
  onInputFocus() {
    this.searchicon = 'assets/images/cross8.png';
    this.showDropdown = false;
    this.showPlaceholder = false;
  }
  viewRates(event, countryName) {
   //this.alphabet = this.selectedCountry;
   this.currency_rate = [];
   this.alphabet = countryName;
   this.getRatesList();
  }
  onClickClose(icon) {
    if (icon == 'assets/images/cross8.png') {
      this.searchicon = 'assets/images/search8.svg';
    }
    this.showDropdown = false;
  }

  getUserPlans(){
    var phone =   this.userPhone;
    if(this.search_country_id == 2 || this.search_country_id == 1) 
  {
    
   

      if(this.search_country_id == 3 || this.search_country_id == 26)
      {
        phone =   phone.substring(2);
      }
      else
      {
        phone =   phone.substring(1);
      }
  }
    this.planService.getPlanInfo(phone).subscribe(
      (res:any)=>{
        console.log(res);
        this.plan = res;
         
        this.balance = this.plan.Balance;
          
      }
    );

 
  }

  generateAlphabets()
  {
     var alpha = []
    for (let i = 65; i <= 90;i++) {
        alpha.push(String.fromCharCode(i));
        this.alphabets = alpha;
    }
  }
  changeAlphabet(obj:string)
  {
    this.alphabet = obj;
    this.currency_rate = [];
    this.getRatesList();
  }
  getRatesList()
  {
    //this.customer_id ='2008999' ;
    //this.pin_no = '1112267924' ;
    //this.alphabet = 'A';
/*
    var body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
    body = body+"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n    <soap:Header>\r\n ";  
    body = body+"<Raza_AuthHeader xmlns=\"http://tempuri.org/\">\r\n";    
    body = body+"<AuthUsername>AjinTestUser</AuthUsername>\r\n";   
    body = body+"<AuthPassword>AJTeStPWD!FSTr</AuthPassword>\r\n    </Raza_AuthHeader>\r\n";    
    body = body+"</soap:Header>\r\n    <soap:Body>\r\n";    
    body = body+"<GetOnlinePlanRates_Json1 xmlns=\"http://tempuri.org/\">\r\n";      
    //body = body+"<customerID>2008999</customerID>\r\n";   
    body = body+"<customerID>"+this.customer_id+"</customerID>\r\n";  
    //body = body+"<Pin>1112267924</Pin>\r\n ";   
    body = body+"<Pin>"+this.pin_no+"</Pin>\r\n ";  
    body = body+"<Alphabet>"+this.alphabet+"</Alphabet>\r\n  ";  
    body = body+"</GetOnlinePlanRates_Json1>\r\n ";   
    body = body+"</soap:Body>\r\n";    
    body = body+"</soap:Envelope>";
 
*/
    var body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
    body = body+"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n    <soap:Header>\r\n ";  
    body = body+"<Raza_AuthHeader xmlns=\"http://tempuri.org/\">\r\n";    
    body = body+"<AuthUsername>AjinTestUser</AuthUsername>\r\n";   
    body = body+"<AuthPassword>AJTeStPWD!FSTr</AuthPassword>\r\n    </Raza_AuthHeader>\r\n";    
    body = body+"</soap:Header>\r\n    <soap:Body>\r\n";    
    body = body+"<GetOnlinePlanRates_Json2 xmlns=\"http://tempuri.org/\">\r\n";      
    //body = body+"<customerID>2008999</customerID>\r\n";   
    body = body+"<customerId>"+this.customer_id+"</customerId>\r\n";  
    //body = body+"<Pin>1112267924</Pin>\r\n ";   
    body = body+"<pin>"+this.pin_no+"</pin>\r\n ";  
    body = body+"<destination>"+this.alphabet+"</destination>\r\n  ";  
    //body = body+"<destination>ind</destination>\r\n  "; 
    body = body+"</GetOnlinePlanRates_Json2>\r\n ";   
    body = body+"</soap:Body>\r\n";    
    body = body+"</soap:Envelope>";
 
    //console.log(body);
    this.planService.countryRates(body).subscribe(
      res => {
        var res_arr = res.split("<?xml version");
        if(res_arr[0])
        {
          this.allowAutoload = true;
          var plans_list = JSON.parse(res_arr[0]);
          this.plans_list     = plans_list;
          if(plans_list.AmountChargedCurrencyCode == 'USD' || plans_list.AmountChargedCurrencyCode == 'CAD')
          {
            this.plan_exchange_rate = 1;
            this.plan_charg_in_currency = plans_list.AmountChargedCurrencyCode;
            //this.plan_charg_in_currency = 'USD';
          }
          
          else
          {
            this.plan_exchange_rate = plans_list.ExchangeRate;
            this.plan_charg_in_currency = plans_list.AmountChargedCurrencyCode;
          }
          
          this.plan_currency_code = plans_list.CurrencyCode;
          
         // this.plan_charg_in_currency = 'USD';

           console.log(this.plans_list);
         // this.currency_rate.push(plans_list.PlanRates);
          if(plans_list.PlanRates && plans_list.PlanRates.length>0)
          {
            for(var i=0; i<plans_list.PlanRates.length; i++)
            {
              this.currency_rate.push(plans_list.PlanRates[i]);
            }
          }
        //  this.currency_rate =plans_list.PlanRates;
          this.show_currency_rate = true;
          this.SubCurrencySymbol = plans_list.SubCurrencySymbol;
        }
        

         this.printJson();
      },
      (err: any) => {
        console.log(err);
        
      }
    ); 
  }
  printJson()
  {
    console.log(this.currency_rate[0]);
    //console.log(this.plans_list);
  }
 
  scrolling=(s)=>{
    let sc = s.target.scrollingElement.scrollTop;//+s.target.scrollingElement.height;
    this.viewHeight = this.elementView.nativeElement.offsetHeight;
    console.log(this.viewHeight +" & "+parseFloat(sc+700))

    if(parseFloat(sc+500) >=(this.viewHeight) && this.allowAutoload ==true )
    {
      this.allowAutoload = false;
      this.overEvent(true);
      return false
    }
     
  }

  overEvent(obj)
  {
    var alpha;
     if(obj == true)
     {
      for (let i = 65; i <= 90;i++) {
        if(this.alphabet == String.fromCharCode(i) && this.alphabet!= 'Z' )
         {
          this.alphabet = String.fromCharCode(i+1);
          this.getRatesList();
          return false;
         }
       }
     }
  
  }
 

  setCurrentCountry()
  {
    //localStorage.removeItem('session_key');
      let countries_list = [{CountryId:1,CountryName:"U.S.A",CountryCode:'1'},
    {CountryId:2,CountryName:"CANADA",CountryCode:'1'},
    {CountryId:3,CountryName:"U.K",CountryCode:'44'}];

    for(var j = 0; j <  countries_list.length;j++){
      if(countries_list[j].CountryId == this.search_country_id)
      { 
        const setting = new CurrentSetting();
        setting.country = countries_list[j];
         this.razaEnvService.setCurrentSetting(setting);
      }
  } 
}
   
   
continue()
{
   //this.router.navigateByUrl('/mobile_pay');
 
   this.router.navigate(['/mobile_pay']);
   
}

toFixed(num) {
  /*fixed = fixed || 0;
  fixed = Math.pow(10, fixed);
  return Math.floor(num * fixed) / fixed;
*/

return Math.floor(num*10)/10;
 //return ( num * 10  / 10 ).toFixed(1)
}
toFloor(num)
{
  var fixed = 0;
  fixed = Math.pow(10, fixed);
  return Math.floor(num * fixed) / fixed;
}
get_fix_amount(num)
{
  var number = num;
  if(num < 1)
  {
    //console.log(num);
    ///var parts = num.toString().split('.');
    var parts = (num*100)* 100  / 100;
    number = parts;
    //console.log(parts);
    //number =  parts[1].substring(0, 2);
    //number = parseFloat(parts) 
    //number = this.firstDigit(number);
  }
 

 return number;
}

currencyPrice(num)
{
  //return Math.round(num*this.plan_exchange_rate);
  var amount = num*this.plan_exchange_rate;
  if(amount >= 100 )
  {}
    amount = amount/100;
    return ( amount * 100  / 100 ).toFixed(4)
  //return this.toFloor(num*this.plan_exchange_rate)
}


currencyPrice1(num)
{
  //return Math.round(num*this.plan_exchange_rate);
  var amount = num*this.plan_exchange_rate;
  if(amount >= 100 )
  {}
    amount = amount/100;
    var string_amt = ( amount * 100  / 100 ).toFixed(4);
    const str = String(string_amt);
    
    return str.slice(0, -2);
  //return this.toFloor(num*this.plan_exchange_rate)
}


onChange() {
  if (this.isAutorefill == true) {
     
     this.isAutorefill = false;
     this.getplanNormal ()
     
   }
  else {
    this.isAutorefill = true;
    this.getplanWithDiscount ()
   }
}

rate_array(amount, obj, is_last)
{
    var dlr = amount;
     obj = this.rateDenomination;
    var caling_amt = dlr*this.plan_exchange_rate;
    console.log(obj, " and plan_exchange_rate ",this.plan_exchange_rate );
    var new_call_rate = this.currencyPrice(obj.CallingRate)
   
    console.log("new_call_rate "+new_call_rate);
    console.log("caling_amt "+caling_amt);

    //var calling_min = ((caling_amt/parseFloat(new_call_rate))*100 )
    var calling_min = ((caling_amt/parseFloat(new_call_rate)) )
    console.log(calling_min);     calling_min = ( calling_min * 10  / 10 );//.toFixed(2);;
    
    var rate = obj.CallingRate*this.plan_exchange_rate;
    rate = rate/100;
    var per_min = ( rate * 10  / 10 ).toFixed(2); //this.get_fix_amount(caling_amt/calling_min);
    var  charge_currency_amt = caling_amt;
    if(this.plans_list.CurrencyCode != this.plan_charg_in_currency )
    {
      charge_currency_amt = dlr;
    }
    else
    {
      charge_currency_amt = caling_amt;
    }

    return {
      'is_last':is_last,
      'hundred_price':caling_amt,
      amount_usd:charge_currency_amt, 
      'amount':this.toFixed(caling_amt), 
      'minutes':this.toFloor(calling_min), 
      'rate_pm': per_min }
}


rate_array_with_disc(amount, obj, is_last, discount, amount_change)
{
    var dlr = amount
    var disc_amt = discount;
     obj = this.rateDenomination;
     console.log(obj, " and plan_exchange_rate ",this.plan_exchange_rate );
    var caling_amt = this.toFixed(dlr*this.plan_exchange_rate);
    this.hundred_price = this.toFixed(caling_amt);
    //var calling_min = (dlr/obj.CallingRate)*100;
    var new_call_rate = this.currencyPrice(obj.CallingRate)
    //var calling_min = (caling_amt/parseFloat(new_call_rate)*100 )
    var calling_min = (caling_amt/parseFloat(new_call_rate))
    
    var charge_currency_amt = 0;
    if(this.plans_list.CurrencyCode != this.plan_charg_in_currency )
    {
      charge_currency_amt = dlr;
    }
    else
    {
      charge_currency_amt = discount*this.plan_exchange_rate;
    }
 
    var new_amt = this.get_fix_amount(( ((disc_amt*this.plan_exchange_rate)/calling_min)* 100  / 100 )); 
    var per_min = ( new_amt * 10  / 10 ).toFixed(2);//// 
    //var per_min = this.toFixed(new_amt); //this.toFixed(obj.CallingRate*this.plan_exchange_rate);//this.get_fix_amount(caling_amt/calling_min);

    if(amount_change == 'fixed')
    {
      
      amount = (amount==100)?90:amount;
      charge_currency_amt = amount*this.plan_exchange_rate;

       

      if(amount==90) 
      {
        var min = this.toFloor(calling_min)
        var tenmin = (min/100*90)*10/100 ;
       
        calling_min = (calling_min+(tenmin));
      }
      else
        calling_min = (calling_min+(calling_min*10/100));
      
       
    
    }

    return { 
      'is_last':is_last,
      'hundred_price':this.hundred_price,
      amount_usd:charge_currency_amt, 
      'amount': this.toFixed(charge_currency_amt),
      'minutes':this.toFloor(calling_min), 
      'rate_pm': per_min 
    }
}
getplanWithDiscount()
{
  this.planInfo         = [];
  this.selected_country = this.rateDenomination;
  var obj = this.rateDenomination;
  var plan_arr = this.rate_array_with_disc(5, obj, 'no', 4.5, 'fixed');//{'is_last':'no',amount_usd:charge_currency_amt, 'amount':this.toFixed(caling_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':this.toFixed(per_min)}
  this.planInfo.push(plan_arr);
  plan_arr = this.rate_array_with_disc(10, obj, 'no', 9, 'fixed');//{'is_last':'no',amount_usd:charge_currency_amt,'amount':this.toFixed(caling_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':this.toFixed(per_min)}
    this.planInfo.push(plan_arr);
    plan_arr = this.rate_array_with_disc(20, obj, 'no', 18, 'fixed');//{'is_last':'no',amount_usd:charge_currency_amt,'amount':this.toFixed(caling_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':this.toFixed(per_min)}
    this.planInfo.push(plan_arr);
    plan_arr = this.rate_array_with_disc(50, obj, 'no', 45, 'fixed');//{'is_last':'no',amount_usd:charge_currency_amt,'amount':this.toFixed(caling_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':this.toFixed(per_min)}
    this.planInfo.push(plan_arr);
  
    plan_arr = this.rate_array_with_disc(100, obj, 'yes', 80, 'fixed');// {'is_last':'yes', amount_usd:charge_currency_amt,'amount':this.toFixed(charge_currency_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':per_mina}
  this.planInfo.push(plan_arr);
 
  this.step_one = true;
}
getplanNormal ()
{
  this.planInfo         = [];
  this.selected_country = this.rateDenomination;
  var obj = this.rateDenomination;
  var plan_arr = this.rate_array(5, obj, 'no');//{'is_last':'no',amount_usd:charge_currency_amt, 'amount':this.toFixed(caling_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':this.toFixed(per_min)}
  this.planInfo.push(plan_arr);
  plan_arr = this.rate_array(10, obj, 'no');//{'is_last':'no',amount_usd:charge_currency_amt,'amount':this.toFixed(caling_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':this.toFixed(per_min)}
    this.planInfo.push(plan_arr);
    plan_arr = this.rate_array(20, obj, 'no');//{'is_last':'no',amount_usd:charge_currency_amt,'amount':this.toFixed(caling_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':this.toFixed(per_min)}
    this.planInfo.push(plan_arr);
    plan_arr = this.rate_array(50, obj, 'no');//{'is_last':'no',amount_usd:charge_currency_amt,'amount':this.toFixed(caling_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':this.toFixed(per_min)}
    this.planInfo.push(plan_arr);
  
    plan_arr = this.rate_array_with_disc(100, obj, 'yes', 90, 'change');// {'is_last':'yes', amount_usd:charge_currency_amt,'amount':this.toFixed(charge_currency_amt), 'minutes':this.toFloor(calling_min), 'rate_pm':per_mina}
  this.planInfo.push(plan_arr);
 
  this.step_one = true;

}

select_to_country(obj:any)
{
   
    this.rateDenomination = obj;
    /*if(this.isAutorefill == true)
    this.getplanNormal ();
    else
    */
    this.isAutorefill = false
    this.getplanNormal ()
    //this.getplanWithDiscount();

    
  

   /* var caling_amt = dlr*this.plan_exchange_rate;
    var calling_min = (dlr/obj.CallingRate)*100;

    

    var per_min = this.toFixed(obj.CallingRate*this.plan_exchange_rate);//this.get_fix_amount(caling_amt/calling_min);
    var  charge_currency_amt = caling_amt;
    if(this.plans_list.CurrencyCode != this.plan_charg_in_currency )
    {
      charge_currency_amt = dlr;
    }
    else
    {
      charge_currency_amt = caling_amt;
    }*/
   

    /* dlr = 10
     caling_amt = dlr*this.plan_exchange_rate;
     calling_min = (dlr/obj.CallingRate)*100;
     per_min = this.toFixed(obj.CallingRate*this.plan_exchange_rate);//this.get_fix_amount(caling_amt/calling_min);
     if(this.plans_list.CurrencyCode != this.plan_charg_in_currency )
    {
      charge_currency_amt = dlr;
    }
    else
    {
      charge_currency_amt = caling_amt;
    }*/

     
/*
    dlr = 20
    caling_amt = dlr*this.plan_exchange_rate;
    calling_min = (dlr/obj.CallingRate)*100;
    per_min = this.toFixed(obj.CallingRate*this.plan_exchange_rate);//this.get_fix_amount(caling_amt/calling_min);
    if(this.plans_list.CurrencyCode != this.plan_charg_in_currency )
    {
      charge_currency_amt = dlr;
    }
    else
    {
      charge_currency_amt = caling_amt;
    }*/
   
/*
   dlr = 50
   caling_amt = dlr*this.plan_exchange_rate;
   calling_min = (dlr/obj.CallingRate)*100;
   per_min = this.toFixed(obj.CallingRate*this.plan_exchange_rate);//this.get_fix_amount(caling_amt/calling_min);
   if(this.plans_list.CurrencyCode != this.plan_charg_in_currency )
    {
      charge_currency_amt = dlr;
    }
    else
    {
      charge_currency_amt = caling_amt;
    }*/
 

  
  
}

get1SelectedClass(amt:any)
{
   
  if(this.selected_country && this.selected_country.CountryToId == amt)
  {
    return 'checked-payment';
  }
  else{
    return '';
  }
}
getSelectedClass(amt:any)
{
  if(this.selectedAmount == parseFloat(amt))
  {
    return 'checked-payment';
  }
  else{
    return '';
  }
}
 
getChecked(amt:any)
{
  if(this.amount == amt)
  {
    return true;
  }
  else{
    return false;
  }
}
  
  private getCountries() {
    
    this.countryService.getAllCountries().subscribe(
      (data: Country[]) => {
        this.allCountry = data;
       
      },
      (err: ApiErrorResponse) => console.log(err),
    );
  }
 
 
 
    
 
  
   
   
  androidFuntionRevert(toast) {
    //console.log(toast);
    try {
         window.androidCallBackInterface.callBackFormData(toast);
        } catch(err) {
       }
    }

    iosFuntionRevert(resultdata)
    {
      //console.log(resultdata);
      try {
           var message = resultdata;
            window.webkit.messageHandlers.callbackHandler.postMessage(message);
            } 
            catch(err) {
            }
 
    }


    onClickAmountOption(amount: number) {
      var payment_amount = ( amount * 10  / 10 ).toFixed(1)
      this.selectedAmount = amount;
      this.amount = payment_amount;
      const model: RechargeCheckoutModel = new RechargeCheckoutModel();
      model.purchaseAmount = parseFloat(payment_amount);
      model.couponCode = '';
      model.currencyCode = this.plan_charg_in_currency;
      model.cvv = '';
      model.planId = this.plan.PlanId
      model.transactiontype = TransactionType.MR;
      model.serviceChargePercentage = this.plan.ServiceChargePercent;
      model.planName = this.plan.CardName;
      model.countryFrom = this.plans_list.CountryFromId;
      model.countryTo = this.selected_country.CountryToId;
      model.cardId = this.plan.CardId;
      model.isAutoRefill = this.isAutorefill; 
      this.checkoutService.setCurrentCart(model);
      localStorage.setItem('currentCart', JSON.stringify(model))
      this.router.navigate(['/mobile_pay']);
     }

     processRecharge()
     {
      this.router.navigate(['/mobile_pay']);
     }
    closeApp()
    {
     // console.log(this.platform);
      if(this.platform == 'ios')
      {
        this.iosFuntionRevert('Mobile web view closed by client');
      }
      if(this.platform == 'android')
      {
        this.androidFuntionRevert('Mobile web view closed by client');
      }
       
     
    }
  
   
  goBack()
  {
      if(this.step_one == true)
      {
        this.step_one = false;
        this.amount = '';
      }
      else
    this.closeApp();
  
  }
   
   
  
  showAndroidToast(obj)
  {
  if(this.platform == 'ios')
    {
      this.iosFuntionRevert(obj);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(obj);
    }
  }
  get_promotion_class(){
    if(this.promotionCode!= ''){
      return 'top-space'
    }
    else{
      return '';
    }
  }

getPriceSymbol(num:number)
{
  var amount = num*this.plan_exchange_rate;
  if(amount >= 100 )
  {
    return this.plans_list.CurrencySymbol;
  }
  else{
    return this.SubCurrencySymbol;
  }
  

}
 

}


