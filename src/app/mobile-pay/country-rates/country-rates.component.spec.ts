import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryRatesComponent } from './country-rates.component';

describe('CountryRatesComponent', () => {
  let component: CountryRatesComponent;
  let fixture: ComponentFixture<CountryRatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryRatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryRatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
