import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopupErrorComponent } from './topup-error.component';

describe('TopupErrorComponent', () => {
  let component: TopupErrorComponent;
  let fixture: ComponentFixture<TopupErrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopupErrorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopupErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
