import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topup-error',
  templateUrl: './topup-error.component.html',
  styleUrls: ['./topup-error.component.scss']
})
export class TopupErrorComponent implements OnInit {
@Input() json_data: any[];
  errorMsg:String;
   
    constructor(
        public dialogRef: MatDialogRef<TopupErrorComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() {
       this.errorMsg = this.data.error_msg;
	   console.log(this.errorMsg);
     }

    onClickOk(): void{
        this.dialogRef.close(true);
    }

    closeIcon(): void {
        this.dialogRef.close(false);
      }
}
