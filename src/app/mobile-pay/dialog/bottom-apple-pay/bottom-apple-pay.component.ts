import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-bottom-apple-pay',
  templateUrl: './bottom-apple-pay.component.html',
  styleUrls: ['./bottom-apple-pay.component.scss']
})
export class BottomApplePayComponent implements OnInit {

  constructor(private _bottomSheetRef: MatBottomSheetRef<BottomApplePayComponent>) { }

  ngOnInit(): void {
  }
  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }
  close()
  {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

}
