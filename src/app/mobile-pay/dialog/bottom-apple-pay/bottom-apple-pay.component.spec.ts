import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomApplePayComponent } from './bottom-apple-pay.component';

describe('BottomApplePayComponent', () => {
  let component: BottomApplePayComponent;
  let fixture: ComponentFixture<BottomApplePayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BottomApplePayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomApplePayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
