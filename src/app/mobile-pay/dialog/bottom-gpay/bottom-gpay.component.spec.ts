import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomGpayComponent } from './bottom-gpay.component';

describe('BottomGpayComponent', () => {
  let component: BottomGpayComponent;
  let fixture: ComponentFixture<BottomGpayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BottomGpayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomGpayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
