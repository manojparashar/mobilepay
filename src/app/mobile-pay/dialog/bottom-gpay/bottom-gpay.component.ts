import { Component, OnInit } from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
@Component({
  selector: 'app-bottom-gpay',
  templateUrl: './bottom-gpay.component.html',
  styleUrls: ['./bottom-gpay.component.scss']
})
export class BottomGpayComponent implements OnInit {
  _bottomSheetgpay: any;

  constructor(private _bottomSheetRef: MatBottomSheetRef<BottomGpayComponent>) { }

  ngOnInit(): void {
  }
  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }
  close()
  {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }


}
