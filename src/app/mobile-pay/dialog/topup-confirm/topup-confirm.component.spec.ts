import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopupConfirmComponent } from './topup-confirm.component';

describe('TopupConfirmComponent', () => {
  let component: TopupConfirmComponent;
  let fixture: ComponentFixture<TopupConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopupConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopupConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
