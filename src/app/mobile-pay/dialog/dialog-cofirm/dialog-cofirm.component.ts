import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialog-cofirm',
  templateUrl: './dialog-cofirm.component.html',
  styleUrls: ['./dialog-cofirm.component.scss']
})


export class DialogCofirmComponent implements OnInit {
  @Input() json_data: any[];
  transType:String;
  topupPhone:String;
  topupAmount:String;
    constructor(
        public dialogRef: MatDialogRef<DialogCofirmComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() {
      this.transType = this.data.payment_type;
      this.topupPhone = localStorage.getItem('topupPhone')
      this.topupAmount = localStorage.getItem('topupAmount')
     // this.transType = this.data;
      console.log(this.data.payment_type);

      localStorage.removeItem('topupPhone');
      localStorage.removeItem('topupAmount');
     }

    onClickOk(): void{
        this.dialogRef.close(true);
    }

    closeIcon(): void {
        this.dialogRef.close(false);
      }
}
