import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogOperatorComponent } from './dialog-operator.component';

describe('DialogOperatorComponent', () => {
  let component: DialogOperatorComponent;
  let fixture: ComponentFixture<DialogOperatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogOperatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogOperatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
