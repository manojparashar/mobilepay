import { Component, OnInit, ViewEncapsulation, Injector } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
//import { MatCarousel, MatCarouselComponent } from '@ngmodule/material-carousel';
//import { TextMaskModule } from 'angular2-text-mask';

import { Subscription } from 'rxjs';
import { CountriesService } from '../../core/services/country.service';
import { Country } from '../../core/models/country.model';
import { RazaEnvironmentService } from '../../core/services/razaEnvironment.service';
import { SearchRatesService } from '../../rates/searchrates.service';
import { CurrentSetting } from '../../core/models/current-setting';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
 
import { Router, ActivatedRoute } from '@angular/router';

import { Title } from '@angular/platform-browser';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthenticationService } from '../../core/services/auth.service';
import { AppBaseComponent } from '../../shared/components/app-base-component';
import { timer } from 'rxjs';
import { autoCorrectIfPhoneNumber, isValidPhoneOrEmail } from '../../shared/utilities';
import { environment } from 'environments/environment';
import { ErrLoginComponent } from '../dialog/err-login/err-login.component';
declare var jQuery: any;
interface Window {
  webkit?: any;
  androidCallBackInterface:any;
}
declare var window: Window;
@Component({
  selector: 'app-webview',
  templateUrl: './webview.component.html',
  styleUrls: ['./webview.component.scss']
})
export class WebviewComponent extends AppBaseComponent implements OnInit {
  
  loginForm: FormGroup;
  returnUrl: string;
  showDropdown: boolean = false;
  showForgotPass: boolean = false;
  allCountry: any[];
  countryFrom: Country[]
  currentSetting$: Subscription;
  currentSetting: CurrentSetting;
  showPlaceholder: boolean = true;

  timeLeft: number = 120;
  interval;
  otpSend: boolean = false;
  subscribeTimer: number;
  forgotPasswordForm: FormGroup;
  isEnableResendOtp: boolean = false;
  captchaKey = environment.captchaKey;

  userPhone : any;
  userPass : any;
  userToken : any;
  enc_key:any;
  key_arr:any;
  planId: string;

  phone: string;
search_country:any='US';
search_country_id:any=1;
 
isEnableOtherPlan: boolean = false;
isAutoRefillEnable: boolean = false;
paymentSubmitted: boolean;
amount:any=10;
processType:any=''; 
promotionCode:string='';
platform:string='';
countryFromId:number;
topup_no:string='';
customer_id:any;
pin_no:any;
alphabet:any;
  searchicon: string = '../assets/images/search8.svg';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
     
    private countryService: CountriesService,
    private searchRatesService: SearchRatesService,
    private razaEnvService: RazaEnvironmentService,
    private dialog: MatDialog,
    public matDialog: MatDialog,
    public signupDialog: MatDialog,
    _injector: Injector
  ) {
    super(_injector);
  }

  private getCountryFrom() {
    this.countryService.getFromCountries().subscribe((res: Country[]) => {
      this.countryFrom = res;
    });
  }
  
  ngOnInit() {
    this.enc_key    = this.route.snapshot.paramMap.get('post_data');
    localStorage.setItem('web_enc_key', this.enc_key);
 
    let data_list   =  JSON.parse(atob(this.enc_key));
    console.log(this.enc_key);
    console.log(data_list);
    this.key_arr    = data_list;
     
    this.userPhone  = (data_list.phone)?data_list.phone:'';;
    this.userPass   = (data_list.password)?data_list.password:'';
    this.userToken = (data_list.token)?data_list.token:'';
    this.promotionCode = (data_list.coupon_code)?data_list.coupon_code:'';
    this.search_country_id  = data_list.country_id;
    
    this.countryFromId      = this.search_country_id;
    let device_info         = data_list.de; 
    this.platform = device_info;
     //this.loginWithToken();
     this.search_country_id  = data_list.country_id;
     
    this.countryFromId      = this.search_country_id;
     
    let top_no              = ( data_list.top_no && data_list.top_no !='' ) ?data_list.top_no:0;
    this.topup_no           = top_no;

    this.customer_id =(data_list.customer_id)?data_list.customer_id:'' ;
    this.pin_no = (data_list.pin_no)?data_list.pin_no:'' ;
    this.alphabet = (data_list.alphabet)?data_list.alphabet:'A';


    this.processLoginSteps();
    this.currentSetting$ = this.razaEnvService.getCurrentSetting().subscribe(res => {
      this.currentSetting = res;
    });
    this.getCountryFrom();

    localStorage.setItem('platform', this.platform);

  }


  processLoginSteps()
  {
    localStorage.setItem('prev_token', this.userToken);
    localStorage.removeItem('currentUser');
    var d = new Date();
      var gtime = d.getTime()+" ";
       localStorage.setItem('last_login_time', gtime);
 
       
  if(this.userPhone == '13129758542' ) 
  
  // if(this.userPhone == '16475050136' )
    {
      this.autoLoginUser();
    }
    else
    {
      this.loginWithToken();
    }
   
  }
 
  loginWithToken() 
  {
    
      
       let body = {
          username: this.userToken,
          password: '0000000000',
          captcha: ''
        };
     
        this.authService.loginwToken(body, false, "Y").subscribe((response) => {
          //this.authService.login(body, false, "Y").subscribe((response) => {
          if (response != null) {
            this.routProcess();
          }  
        },
        (error) => {
         const dialogRef = this.dialog.open(ErrLoginComponent, {
            data: {
              success: 'success',
            }
          });
           
          dialogRef.afterClosed().subscribe(result => {
            this.showAndroidToast('Mobile web view closed by client.');
          });
        });

       /* this.executeCaptcha('login').subscribe((token) =>{ },(error) => {
          console.log("error "+error); 
          } ) */

  }
  
  ////////// Forgor password /////////

 
  autoLoginUser() 
  {
    const phoneOrEmail = this.userPhone;
    /*this.executeCaptcha('login').subscribe((token) =>{},(error) => {
        console.log(error.error_description);
          //console.log("error "+error); 
         // this.closeApp();
    }
    )
	*/
    let body = {
        username: phoneOrEmail,
        password: this.userPass,
        captcha: ''
        
      };
      
      this.authService.login(body, false, "Y").subscribe((response) => {
        if (response != null) {
           
           this.routProcess();
           
        
        }  
      },
        (error) => {
          console.log(error.error_description);
         // this.closeApp();
        });
     
  }

  routProcess()
  {
    
      if(this.topup_no != '')
      {
        this.router.navigate(['/topup/']);
      }
      
      else if(this.customer_id !='')
      {
        this.router.navigate(['/country_rates/']);
      }
      else
      this.router.navigate(['/plans']);
      
     
  }
  showAndroidToast(obj)
  {
  if(this.platform == 'ios')
    {
      this.iosFuntionRevert(obj);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(obj);
    }
  }
   
  iosFuntionRevert(resultdata)
  {
    console.log(resultdata);
    try {
         var message = resultdata;
          window.webkit.messageHandlers.callbackHandler.postMessage(message);
          } 
          catch(err) {
          }

  }
   
  androidFuntionRevert(toast) {
    console.log(toast);
    try {
         window.androidCallBackInterface.callBackFormData(toast);
        } catch(err) {
       }
    }

  


   

}