import { Component, OnInit , HostListener} from '@angular/core';
import {FormControl} from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
declare var jQuery: any;
interface Window {
  webkit?: any;
  androidCallBackInterface:any;
}
declare var window: Window;
@Component({
  selector: 'app-new-privacy-policy',
  templateUrl: './new-privacy-policy.component.html',
  styleUrls: ['./new-privacy-policy.component.scss']
})
export class NewPrivacyPolicyComponent implements OnInit {

  mode = new FormControl('over');
  headerValue : number = 1;
  platform:string='';
  enc_key:any;
  country_id : number;
  phone_numbers:any;
  phoneNumber:any='';
  display_no:any='';
  constructor(private router: Router,
    private route: ActivatedRoute, private titleService: Title) { }

  ngOnInit() {
    this.enc_key    = this.route.snapshot.paramMap.get('post_data');
    localStorage.setItem('web_enc_key', this.enc_key);
 
    let data_list   =  JSON.parse(atob(this.enc_key));

    let device_info         = data_list.de; 
    this.platform = device_info;
    this.titleService.setTitle('Privacy Policy'); 
  }

  // @HostListener('window:scroll', ['$event'])
  //   checkScroll() {
  //     const scrollPosition = window.pageYOffset
      
  //     if(scrollPosition > 5){
  //       this.headerValue = 2;

  //     }else{
  //       this.headerValue = 1;
  //     }

  //   }


    goBack(obj:any)
    {
        var msg = '';
        if(obj !='')
        {
          msg = obj;
        }
        else{
          msg = 'Mobile web view closed by client'
        }
        msg = 'Mobile web view closed by client'
        this.closeApp(msg);
    }
  
    closeApp(obj:any)
    {
      var msg = '';
      
      if(obj !='')
      {
        msg = obj;
      }
      else{
        msg = 'Mobile web view closed by client'
      }
      msg = 'Mobile web view closed by client'
      if(this.platform == 'ios')
      {
        this.iosFuntionRevert(msg);
      }
      if(this.platform == 'android')
      {
        this.androidFuntionRevert(msg);
      }
   }
   showAndroidToast(obj)
   {
   if(this.platform == 'ios')
     {
       this.iosFuntionRevert(obj);
     }
     if(this.platform == 'android')
     {
       this.androidFuntionRevert(obj);
     }
   }
    
   iosFuntionRevert(resultdata)
   {
     console.log(resultdata);
     try {
          var message = resultdata;
           window.webkit.messageHandlers.callbackHandler.postMessage(message);
           } 
           catch(err) {
           }
  
   }
  
   androidFuntionRevert(toast) {
     console.log(toast);
     try {
          window.androidCallBackInterface.callBackFormData(toast);
         } catch(err) {
        }
     }

}