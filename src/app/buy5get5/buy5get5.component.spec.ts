import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Bu5get5Component } from './bu5get5.component';

describe('Bu5get5Component', () => {
  let component: Bu5get5Component;
  let fixture: ComponentFixture<Bu5get5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Bu5get5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Bu5get5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
