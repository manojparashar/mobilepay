import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';

import { AnonomysOnlyGuard } from './core/guards/anonomys-only.guard';
import { HomepageComponent } from './home/homepage/homepage.component';
//import { MobiledealsComponent } from './mobiledeals/mobiledeals.component';

//import { LearnmoreComponent } from './globalrates/learnmore/learnmore.component';

//import { CreateInstanceComponent } from './create-instance/create-instance.component';

//import { Page404Component } from './page404/page404.component';
//import { Buy5get5Component } from './deals/pages/buy5get5/buy5get5.component';
//import { AutoLoginComponent } from './auto-login/auto-login.component'; 
import { PlanComponent } from './mobile-pay/plan/plan.component';
import { PaymentComponent } from './mobile-pay/payment/payment.component';
import { CartResolverService } from './checkout/services/cart-resolver.service';
import { CartRequireGuard } from './checkout/guards/cart-require.guard';
import { TopupComponent } from './mobile-pay/topup/topup.component';
//import {WebViewComponent} from './web-view/web-view.component';
//import {WebPayComponent} from './web-pay/web-pay.component';
//import { WebviewComponent } from './mobile-pay/webview/webview.component';

import { CountryRatesComponent } from './mobile-pay/country-rates/country-rates.component';
import { TermsConditionComponent } from './mobile-pay/terms-condition/terms-condition.component';
import { ContactUsComponent } from './mobile-pay/contact-us/contact-us.component';
import { Buy5get5Component } from './buy5get5/buy5get5.component'
import { ThankyouComponent } from './thankyou/thankyou.component';
import { BuyOneGetOneComponent } from './mobile-pay/buy-one-get-one/buy-one-get-one.component'
const routes: Routes = [
  { path: '', component: HomepageComponent, pathMatch: 'full' },
  { path: 'buy5get5/:post_data', component: Buy5get5Component},
  { path: 'thankyou', component: ThankyouComponent},
  { path: 'buy1get1', component: BuyOneGetOneComponent},
  
  //{ path: '', component: PaymentComponent  },
  { path: 'static/:post_data', component: HomepageComponent},
  //{ path: 'webview/:post_data', component: WebviewComponent },
  { path: 'topup/:post_data', component: TopupComponent },
  { path: 'country_rates', component: CountryRatesComponent },
  { path: 'topup', component: TopupComponent },
  { path: 'plans/:post_data', component: PlanComponent },
  { path: 'plans', component: PlanComponent },
  { path: 'mobile_pay', component: PaymentComponent,canActivate: [CartRequireGuard],resolve: { cart: CartResolverService } },
 
 // { path: 'generate_nonce', component: CreateInstanceComponent, pathMatch: 'full' },
  //{ path: 'mobileapp', component: HomepageComponent, data: { scrollToMobileApp: true } },

  //{ path: 'auth', loadChildren: () => import('./authentication/auth.module').then(m => m.AuthModule), canActivate: [AnonomysOnlyGuard] },
  { path: 'account', loadChildren: () => import('./accounts/account.module').then(m => m.AccountModule), canActivate: [AuthGuard] },
  { path: 'recharge', loadChildren: () => import('./recharge/recharge.module').then(m => m.RechargeModule) },

 //{ path: 'aboutus', loadChildren: () => import('./aboutus/aboutus.module').then(m => m.AboutusModule) },
 // { path: 'howitworks', loadChildren: () => import('./howitworks/howitworks.module').then(m => m.HowitworksModule) },
  //{ path: 'contactus', loadChildren: () => import('./contactus/contactus.module').then(m => m.ContactusModule) },
  //{ path: 'becomeapartner', loadChildren: () => import('./becomeapartner/becomeapartner.module').then(m => m.BecomeapartnerModule) },

 // { path: 'features', loadChildren: () => import('./features/features.module').then(m => m.FeaturesModule) },
 // { path: 'searchrates', loadChildren: () => import('./rates/searchrates.module').then(m => m.SearchratesModule) },
 // { path: 'faq', loadChildren: () => import('./faq/faq.module').then(m => m.FaqModule) },
 // { path: 'feedback', loadChildren: () => import('./feedback/feedback.module').then(m => m.FeedbackModule) },
  //{ path: 'refer-a-friend', loadChildren: () => import('./refer/refer.module').then(m => m.ReferModule) },

  { path: 'mobiletopup', loadChildren: () => import('./mobiletopup/mobiletopup.module').then(m => m.MobiletopupModule) },
  //{ path: 'virtualnumber', loadChildren: () => import('./virtualnumber/virtualnumber.module').then(m => m.VirtualnumberModule) },

  //{ path: 'mobileterms', loadChildren: () => import('./mobileterms/mobileterms.module').then(m => m.MobiletermsModule) },
  //{ path: 'termsandcondition', loadChildren: () => import('./termsandcondition/termsandcondition.module').then(m => m.TermsandconditionModule) },
  //{ path: 'landing-page', loadChildren: () => import('./landing-page/landing-page.module').then(m => m.LandingPageModule) },

  //{ path: 'support', loadChildren: () => import('./privacypolicy/privacypolicy.module').then(m => m.PrivacypolicyModule) },

 // { path: 'callafrica', loadChildren: () => import('./callafrica/callafrica.module').then(m => m.CallafricaModule) },
  //{ path: 'callasia', loadChildren: () => import('./callasia/callasia.module').then(m => m.CallasiaModule) },
  //{ path: 'deals', loadChildren: () => import('./deals/deals.module').then(m => m.DealsModule) },

 // { path: 'freetrial', loadChildren: () => import('./freetrial/freetrial.module').then(m => m.FreeTrialModule) },
  { path: 'purchase', loadChildren: () => import('./purchase/purchase.module').then(m => m.PurchaseModule) },
  { path: 'checkout', loadChildren: () => import('./checkout/checkout.module').then(m => m.CheckoutModule) },

  { path: 'new', loadChildren: () => import('./new-route/new-route.module').then(m => m.NewRouteModule) },
  { path: 'mobileapp', loadChildren: () => import('./new-route/new-route.module').then(m => m.NewRouteModule) },
 
  //{ path: 'web_pay', component: WebPayComponent },
  { path: 'terms_condition/:post_data', component: TermsConditionComponent },
  { path: 'contact-us/:post_data', component: ContactUsComponent },

  { path: 'moto', loadChildren: () => import('./moto/moto.module').then(m => m.MotoModule) },
 
  //{ path: 'mobiledeals', component: MobiledealsComponent, data: { scrollToMobileApp: true } },
  //{ path: 'auto-sign-in/:userPhone/:userPass', component: AutoLoginComponent },
 //{ path: 'learnmore', component: LearnmoreComponent },
  //{path: '404', component: Page404Component},
  //{path: 'buy_five_get_five', component: Buy5get5Component},
  //{ path: '**', redirectTo: './404' }, 
 // { path: 'mobile_pay', loadChildren: () => import('./mobile-pay/mobile-pay.module').then(m => m.MobilePayModule) },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'top', relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]

})
export class AppRoutingModule {

 }