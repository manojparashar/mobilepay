export enum PaymentMethod {
    CreditCard = 'CreditCard',
    Paypal = 'PayPal',
	GooglePay = 'GooglePay',
    ApplePay = 'ApplePay'
}