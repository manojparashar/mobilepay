import { TestBed } from '@angular/core/testing';

import { BrainTreeApplepayService } from './brain-tree-applepay.service';

describe('BrainTreeApplepayService', () => {
  let service: BrainTreeApplepayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BrainTreeApplepayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
