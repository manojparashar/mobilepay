import { Observable, pipe, of, Observer, Subject } from "rxjs";

import { Order } from "../models/cardinal-cruise.model";
import { HttpClient } from "@angular/common/http";
import { CustomErrorHandlerService } from "../../core/services/custom-error-handler.service";
import { Api } from "../../core/services/api.constants";
import { catchError } from "rxjs/operators";
import { EventEmitter, Injectable, NgZone } from "@angular/core";
import { TransactionType, TransactionRequest } from "../models/transaction-request.model";
import { ApiErrorResponse } from "../../core/models/ApiErrorResponse";
import { TransactionProcessFacadeService } from "./transactionProcessFacade";
import { TransactionMobProcessBraintreeService } from "./transaction-mob-process-braintree.service";
import { TransactionProcessBraintreeService } from "./transactionProcessBraintree";
import { LoaderService } from "../../core/spinner/services";
import * as braintree from 'braintree-web';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../../shared/dialog/error-dialog/error-dialog.component';
import { ErrorDialogModel } from '../../shared/model/error-dialog.model';

import { AddCreditcardDialog } from '../../accounts/dialog/add-creditcard-dialog/add-creditcard-dialog';
import { State, BillingInfo } from '../../accounts/models/billingInfo';

import { CustomerService } from '../../accounts/services/customerService';
import { RazaSnackBarService } from '../../shared/razaSnackbar.service';

import { CreditCard } from '../../accounts/models/creditCard';
import { PaymentOptionsComponent } from '../../checkout/pages/payment-info/payment-options/payment-options.component'
import { NewPlanCheckoutModel, ICheckoutModel } from '../../checkout/models/checkout-model';
import { IPaypalCheckoutOrderInfo, ActivationOrderInfo, RechargeOrderInfo, ICheckoutOrderInfo, MobileTopupOrderInfo } from '../../payments/models/planOrderInfo.model';
import { BraintreeService } from '../../payments/services/braintree.service';


import { ApiProcessResponse } from '../../core/models/ApiProcessResponse';
import { isNullOrUndefined } from "../../shared/utilities";
import { ValidateCouponCodeResponseModel, ValidateCouponCodeRequestModel } from '../../payments/models/validate-couponcode-request.model';
import { Account } from "../models/cardinal-cruise.model";
import { GenerateTransactionRequestModel } from "../models/generate-transaction-req.model";
import { ActivatedRoute } from '@angular/router';
import { AnonymousSubject } from "rxjs/internal-compatibility";
 
import { applePay, ApplePayStatusCodes, ApplePaySession } from 'braintree-web/apple-pay';
declare var Cardinal: any;
declare var jQuery: any;
interface Window {
  webkit?: any;
  androidCallBackInterface: any;
  ApplePaySession: ApplePaySession
}
declare var window: Window;

@Injectable({ providedIn: 'root' })


 
export class BrainTreeApplepayService {
  private _jwtToken: string;
  cvvStored: string;
  billingInfo: BillingInfo;
  customerSavedCards: CreditCard[];
  currentCart: ICheckoutModel;

  paymentProcessor: any;
  constructor(
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private ngZone: NgZone,
    public dialog: MatDialog,
    //private paymentOptionComponent = new PaymentOptionsComponent <any>();
    private errorHandleService: CustomErrorHandlerService,

    private transactionBraintreeService: TransactionMobProcessBraintreeService,
    private loaderService: LoaderService,
    private customerService: CustomerService,
    private razaSnackbarService: RazaSnackBarService,
    private braintreeService: BraintreeService,

    private transactionProcessBraintree: TransactionMobProcessBraintreeService,
  ) {

  }

  initTransaction(transaType: TransactionType, order: Order): Observable<TransactionRequest | ApiErrorResponse> {
    return this.httpClient.post<TransactionRequest>(`${Api.transactions.init}/${transaType}`, order)
      .pipe(
        catchError(err => this.errorHandleService.handleHttpError(err))
      );
  }

  /*
   * init cardinal transaction.
   */
  private initPayment(model: TransactionRequest) {

    Cardinal.setup("init", {
      jwt: this._jwtToken,
      displayLoading: true
    });

    const service = this.loaderService;
    Cardinal.on("ui.render", function () {
      // console.log('ui.render');
      service.displayHard(false);
    });
  }

  private registerPaymentSetupComplete() {
    Cardinal.on('payments.setupComplete', function () {
      //  console.log("payment setup complete");
    });
  }

  private validatePaymentResponse(model: TransactionRequest, nonce: any) {
    let service: TransactionMobProcessBraintreeService = this.transactionBraintreeService;
    const loaderService = this.loaderService;
    service.processTransaction(model, nonce);
     
  }

  private setupCardinalTransaction(model: TransactionRequest) {

    this.loaderService.displayPaymentHard(true);
    /* Cardinal.configure({
       logging: {
         level: "on",
       }
     });
     this.initPayment(model);
     this.registerPaymentSetupComplete();
     this.validatePaymentResponse(model);*/
  }

  startAppePayTransaction(model: TransactionRequest): void {

    //this.loaderService.displayPaymentHard(true);
    this.createClient(model);
    
  }
 
  openErrorDialog(error: ErrorDialogModel): void {
    this.dialog.open(ErrorDialogComponent, {
      data: { error }
    });
  }

  createClient(model: TransactionRequest): void {
    // var cart_amount = model.checkoutOrderInfo.checkoutCart.totalAmount();
    console.log(TransactionType.MR);
    console.log(model);
    var currency = model.checkoutOrderInfo.checkoutCart.currencyCode;
    //this.httpClient.get(Api.braintree.generateToken)


   
    this.httpClient.get(Api.braintree.generateToken + '/' + currency)
      .subscribe((data: any) => {
        var token = data.token;
        let country_code = 1
       

        if (model.checkoutOrderInfo.checkoutCart.currencyCode == 'USD')
          country_code = 1;
        if (model.checkoutOrderInfo.checkoutCart.currencyCode == 'CAD')
          country_code = 2;
        if (model.checkoutOrderInfo.checkoutCart.currencyCode == 'GBP')
          country_code = 3;

         
         var Apple_PaymentInstance = '';
         let service: TransactionMobProcessBraintreeService = this.transactionBraintreeService;
       braintree.client.create({
          //authorization: token
          authorization: 'sandbox_24mbqd8n_vy52cdpt3p33rykc',
          
        }).then(function (clientInstance) {
          console.log("Your are here in client creation");
         

          

          braintree.applePay.create({client: clientInstance}, function (applePayErr, applePayInstance) {
            if (applePayErr) {
              // Handle error here
              return;
            }
             // ...
            });

          }).then(function (applePayInstance) {
           // console.log(applePayInstance);
           
             

          }).catch(function (err) {
            // Handle error
            console.error("Brain tree error is ");
            console.log(err);
          });  

            

             
            
             
              
 
      });
  }
 
 

 

  
 

   
  

  


}
