import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare var jQuery: any;
interface Window {
  webkit?: any;
  androidCallBackInterface:any;
}
declare var window: Window;
@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.scss']
})
export class NewContactComponent implements OnInit {

  platform:string='';
  enc_key:any;
  country_id : number;
  phone_numbers:any;
  phoneNumber:any='';
  display_no:any='';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.loadScript('assets/js/livechat.js');
    this.enc_key    = this.route.snapshot.paramMap.get('post_data');
    localStorage.setItem('web_enc_key', this.enc_key);
 
    let data_list   =  JSON.parse(atob(this.enc_key));

    let device_info         = data_list.de; 
    this.platform = device_info;
     if(data_list.country_id == 1)
     {
      this.phoneNumber = '1 8774634233';
      this.display_no = '1-877.463.4233';
     }

     if(data_list.country_id == 2)
     {
      this.phoneNumber = '1 8005503501';
      this.display_no = '1-800.550.3501';
     }

     if(data_list.country_id == 3)
     {
      this.phoneNumber = '+44 8000418192';
      this.display_no = '+44 800-041-8192';
     }

     if(data_list.country_id == 8)
     {
      this.phoneNumber = '+61283173403';
      this.display_no = '+ 61 (28) 3173403';
     }
     if(data_list.country_id == 20)
     {
      this.phoneNumber = '+6498844133';
      this.display_no = '+ 64 (9) 8844133';
     }
     
    this.phone_numbers = {
      8:'+ 61 (28) 3173403',
      20:'+ 64 (9) 8844133',
      26:'',
      3:'+44 800-041-8192',
      1:'1-877.463.4233',
      2:'1-800.550.3501'}

  }
  public loadScript(url: string) 
  {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }
  goBack(obj:any)
  {
      var msg = '';
      if(obj !='')
      {
        msg = obj;
      }
      else{
        msg = 'Mobile web view closed by client'
      }
      msg = 'Mobile web view closed by client'
      this.closeApp(msg);
  }

  closeApp(obj:any)
  {
    var msg = '';
    
    if(obj !='')
    {
      msg = obj;
    }
    else{
      msg = 'Mobile web view closed by client'
    }
    msg = 'Mobile web view closed by client'
    if(this.platform == 'ios')
    {
      this.iosFuntionRevert(msg);
    }
    if(this.platform == 'android')
    {
      this.androidFuntionRevert(msg);
    }
 }
 showAndroidToast(obj)
 {
 if(this.platform == 'ios')
   {
     this.iosFuntionRevert(obj);
   }
   if(this.platform == 'android')
   {
     this.androidFuntionRevert(obj);
   }
 }
  
 iosFuntionRevert(resultdata)
 {
   console.log(resultdata);
   try {
        var message = resultdata;
         window.webkit.messageHandlers.callbackHandler.postMessage(message);
         } 
         catch(err) {
         }

 }

 androidFuntionRevert(toast) {
   console.log(toast);
   try {
        window.androidCallBackInterface.callBackFormData(toast);
       } catch(err) {
      }
   }

}
