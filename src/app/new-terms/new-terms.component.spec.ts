import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTermsComponent } from './new-terms.component';

describe('NewTermsComponent', () => {
  let component: NewTermsComponent;
  let fixture: ComponentFixture<NewTermsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewTermsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
