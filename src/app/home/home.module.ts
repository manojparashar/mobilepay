import { NgModule } from '@angular/core';
import { MaterialModule } from '../core/material.module';
import { HomepageComponent } from './homepage/homepage.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';


import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatDialogModule } from "@angular/material/dialog";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


//import { EnvVariableComponent } from './env-variable/env-variable.component';
 
//import { TestimonialsComponent } from '../shared/testimonials/testimonials.component';
//import { PromotionComponent } from '../promotion/promotion.component';
//import { MatCarouselModule } from '@ngmodule/material-carousel';
//import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
//import { LazyimgDirective } from '../core/lazyimg.directive';
import { MatButtonModule } from '@angular/material/button';
//import { RecaptchaModule, RecaptchaFormsModule, RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from 'ng-recaptcha';

 
@NgModule({
  imports: [
   // FormsModule,
    //RecaptchaModule,
    //RecaptchaFormsModule,
    //RecaptchaV3Module,
    CoreModule,
   // ReactiveFormsModule,
    MaterialModule,
    //MatAutocompleteModule,
    MatDialogModule,
    SharedModule,
    CommonModule,
    MatButtonModule,
    //MatCarouselModule.forRoot(),
	//NgxSkeletonLoaderModule.forRoot(),
    RouterModule.forChild([
      { path: '', component: HomepageComponent },
      //{ path: 'mobileapp', component: HomepageComponent, data: { scrollToMobileApp: true } },
     // { path: 'env', component: EnvVariableComponent },
      
    ])
  ],
  exports: [
    HomepageComponent,
    CommonModule,
    //MatCarouselModule
  ],
  declarations: [
    HomepageComponent,
    
    //EnvVariableComponent,
   // TestimonialsComponent,
  //PromotionComponent,
 // LazyimgDirective 
  ],
  providers: [],
  entryComponents: []
})
export class HomeModule { };