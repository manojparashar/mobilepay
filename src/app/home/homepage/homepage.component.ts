import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef, ViewChild, Injector } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
 
import { CountriesService } from '../../core/services/country.service';
import { AuthenticationService } from '../../core/services/auth.service';
import { SearchRatesService } from '../../rates/searchrates.service';
import { RazaEnvironmentService } from '../../core/services/razaEnvironment.service';


import { MatDialog } from '@angular/material/dialog';
 
 
import { Platform } from '@angular/cdk/platform';
import { ErrLoginComponent } from '../../mobile-pay/dialog/err-login/err-login.component';

import { AppBaseComponent } from '../../shared/components/app-base-component';

declare var jQuery: any;
interface Window {
  webkit?: any;
  androidCallBackInterface:any;
}
declare var window: Window;


 
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],  
})
export class HomepageComponent extends AppBaseComponent implements OnInit, AfterViewInit, OnDestroy {
  headerValue: number = 1;
  // slideConfig = { "slidesToShow": 3, "slidesToScroll": 2 };
  slideConfig = {
    "slidesToShow": 3, "slidesToScroll": 2,
    'responsive': [{
      'breakpoint': 769,
      'settings': {
        'slidesToShow': 2,
        'slidesToScroll': 1,
        'arrows': true,
        'dots': true
      }
    }]
  };
   
  isAuthenticated: boolean = false;
   
  @ViewChild('typedEl',{static: true}) typedEl: any;
  
  
  process_info:any='';
  platforms:any='';
  enc_key:any;
   
  
 
  recharge_screen:any='';

  userPhone : any;
  userPass : any;
  userToken : any;
   
  key_arr:any;
  planId: string;
  phone: string;
  search_country:any='US';
  search_country_id:any=1;
   
  isEnableOtherPlan: boolean = false;
  isAutoRefillEnable: boolean = false;
  paymentSubmitted: boolean;
  amount:any=10;
  processType:any=''; 
  promotionCode:string='';
   
  countryFromId:number;
  topup_no:string='';
  customer_id:any;
  pin_no:any;
  alphabet:any;


  loginForm: FormGroup;
  returnUrl: string;
  showDropdown: boolean = false;
  showForgotPass: boolean = false;
  allCountry: any[];
   
  showPlaceholder: boolean = true;

  timeLeft: number = 120;
  interval;
  otpSend: boolean = false;
  subscribeTimer: number;
  forgotPasswordForm: FormGroup;
  isEnableResendOtp: boolean = false;
  platform_type :string='';
  showContinue:boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
   
    public dialog: MatDialog,
    
    public platform: Platform,
 
    private titleService: Title,
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    
    private countryService: CountriesService,
    private searchRatesService: SearchRatesService,
    private razaEnvService: RazaEnvironmentService,
     
    public matDialog: MatDialog,
    public signupDialog: MatDialog,
    _injector: Injector
 
  ) {
    super(_injector);
  }

  ngOnInit() 
  {
     
 
    this.enc_key = this.route.snapshot.paramMap.get('post_data');
    localStorage.setItem('web_enc_key', this.enc_key);


    let data_list           =  JSON.parse(atob(this.enc_key));
    
     
      this.key_arr            = data_list;
      this.userPhone          = (data_list.phone)?data_list.phone:'';;
      this.userPass           = (data_list.password)?data_list.password:'';
      this.userToken          = (data_list.token)?data_list.token:'';
      this.promotionCode      = (data_list.coupon_code)?data_list.coupon_code:'';
      
      
      this.search_country_id  = data_list.country_id;
      this.countryFromId      = this.search_country_id;
    

    let device_info         = data_list.de;
    this.promotionCode      = (data_list.coupon_code)?data_list.coupon_code:'';
    this.customer_id        = (data_list.customer_id)?data_list.customer_id:'' ;

    if(this.promotionCode && this.promotionCode == 'buy5-get5')
      {
        this.router.navigate(['/buy5get5/'+this.enc_key]);
      }
       
    if(device_info.includes("||"))
    {
      var dres = device_info.split("||")
      this.platforms           = dres[4];
      //this.DeviceType         = dres[4];
    }
    else
    {
      this.platforms           = device_info;
      //this.DeviceType         = device_info;
    }
    let top_no              = ( data_list.top_no && data_list.top_no !='' ) ?data_list.top_no:'';
    this.topup_no           = top_no;
    if(this.topup_no == '' && this.customer_id == '')
    {
      this.recharge_screen = 'yes';
    }

    this.countryFromId      = this.search_country_id;
    
    this.platform_type = device_info;
     //this.loginWithToken();
     this.search_country_id  = data_list.country_id;
     
    this.countryFromId      = this.search_country_id;
    

    this.customer_id =(data_list.customer_id)?data_list.customer_id:'' ;
    this.pin_no = (data_list.pin_no)?data_list.pin_no:'' ;
    this.alphabet = (data_list.alphabet)?data_list.alphabet:'A';

    this.processLoginSteps();
    
    localStorage.setItem('platform', this.platform_type);
  }
  
  processLogin()
  {
   
  //this.router.navigate(['/webview/'+this.enc_key]);
  }
  
  ngAfterViewInit(): void {
    // const options = {
    //   strings: ['Stay Safe & Keep Talking', 'Get up to 20% bonus'],
    //   typeSpeed: 130,
    //   backSpeed: 130,
    //   showCursor: true,
    //   smartBackspace: true,
    //   cursorChar: '|',
    //   loop: true
    // };

    // const typed = new Typed('.typed-element', options);

  }

  ngOnDestroy(): void {
 
  }

  goBack(obj:any)
  {
      var msg = '';
      if(obj !='')
      {
        msg = obj;
      }
      else{
        msg = 'Mobile web view closed by client'
      }
      msg = 'Mobile web view closed by client'
      this.closeApp(msg);
  }

  closeApp(obj:any)
  {
    var msg = '';
    
    if(obj !='')
    {
      msg = obj;
    }
    else{
      msg = 'Mobile web view closed by client'
    }
    msg = 'Mobile web view closed by client'
    if(this.platforms == 'ios')
    {
      this.iosFuntionRevert(msg);
    }
    if(this.platforms == 'android')
    {
      this.androidFuntionRevert(msg);
    }
 }


 processLoginSteps()
  {
    localStorage.setItem('prev_token', this.userToken);
    localStorage.removeItem('currentUser');
    var d = new Date();
      var gtime = d.getTime()+" ";
       localStorage.setItem('last_login_time', gtime);
 

  // if(this.userPhone == '13129758545' ) 
  //if(this.userPhone == '17735750008' )
     if(this.userPhone == '16475050136')
    {
      
      this.autoLoginUser();
    }
    else
    {
      this.loginWithToken();
    }
   
  }
 
  loginWithToken() 
  {
    
      
       let body = {
          username: this.userToken,
          password: '0000000000',
          captcha: ''
        };
     
        this.authService.loginwToken(body, false, "Y").subscribe((response) => {
          //this.authService.login(body, false, "Y").subscribe((response) => {
          if (response != null) {
            this.routProcess();
            this.showContinue = true;
          }  
        },
        (error) => {
         const dialogRef = this.dialog.open(ErrLoginComponent, {
            data: {
              success: 'success',
            }
          });
           
          dialogRef.afterClosed().subscribe(result => {
            this.showAndroidToast('Mobile web view closed by client.');
          });
        });

       /* this.executeCaptcha('login').subscribe((token) =>{ },(error) => {
          console.log("error "+error); 
          } ) */

  }
  
  ////////// Forgor password /////////

 
  autoLoginUser() 
  {
    const phoneOrEmail = this.userPhone;
  /*  this.executeCaptcha('login').subscribe((token) =>{},(error) => {
      console.log(error.error_description);
        //console.log("error "+error); 
       // this.closeApp();
  })*/
   /*   TESTD4BEU0B3H4V45S1J9M3P
    let body = {
        username: phoneOrEmail,
        password: this.userPass,
        captcha: token
        
      };*/

      let body = {
        username: 'TESTD4BEU0B3H4V45S1J9M3P',
        password: phoneOrEmail,
        captcha: ''
        
      };
      this.authService.loginwToken(body, false, "Y").subscribe((response) => {
        if (response != null) {
           
            this.routProcess();
          this.showContinue = true;
           
        
        }  
      },
        (error) => {
          console.log(error.error_description);
         // this.closeApp();
        });
    
     
  }
  processLogin1()
  {
    this.routProcess();
  }
  routProcess()
  {
    if(this.promotionCode && this.promotionCode == 'buy1get1')
      {
        this.router.navigate(['/buy1get1/']);
      }
      else{

     
        if(this.topup_no != '')
        {
          this.router.navigate(['/topup/']);
        }
        
        else if(this.customer_id !='')
        {
          this.router.navigate(['/country_rates/']);
        }
        else
        this.router.navigate(['/plans']);
      }
      
     
  }
  showAndroidToast(obj)
  {
  if(this.platform_type == 'ios')
    {
      this.iosFuntionRevert(obj);
    }
    if(this.platform_type == 'android')
    {
      this.androidFuntionRevert(obj);
    }
  }
   
  iosFuntionRevert(resultdata)
  {
    console.log(resultdata);
    try {
         var message = resultdata;
          window.webkit.messageHandlers.callbackHandler.postMessage(message);
          } 
          catch(err) {
          }

  }
   
  androidFuntionRevert(toast) {
    console.log(toast);
    try {
         window.androidCallBackInterface.callBackFormData(toast);
        } catch(err) {
       }
    }
    

}
 