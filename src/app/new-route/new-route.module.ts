import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewFaqComponent } from '../new-faq/new-faq.component';
import { NewContactComponent } from '../new-contact/new-contact.component';
import { NewTermsComponent } from '../new-terms/new-terms.component';
import { NewPrivacyPolicyComponent } from '../new-privacy-policy/new-privacy-policy.component';
import { RouterModule } from '@angular/router';
import { NewAboutComponent } from '../new-about/new-about.component';
import { MaterialModule } from '../core/material.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    NewFaqComponent,
    NewContactComponent,
    NewTermsComponent,
    NewPrivacyPolicyComponent,
    NewAboutComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    RouterModule.forChild([
      { path:'contact-us', component:NewContactComponent },
      { path:'faq', component:NewFaqComponent },
      { path:'about-us', component:NewAboutComponent },
      { path:'terms', component:NewTermsComponent },
      { path:'privacy_policy', component:NewPrivacyPolicyComponent },

      { path:'contact-us/:post_data', component:NewContactComponent },
      { path:'faq/:post_data', component:NewFaqComponent },
      { path:'about-us/:post_data', component:NewAboutComponent },
      { path:'terms/:post_data', component:NewTermsComponent },
      { path:'privacy_policy/:post_data', component:NewPrivacyPolicyComponent }


      
    ])
  ]
})
export class NewRouteModule { }
