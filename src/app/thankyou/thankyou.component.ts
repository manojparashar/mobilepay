import { Component, OnInit } from '@angular/core';
import { applePay, ApplePayStatusCodes, ApplePaySession } from 'braintree-web/apple-pay';
declare var jQuery: any;
interface Window {
  webkit?: any;
  androidCallBackInterface: any;
  ApplePaySession: ApplePaySession;
}

declare let google:any
declare var window: Window;
var paymentsClient = new google.payments.api.PaymentsClient({
  environment: 'PRODUCTION' // TEST or 'PRODUCTION'
});
var Google_PaymentInstance: any;
var Apple_PaymentInstance: any;

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss']
})
export class ThankyouComponent implements OnInit {

  transType:String;
  topupPhone:String;
  topupAmount:String;
  platform:string;
    constructor( ) { }

    ngOnInit() {
      
      this.topupPhone = localStorage.getItem('topupPhone')
      this.topupAmount = localStorage.getItem('topupAmount')
      this.platform = localStorage.getItem('platform')

      localStorage.removeItem('topupPhone');
      
      localStorage.removeItem('topupAmount');
     }
  showThankYou()
  {
    let return_msg = localStorage.getItem('payment_response');
    localStorage.removeItem('payment_response');
    this.showAndroidToast('Mobile web view closed by client. ' + return_msg);
  }


  showAndroidToast(obj) {
    if (this.platform == 'ios') {
      this.iosFuntionRevert(obj);
    }
    if (this.platform == 'android') {
      this.androidFuntionRevert(obj);
    }
  }
  
   androidFuntionRevert(toast) {
    console.log(toast);
    try {
      window.androidCallBackInterface.callBackFormData(toast);
    } catch (err) {
    }
  }

  iosFuntionRevert(resultdata) {
    console.log(resultdata);
    try {
      var message = resultdata;
      window.webkit.messageHandlers.callbackHandler.postMessage(message);
    }
    catch (err) {
    }

  }

}
