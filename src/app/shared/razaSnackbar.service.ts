import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NotificationRibbonComponent } from "./dialog/notification-ribbon/notification-ribbon.component";

@Injectable({ providedIn: 'root' })
export class RazaSnackBarService {
    constructor(public snackBar: MatSnackBar) { }


    public openWarning(message: string) {
        this._openSnackBar(message, 'warning', true);
    }

    public openSuccess(message: string, closable?: true) {
        this._openSnackBar(message, 'success', true);
    }

    public openError(message: string, closable?: true) {
        this._openSnackBar(message, 'error', true);
    }

    public _openSnackBar(message: string, type: string = 'warning | success | info | error', closable?: boolean) {
        this.snackBar.openFromComponent(NotificationRibbonComponent, {
            verticalPosition: 'top',
            data: { message: message, closable: closable, type: type },
            panelClass: 'raza-notify-ribbon',
            duration: closable ? null : 2000
        });
    }

}