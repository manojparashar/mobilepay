import { Pipe, PipeTransform } from '@angular/core';
import { RazaEnvironmentService } from '../../core/services/razaEnvironment.service';
import { CurrencyCode } from '../../core/interfaces/CurrencyCode';

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'sCurrency' })
export class currencySmallSymbolPipe implements PipeTransform {
  constructor(private razaEnvService: RazaEnvironmentService) { }
  currencySymbol: string;

  transform(value: number, currency: CurrencyCode): string {
    if (currency == CurrencyCode.GBP) {
      this.currencySymbol = "p";
      return value + "" + this.currencySymbol;
    }
    else {
      this.currencySymbol = "¢";
      return value + " " + this.currencySymbol;
    }
  }
}

@Pipe({ name: 'bCurrency' })
export class currencyMainSymbolPipe implements PipeTransform {
  constructor(private razaEnvService: RazaEnvironmentService) { }
  currencySymbol: string = "$";
  transform(value: number, currency: CurrencyCode): string {
    //console.log(currency);
    if (currency == CurrencyCode.GBP) {
      this.currencySymbol = "£";
      return this.currencySymbol + "" + this.toFixed(value, 2);
    }
    else if (currency == CurrencyCode.USD) {
      this.currencySymbol = "$";
      return this.currencySymbol + "" + this.toFixed(value, 2);
    }
    else if (currency == CurrencyCode.CAD) {
      this.currencySymbol = "$";
      return this.currencySymbol + "" + this.toFixed(value, 2);
    }
    
    else if(!currency) {
     // this.currencySymbol = currency;
      return this.currencySymbol + " " + this.toFixed(value, 2);
    }
    else if(currency){
      this.currencySymbol = currency;
      return this.currencySymbol + " " + this.toFixed(value, 2);
    }
    else  {
      //this.currencySymbol = currency;
      return this.currencySymbol + " " + this.toFixed(value, 2);
    }
  }


  toFixed(num, fixed) {
    fixed = fixed || 0;
    fixed = Math.pow(10, fixed);
    return Math.floor(num * fixed) / fixed;
  }
}