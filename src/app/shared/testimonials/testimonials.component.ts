
import { Component, OnInit ,NgModule,ViewEncapsulation ,HostListener} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCarousel, MatCarouselComponent } from '@ngmodule/material-carousel';
@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TestimonialsComponent implements OnInit {
  innerWidth:any;
  constructor() { }
  main_bg  = 0;
  prop = 50;
  data = [
  {  name: "jeanine",
          date: "Mar 1,2020",
          content:"Best rates to call democratic republic of Congo ,Send Mobile Top-up through app itself",
          avatar:"/assets/images/testimonials/RazaAppIcon_120x120.png",
          bg_img: "/assets/images/testimonials/testimonial-bg-4.jpg",
          bg_color:"#493747"
        },
        {  name: "Sabir",
          date: "Aug 1,2020",
          content:"Good customer skill, fast response and very cooperative knows the job very well, thanks for prom extra 10%" ,
          avatar:"/assets/images/testimonials/testm_2.png",
          bg_img: "/assets/images/testimonials/testimonial-bg-5.jpg",
          bg_color:"#4d656f"
        },
        {  name: "Patrick",
          date: "Jan 31,2020",
          content:"This is the best international calling facility. Thank you so much Raza.",
          avatar:"/assets/images/testimonials/testm_2.png",
          bg_img: "/assets/images/testimonials/testimonial-bg-6.jpg",
          bg_color:"#273137"
        },
        {  name: "Gulshan",
          date: "Oct 28,2019",
          content:"Low rates and good service, I recommend Raza.com to all my friends and family members" ,
          avatar:"/assets/images/testimonials/testm_1.png",
          bg_img: "/assets/images/testimonials/testimonial-bg-1.jpg",
          bg_color:"#694957",
        },
        {  name: "Manisha",
          date: "Aug 18,2019",
          content:"Exceptionally good service Great Network, never faced any issues." ,
          avatar:"/assets/images/testimonials/testm_3.png",
          bg_img: "/assets/images/testimonials/testimonial-bg-2.jpg",
          bg_color:"#43aec8"
        },
        {  name: "Harsh",
          date: "Aug 17,2019",
          content:"I am with Raza for more than 3 years. Making calls is a great experience with affordable price. I wish i can give more than five stars." ,
          avatar:"/assets/images/testimonials/testm_2.png",
          bg_img: "/assets/images/testimonials/testimonial-bg-3a.jpg",
          bg_color:"#e1bd5d"
        }
        
      ];



   


 


  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    if(this.innerWidth > 400){
      this.prop = 140;
    }else if(this.innerWidth > 500){

      this.prop = 100;

    }else if(this.innerWidth < 500 && this.innerWidth > 1050){

      this.prop = 50;
    }
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

}
