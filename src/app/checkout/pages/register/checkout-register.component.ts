import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
//import { isNullOrUndefined } from 'util';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
import { Country } from '../../../core/models/country.model';
import { CountriesService } from '../../../core/services/country.service';
import { AuthenticationService } from '../../../core/services/auth.service';
import { ApiErrorResponse } from '../../../core/models/ApiErrorResponse';
import { OtpConfirmationComponent } from '../../../shared/dialog/otp-confirmation/otp-confirmation.component';
import { RegisterCustomerModel } from '../../../core/models/register-customer.model';
import { CheckoutService } from '../../services/checkout.service';
import { ICheckoutModel, NewPlanCheckoutModel } from '../../models/checkout-model';
import { TransactionType } from '../../../payments/models/transaction-request.model';
import { RazaLayoutService } from '../../../core/services/raza-layout.service';
import { PasswordBoxComponent } from '../../../shared/dialog/password-box/password-box.component';
import { AppBaseComponent } from '../../../shared/components/app-base-component';
import { isNullOrUndefined } from "../../../shared/utilities";
import { switchMap } from 'rxjs/operators';
 
import { CurrentSetting } from '../../../core/models/current-setting';
import { RazaEnvironmentService } from '../../../core/services/razaEnvironment.service';
@Component({
  selector: 'app-register',
  templateUrl: './checkout-register.component.html',
  styleUrls: ['./checkout-register.component.scss']
})
export class CheckoutRegisterComponent extends AppBaseComponent implements OnInit, OnDestroy {

  currentCart: ICheckoutModel
  purchaseInfoForm: FormGroup;
  countries: Country[];
  currentCountry: Country;
  submitButtonText = 'Verify';
  countryFlagCss: string;
  phoneAlreadyExistError = false;
  showDropdown: boolean = false;
  private currentCart$: Subscription;
  currentSetting$: Subscription;
  currentSetting: CurrentSetting;
  currentCurrency:any;
  allCountry: any[];
  countryFrom: Country[];
  isPromotion:boolean = false;
  hidePrefix:boolean=true;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private titleService: Title,
    private countryService: CountriesService,
    private authService: AuthenticationService,
    private dialog: MatDialog,
    private checkoutService: CheckoutService,
    private razaLayoutService: RazaLayoutService,
    private razaEnvService: RazaEnvironmentService,
    
    _injector: Injector
  ) {
    super(_injector)
  }

  ngOnInit() {
    this.titleService.setTitle('Register your account');
    this.razaLayoutService.setFixedHeader(true);

    if (this.authService.isAuthenticated()) {
      this.redirectToPaymentInfo();
    }

    this.purchaseInfoForm = this.formBuilder.group({
      phoneNumber: ['', [Validators.required]],
      password: ['']
    });

    this.currentSetting$ = this.razaEnvService.getCurrentSetting().subscribe(res => {
      this.currentSetting = res;
    });
    //get current cart.
    this.getCurrentCart();
    this.getFromCountries();
    this.gotoTop();
    this.getCountryFrom();
    const cart: NewPlanCheckoutModel = this.currentCart as NewPlanCheckoutModel;
    this.isPromotion = cart.isPromotion;
  }

  private getCountryFrom() {
    this.countryService.getFromCountries().subscribe((res: Country[]) => {
      this.countryFrom = res;
    });
  }

  gotoTop() {
    setTimeout (() => {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }, 1000);
  }
  ngOnDestroy(): void {
    this.currentCart$.unsubscribe();
  }
 
  getFromCountries() {
    this.countryService.getFromCountries().subscribe(
      (res: Country[]) => {

        const cart: NewPlanCheckoutModel = this.currentCart as NewPlanCheckoutModel;
        this.currentCountry = res.find(a => a.CountryId === cart.countryFrom);
        this.countryFlagCss = `flag flag-f-${this.currentCountry.CountryId}`

      });
  }

  /* Get Purchased Plan. */
  getCurrentCart() {
    this.currentCart$ = this.checkoutService.getCurrentCart().subscribe(
      (res: ICheckoutModel) => {
        this.currentCart = res;
      },
      err => { },
      () => {
        if (isNullOrUndefined(this.currentCart)) {
          this.router.navigate(['/']);
        }
      })
  }

  validateUsername()
  {
    var reciever = this.purchaseInfoForm.value.phoneNumber;
    var phoneno = /^\d{10}$/;
    if(reciever !='')
    {
       if(  reciever.match(phoneno) )
      {
        this.hidePrefix = true;
      }
      else{
        this.hidePrefix = false;
      }
    }
  }
  onPurchaseInfoFormSubmit() {
    this.phoneAlreadyExistError = false;
    if (!this.purchaseInfoForm.valid) {
      return;
    }
 
  }

  private openPasswordLoginDialog(phoneNumber: string) {
    /*var phoneno = /^\d{10}$/;
    var reciever = phoneNumber;
    if( reciever.match(phoneno) )
    {}
      
    */
   localStorage.setItem("login_no", phoneNumber);
    const dialogPassword = this.dialog.open(PasswordBoxComponent,
      {
        data: {
          phoneNumber: `${this.currentCountry.CountryCode}${phoneNumber}`,
        }
      });

    dialogPassword.afterClosed().subscribe(res => {
      if (res === true) {
        this.redirectToPaymentInfo();
      }
    },
      err => { },
    )
  }


  private sendOtp() {
    const phoneWithCountryCode = `${this.currentCountry.CountryCode}${this.purchaseInfoForm.value.phoneNumber}`
    //this.executeCaptcha('login').toPromise().then(token => {})
    this.authService.sendOtpForRegister(phoneWithCountryCode, '').toPromise()
      .then(res => {
        this.openOtpConfirmDialog(this.purchaseInfoForm.value.phoneNumber);
      })
    
  }

   

  private openOtpConfirmDialog(phoneNumber: string) {
   /* var phoneno = /^\d{10}$/;
    var reciever = phoneNumber;
    if( reciever.match(phoneno) )
    {}*/
      
    localStorage.setItem("login_no", phoneNumber);

    const dialogOtpConfirm = this.dialog.open(OtpConfirmationComponent,
      {
        data: {
          phoneNumber: `${this.currentCountry.CountryCode}${phoneNumber}`,
        }
      });

    dialogOtpConfirm.afterClosed().subscribe(otp => {
      if (!isNullOrUndefined(otp)) {
        this.registerAndLoginUser(phoneNumber, otp);
      }
    },
      err => { },
    )
  }

  private registerAndLoginUser(phoneNumber: string, otp: string) {
    //user Register and logIn.  navigate to payment Info.
    let callingToCountryId: number = 341;
    if (this.currentCart.transactiontype === TransactionType.Sale || this.currentCart.transactiontype === TransactionType.Activation) {
      const cart = this.currentCart as NewPlanCheckoutModel;
      callingToCountryId = cart.countryTo;
    }
    const tempEmail = `${phoneNumber}@raza.temp`;
    const registerModel: RegisterCustomerModel = {
      emailAddress: tempEmail,
      password: otp,
      firstName: '',
      lastName: '',
      primaryPhone: phoneNumber,
      alternatePhone: '',
      streetAddress: '',
      city: '',
      state: '',
      zipCode: '',
      country: this.currentCountry.CountryId,
      aboutUs: '',
      refererEmail: '',
      callingCountryId: callingToCountryId,
      isQuickSignUp: true
    }

    this.authService.register(registerModel).toPromise().then(
      (res: boolean) => {
        if (res) {
          const loginBody = {
            username: tempEmail,
            password: otp
          }
          this.authService.login(loginBody).toPromise().then(user => {
            this.redirectToPaymentInfo();
          })
        }
      }
    )
  }

  redirectToPaymentInfo() {
    this.router.navigate(['/checkout/payment-info']);
  }



  closeFlagDropDown() {
    this.showDropdown = false;
  }

  openFlagDropDown() {

    if (this.showDropdown) {
      this.showDropdown = false;
    } else {
      this.showDropdown = true;
    }
  }

  onSelectCountrFrom(country: Country) {
    this.currentSetting.country = country;
    this.razaEnvService.setCurrentSetting(this.currentSetting);
 
    this.closeFlagDropDown();
    this.setcurrentCurrency();
  }
  setcurrentCurrency()
  {
    if(this.currentSetting.country.CountryId == 1)
      this.currentCurrency='USD';
      if(this.currentSetting.country.CountryId == 2)
      this.currentCurrency='CAD';
      if(this.currentSetting.country.CountryId == 3)
      this.currentCurrency='GBP';
      const cart: NewPlanCheckoutModel = this.currentCart as NewPlanCheckoutModel;
      //var cart_info = this.checkoutService.getCurrentCart().
      if(this.currentSetting.country.CountryId == 1)
      {
        cart.details.ServiceCharge = 0
      }
      else{
        cart.details.ServiceCharge = 10
      }
      

      cart.CurrencyCode = this.currentCurrency;
      cart.currencyCode = this.currentCurrency;
      this.checkoutService.setCurrentCart(cart);
      console.log(cart);
       
  }

}
