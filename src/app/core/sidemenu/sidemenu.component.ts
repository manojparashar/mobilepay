import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Plan } from '../../accounts/models/plan';
import { AuthenticationService } from '../services/auth.service';
import { SideBarService } from './sidemenu.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { LoginpopupComponent } from '../../core/loginpopup/loginpopup.component';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {
  isLoggedIn: boolean;
  isSmallScreen: boolean;
  plan: Plan;
  isDisplayAccountMenu: boolean;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    public dialog: MatDialog,
    private sideBarNavService: SideBarService
  ) { }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.sideBarNavService.toggle();
      }
    })
  }

  isUserAuthenticated(): boolean {
    return this.authService.isAuthenticated();
  }
  clickRecharge()
  {
    if(this.authService.isAuthenticated())
    {
      this.router.navigate(['/account/overview']);
    }
    else
    {
      const dialogConfig = new MatDialogConfig();
      // The user can't close the dialog by clicking outside its body
      dialogConfig.disableClose = true;
      dialogConfig.id = "modal-component";
      dialogConfig.height = "350px";
      dialogConfig.width = "600px";
      dialogConfig.data = {
        name: "logout",
        title: "Are you sure you want to logout?",
        description: "Pretend this is a convincing argument on why you shouldn't logout :)",
        actionButtonText: "Logout",
      }
      // https://material.angular.io/components/dialog/overview
      const modalDialog = this.dialog.open(LoginpopupComponent, dialogConfig);
    }
  }
  
  redirectClick()
  { 
      if(this.authService.isAuthenticated())
      {
        this.router.navigate(['account/overview'])
      }
      else
      {
      this.dialog.open(LoginpopupComponent, {
     
        data: { slideIndex: '' }
      });
      }
      
  }

}
