import { Component, OnInit, Input } from '@angular/core';
import { Plan } from '../../../accounts/models/plan';

@Component({
  selector: 'app-account-top-menu',
  templateUrl: './account-top-menu.component.html',
  styleUrls: ['./account-top-menu.component.scss']
})
export class AccountTopMenuComponent implements OnInit {
  @Input() plan: Plan;

  constructor(
  ) { }

  ngOnInit() {

  }
}
