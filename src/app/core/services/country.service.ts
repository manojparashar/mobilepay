import { ComponentFactoryResolver, Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { Country } from "../models/country.model";
import { RazaEnvironmentService } from "./razaEnvironment.service";
import { Api } from "./api.constants";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CustomErrorHandlerService } from "./custom-error-handler.service";
import { State, PostalCode } from "../../accounts/models/billingInfo";
import { ApiErrorResponse } from "../models/ApiErrorResponse";
import { map, catchError } from "rxjs/operators";
import { NgxXml2jsonService } from 'ngx-xml2json';
import { switchMap } from "rxjs/operators";
@Injectable({providedIn: 'root'})
export class CountriesService {
    constructor(private httpClient: HttpClient,
        private razaEnvService: RazaEnvironmentService,
        private errorHandleService: CustomErrorHandlerService,
        private ngxXml2jsonService: NgxXml2jsonService
    ) { }
    
    private _getHeaders():Headers {
      let header = new Headers({
        'Content-Type': 'text/xml'
      });
   
      return header;
   }

    //public getSoap():Observable<any| ApiErrorResponse>
    public getSoap() 
    { 
    }
    async parseXmlToJson(xml) {
      // With parser
      /* const parser = new xml2js.Parser({ explicitArray: false });
      parser
        .parseStringPromise(xml)
        .then(function(result) {
          console.log(result);
          console.log("Done");
        })
        .catch(function(err) {
          // Failed
        }); */
  
      // Without parser
      }
 

    public getAllCountries():Observable<Country[] | ApiErrorResponse>
    {
        return this.httpClient.get<Country[]>(`${Api.countries.getAllCountries}`)
        .pipe(
            catchError(err=> this.errorHandleService.handleHttpError(err))
        );  
    }
    
    public getFromCountries():Observable<Country[] | ApiErrorResponse>
    {
        return this.httpClient.get<Country[]>(`${Api.countries.base}/From`)
        .pipe(
            catchError(err=> this.errorHandleService.handleHttpError(err))
        );  
    }

    public getStates(countryId: number):Observable<State[] | ApiErrorResponse>
    {
        return this.httpClient.get<State[]>(`${Api.countries.base}/${countryId}/States`)
        .pipe(
            catchError(err=> this.errorHandleService.handleHttpError(err))
        );  
    }

    public getPostalCodes(stateId: string):Observable<PostalCode[] | ApiErrorResponse>
    {
        return this.httpClient.get<PostalCode[]>(`${Api.countries.getAllPostalCodes}/${stateId}/AreaCodes`)
        .pipe(
            catchError(err=> this.errorHandleService.handleHttpError(err))
        );  
    }


}