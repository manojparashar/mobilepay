export enum CurrencyCode {
    USD = 'USD',
    CAD = 'CAD',
    GBP = 'GBP',
    AUD = 'AUD',
    INR = 'INR',
    NZD = 'NZD',

}